# Rachel's Playdate Game Pack

Download on itchio: https://moosadee.itch.io/rachels-playdate-game-pack

Games for the Playdate! https://play.date/

## Screenshots

![Screenshot of Fin 'N' Kit](RachelsGamePack/screenshots/finnkit-small.gif)
![Screenshot of High Five King](RachelsGamePack/screenshots/playdate-20220724-184729.png)
![Screenshot of Bass Ball](RachelsGamePack/screenshots/playdate-20220703-004628.png)
![Screenshot of Motor Horse](RachelsGamePack/screenshots/playdate-20220917-193155.png)
![Screenshot of Deskapades](RachelsGamePack/screenshots/playdate-20220928-121246.png)
![Screenshot of We Have Lasers](RachelsGamePack/screenshots/playdate-20220708-145550.png)

## License

Creative Commons Attribution license

Under the Creative Commons Attribution license, commonly abbreviated as CC-BY, you are free:

* to Share — to copy, distribute and transmit the work
* to Remix — to adapt the work

Under the following conditions:

* Attribution — You must attribute the work in the manner specified by the author or licensor (but not in any way that suggests that they endorse you or your use of the work).

compilerPath='/media/rachelwil/RachelsSSD1/LIBRARIES/PlaydateSDK-2.2.0/bin/pdc'
simulatorPath='/media/rachelwil/RachelsSSD1/LIBRARIES/PlaydateSDK-2.2.0/bin/PlaydateSimulator'
sourcePath='./Source'
outputPath='./output.pdx'

# pdc [sourcepath] [outputpath]

echo "Compiler path:  $compilerPath"
echo "Simulator path: $simulatorPath"
echo "Source path:    $sourcePath"
echo "Output path:    $outputPath"
echo ""

echo "Compile source..."
$compilerPath $sourcePath $outputPath


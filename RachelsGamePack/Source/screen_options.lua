import 'CoreLibs/graphics.lua'
import 'manager_gamedata.lua'
import 'global.lua'
local gfx = playdate.graphics
local snd = playdate.sound

optionsScreen = {
  ---------------------------------------------------------------------- INIT
  Init = function( self )
    self.nextState = ""

    self.images = {
      background     = gfx.image.new( "images/menubg.png" ),
      button         = gfx.image.new( "images/buttonbg.png" ),
      button2        = gfx.image.new( "images/buttonmid.png" ),
      cursor         = gfx.image.new( "images/cursor2.png" ),
      cursorPoint    = gfx.image.new( "images/cursor3.png" ),
    }

    self.tables = {
      optionsButtons = nil,
      loadCursor = nil,
    }

    self.buttons = {
      { -- 1
        type = "button", text = "Back",
        x = 10, y = 200, width = 180, height = 30,
        textOffsetX = 50, textOffsetY = 7,
        cursorOffsetX = -10, cursorOffsetY = 0,
        gotoState = "mainMenuScreen",
        tabUp = 2,
        tabDown = 1,
        tabLeft = 6,
        tabRight = 6
      },
      { -- 2
        type = "icon", tableImage = 1,
        x = 25, y = 50, width = 40, height = 40,
        cursorOffsetX = 0, cursorOffsetY = 40,
        action = "decrease_sound_volume",
        tabUp = 2,
        tabDown = 1,
        tabLeft = 5,
        tabRight = 3
      },
      { -- 3
        type = "icon", tableImage = 2,
        x = 125,  y = 50, width = 40, height = 40,
        cursorOffsetX = 0, cursorOffsetY = 40,
        action = "increase_sound_volume",
        tabUp = 3,
        tabDown = 1,
        tabLeft = 2,
        tabRight = 4
      },
      { -- 4
        type = "icon", tableImage = 1,
        x = 225,  y = 50, width = 40, height = 40,
        cursorOffsetX = 0, cursorOffsetY = 40,
        action = "decrease_music_volume",
        tabUp = 4,
        tabDown = 6,
        tabLeft = 3,
        tabRight = 5
      },
      { -- 5
        type = "icon", tableImage = 2,
        x = 325,  y = 50, width = 40, height = 40,
        cursorOffsetX = 0, cursorOffsetY = 40,
        action = "increase_music_volume",
        tabUp = 5,
        tabDown = 6,
        tabLeft = 4,
        tabRight = 2
      },
      { -- 6
        type = "button2", text = "Clear data",
        x = 250, y = 200, width = 130, height = 30,
        textOffsetX = 70, textOffsetY = 7, cursorOffsetX = -10, cursorOffsetY = 0,
        action = "clear_data",
        tabUp = 5,
        tabDown = 6,
        tabLeft = 1,
        tabRight = 1
      },
    }

    self.cursor = {
      width = 25,
      height = 25,
      positions = {
      },
      currentPosition = 1,
      frame = 1,
      frameMax = 10
    }

    self.audio = {
      success = snd.sampleplayer.new( "audio/sfx3.wav" ),
      erasing = snd.sampleplayer.new( "audio/sfx6.wav" ),
    }

    self.music = nil

    gfx.setFont( fonts.pedallica )

    self.tables.optionsButtons, result = gfx.imagetable.new( "images/btnOptions" )
    if ( result ~= nil ) then print( "ERROR:", result ) end

    self.tables.loadCursor, result = gfx.imagetable.new( "images/loadcursor" )
    if ( result ~= nil ) then print( "ERROR:", result ) end

    if ( self.music == nil ) then
      self.music, result  = snd.fileplayer.new( "audio/song_dude" )
      if ( result ~= nil ) then print( "ERROR:", result ) end

      self.music:setVolume( dataManager.data.music_volume / 100.0 )
      self.music:play( 0 )
      song_playing = self.music
    end
  end,

  ---------------------------------------------------------------------- UPDATE
  Update = function( self )
    self:HandleInput()

    local selection = self.cursor.currentPosition
    local button = self.buttons[ selection ]

    if ( inputManager:IsKeyPressed( "a" ) and button.action == "clear_data" ) then
      self.cursor.frame += 0.1

      if ( math.floor(self.cursor.frame * 10) % 10 == 0 ) then

        if ( self.audio.erasing:isPlaying() == false ) then
          self.audio.erasing:setVolume( dataManager.data.sound_volume / 100.0 )
          self.audio.erasing:play()
        end

      end

      if ( self.cursor.frame > self.cursor.frameMax ) then
        self.cursor.frame = self.cursor.frameMax
        -- Done loading, execute
        dataManager:ResetData()

        if ( self.audio.success:isPlaying() == false ) then
          self.audio.success:setVolume( dataManager.data.sound_volume / 100.0 )
          self.audio.success:play()
        end
      end
    else
      self.cursor.frame = 1
    end
  end,

  ---------------------------------------------------------------------- DRAW
  Draw = function( self )
    self.images.background:draw( 0, 0 )
    gfx.drawTextAligned( "OPTIONS", 25, 3, kTextAlignment.left )

    gfx.drawTextAligned( "Sound effects", 100, 30, kTextAlignment.center )

    gfx.drawTextAligned( "Music", 300, 30, kTextAlignment.center )

    gfx.drawTextAligned( tostring( dataManager.data.sound_volume ) .. "%", 95, 65, kTextAlignment.center )

    gfx.drawTextAligned( tostring( dataManager.data.music_volume ) .. "%", 300, 65, kTextAlignment.center )

    -- Draw buttons
    for key, button in pairs( self.buttons ) do
      if ( button.type == "button" ) then
        self.images.button:draw( button["x"], button["y"] )
        gfx.drawTextAligned( button["text"], button["x"] + button["textOffsetX"], button["y"] + button["textOffsetY"], kTextAlignment.center )
      elseif ( button.type == "button2" ) then
        self.images.button2:draw( button["x"], button["y"] )
        gfx.drawTextAligned( button["text"], button["x"] + button["textOffsetX"], button["y"] + button["textOffsetY"], kTextAlignment.center )
      else
        -- self.tables.explosion:getImage( effect.frame ):draw( effect.x, effect.y )
        self.tables.optionsButtons:getImage( button.tableImage ):draw( button.x, button.y )
      end
    end

    -- Draw cursor
    local selection = self.cursor.currentPosition
    local button = self.buttons[ selection ]

    if ( inputManager:IsKeyPressed ( "a" ) and button.action == "clear_data" ) then
      -- Holding down the confirm button
      self.tables.loadCursor:getImage( math.floor( self.cursor.frame ) ):draw( button["x"] + button["cursorOffsetX"], button["y"] )

    elseif ( button.type == "button" or button.type == "button2" ) then
      -- Pointing to a text button
      self.images.cursor:draw( button["x"] + button["cursorOffsetX"], button["y"] )

    else
      -- Pointing to an options button
      self.images.cursorPoint:draw( button["x"] + button["cursorOffsetX"], button["y"] + button["cursorOffsetY"] )
    end
  end,

  ---------------------------------------------------------------------- CLEANUP
  Cleanup = function( self )
    -- Free space here as needed
    self.music:stop()
    song_playing = nil
  end,

  ---------------------------------------------------------------------- SCROLL FORWARD
  TabLeft = function( self )
    self.cursor.currentPosition = self.buttons[ self.cursor.currentPosition ].tabLeft
  end,

  ---------------------------------------------------------------------- SCROLL BACKWARD
  TabRight = function( self )
    self.cursor.currentPosition = self.buttons[ self.cursor.currentPosition ].tabRight
  end,

  ---------------------------------------------------------------------- SCROLL BACKWARD
  TabUp = function( self )
    self.cursor.currentPosition = self.buttons[ self.cursor.currentPosition ].tabUp
  end,

  ---------------------------------------------------------------------- SCROLL BACKWARD
  TabDown = function( self )
    self.cursor.currentPosition = self.buttons[ self.cursor.currentPosition ].tabDown
  end,

  ---------------------------------------------------------------------- HANDLE INPUT
  HandleInput = function( self )
    if ( inputManager:IsKeyPressRelease( "up" ) ) then
      self:TabUp()

    elseif ( inputManager:IsKeyPressRelease( "down" ) ) then
      self:TabDown()

    elseif ( inputManager:IsKeyPressRelease( "left" ) ) then
      self:TabLeft()

    elseif ( inputManager:IsKeyPressRelease( "right" ) ) then
      self:TabRight()


    elseif ( inputManager:IsKeyPressRelease( "a" ) ) then
      local cursorPosition = self.cursor["currentPosition"]
      local selection = self.cursor.currentPosition
      local button = self.buttons[ selection ]

      if ( button.gotoState ~= nil ) then
        optionsScreen.nextState = button["gotoState"]
      end

      -- This was originally under ButtonUp for A...
      local cursorPosition = self.cursor["currentPosition"]
      local selection = self.cursor.currentPosition
      local button = self.buttons[ selection ]

      if ( button.action == nil ) then return end

      if      ( button.action == "decrease_sound_volume" ) then
        dataManager.data.sound_volume -= 10
        self.audio.success:setVolume( dataManager.data.sound_volume / 100.0 )
        self.audio.success:play()

      elseif  ( button.action == "increase_sound_volume" ) then
        dataManager.data.sound_volume += 10
        self.audio.success:setVolume( dataManager.data.sound_volume / 100.0 )
        self.audio.success:play()

      elseif  ( button.action == "decrease_music_volume" ) then
        dataManager.data.music_volume -= 10
        self.music:setVolume( dataManager.data.music_volume / 100.0 )
        self.music:play( 0 )

      elseif  ( button.action == "increase_music_volume" ) then
        dataManager.data.music_volume += 10
        self.music:setVolume( dataManager.data.music_volume / 100.0 )
        self.music:play( 0 )

      end

      if ( dataManager.data.sound_volume < 0 )   then dataManager.data.sound_volume = 0 end
      if ( dataManager.data.sound_volume > 100 ) then dataManager.data.sound_volume = 100 end
      if ( dataManager.data.music_volume < 0 )   then dataManager.data.music_volume = 0 end
      if ( dataManager.data.music_volume > 100 ) then dataManager.data.music_volume = 100 end

      dataManager:SaveData()
    end
  end,
}

import 'CoreLibs/graphics.lua'
local gfx = playdate.graphics

creditsScreen = {
  ---------------------------------------------------------------------- INIT
  Init = function( self )
    self.nextState = ""
    self.key = "title"    -- A string identifier for this state

    self.images = {
      background = gfx.image.new( "images/menubg.png" ),
      button     = gfx.image.new( "images/buttonbg.png" ),
      cursor     = gfx.image.new( "images/cursor2.png" ),
    }

    self.buttons = {
      {
        text = "Back",
        x = 10,
        y = 200,
        width = 180,
        height = 30,
        textOffsetX = 50,
        textOffsetY = 7,
        cursorOffsetX = -10,
        gotoState = "mainMenuScreen"
      },
    }

    self.cursor = {
      width = 30,
      height = 30,
      currentPosition = 1
    }
  end,

  ---------------------------------------------------------------------- CLEANUP
  Cleanup = function( self )
    -- Free space here as needed
    self.key = nil
    self.nextState = nil
    self.images = nil
    self.buttons = nil
    self.cursor = nil
  end,

  ---------------------------------------------------------------------- UPDATE
  Update = function( self )
    self:HandleInput()
  end,

  ---------------------------------------------------------------------- DRAW
  Draw = function( self )
    gfx.setFont( fonts.pedallica )

    self.images.background:draw( 0, 0 )
    gfx.drawTextAligned( "CREDITS SCREEN", 25, 3, kTextAlignment.left )

    gfx.drawText( "Rachel's Game Pack (2022-2023)", 10, 25 )
    gfx.drawText( "Programming, Music, Sound Effects, Art", 15, 45 )
    gfx.drawText( "by Rachel Wil Sha Singh", 15, 65 )
    gfx.drawText( "(Moosadee.com)", 15, 85 )

    gfx.drawText( "Fonts from the PlayDate SDK", 10, 125 )
    gfx.drawText( "Pedalicca, Rains, Newsleak, Nontendo,", 15, 145 )
    gfx.drawText( "Quickboot, Roobert, Sasser.", 15, 165 )

    -- Draw buttons
    for key, button in pairs( self.buttons ) do
      self.images.button:draw( button["x"], button["y"] )
      gfx.drawTextAligned( button["text"], button["x"] + button["textOffsetX"], button["y"] + button["textOffsetY"], kTextAlignment.center )
    end

    -- Draw cursor
    local selection = self.cursor.currentPosition
    local button = self.buttons[ selection ]
    self.images.cursor:draw( button["x"] + button["cursorOffsetX"], button["y"] )
  end,

  ---------------------------------------------------------------------- HANDLE INPUT
  HandleInput = function( self )
    if ( inputManager:IsKeyPressRelease( "down" ) ) then
      self.cursor["currentPosition"] = self.cursor["currentPosition"] + 1
      if ( self.cursor["currentPosition"] > #self.buttons ) then
        self.cursor["currentPosition"] = 1
      end

    elseif ( inputManager:IsKeyPressRelease( "up" ) ) then
      self.cursor["currentPosition"] = self.cursor["currentPosition"] - 1
      if ( self.cursor["currentPosition"] == 0 ) then
        self.cursor["currentPosition"] = #self.buttons
      end

    elseif ( inputManager:IsKeyPressRelease( "a" ) ) then
      local cursorPosition = self.cursor["currentPosition"]
      local selection = self.cursor.currentPosition
      local button = self.buttons[ selection ]
      self.nextState = button["gotoState"]
    end
  end,
}

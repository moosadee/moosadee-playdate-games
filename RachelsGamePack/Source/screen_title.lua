import 'CoreLibs/graphics.lua'
import 'manager_gamedata.lua'
import 'manager_input.lua'
import 'global.lua'
local gfx = playdate.graphics
local snd = playdate.sound

titleScreen = {
  ---------------------------------------------------------------------- INIT
  Init = function( self )
    gfx.setFont( fonts.pedallica )

    self.nextState = ""
    self.key = "title"    -- A string identifier for this state

    self.buttons = {
      {
        text = "Play",
        x = 300,
        y = 120,
        width = 90,
        height = 30,
        textOffsetX = 50,
        textOffsetY = 7,
        cursorOffsetX = -10,
        gotoState = ""
      },
      {
        text = "Help",
        x = 300,
        y = 160,
        width = 90,
        height = 30,
        textOffsetX = 50,
        textOffsetY = 7,
        cursorOffsetX = -10,
        gotoState = "helpScreen"
      },
      {
        text = "Exit",
        x = 300,
        y = 200,
        width = 90,
        height = 30,
        textOffsetX = 50,
        textOffsetY = 7,
        cursorOffsetX = -10,
        gotoState = "mainMenuScreen"
      }
    }

    self.gameData = dataManager["data"][global.gameKey]
    if ( self.gameData.levelStatuses == nil ) then
      self.buttons[1].gotoState     = self.gameData.gameState
    else
      -- If self game has levels, the "play" button should go to the level select.
      self.buttons[1].gotoState     = "levelSelectScreen"
    end

    self.images = {
      background = gfx.image.new( self.gameData.title_bg ),
      cursor     = gfx.image.new( self.gameData.cursor ),
      button     = gfx.image.new( "images/buttonbg.png" ),
    }

    self.cursor = {
      width = 30,
      height = 30,
      currentPosition = 1
    }
  end,

  ---------------------------------------------------------------------- CLEANUP
  Cleanup = function( self )
    self.cursor = nil
    self.gameData = nil
    self.images = nil
    self.key = nil
    self.nextState = nil
  end,

  ---------------------------------------------------------------------- UPDATE
  Update = function( self )
    self:HandleInput()
  end,

  ---------------------------------------------------------------------- DRAW
  Draw = function( self )
    self.images.background:draw( 0, 0 )

    -- Draw buttons
    for key, button in pairs( self.buttons ) do
      self.images.button:draw( button["x"], button["y"] )
      gfx.drawTextAligned( button["text"], button["x"] + button["textOffsetX"], button["y"] + button["textOffsetY"], kTextAlignment.center )
    end

    -- Draw cursor
    local selection = self.cursor.currentPosition
    local button = self.buttons[ selection ]
    self.images.cursor:draw( button["x"] + button["cursorOffsetX"], button["y"] )
  end,

  ---------------------------------------------------------------------- HANDLE INPUT
  HandleInput = function( self )
    if ( inputManager:IsKeyPressRelease( "down" ) ) then
      self.cursor["currentPosition"] = self.cursor["currentPosition"] + 1
      if ( self.cursor["currentPosition"] > #self.buttons ) then
        self.cursor["currentPosition"] = 1
      end

    elseif ( inputManager:IsKeyPressRelease( "up" ) ) then
      self.cursor["currentPosition"] = self.cursor["currentPosition"] - 1
      if ( self.cursor["currentPosition"] == 0 ) then
        self.cursor["currentPosition"] = #self.buttons
      end

    elseif ( inputManager:IsKeyPressRelease( "a" ) ) then
      local cursorPosition = self.cursor["currentPosition"]
      local selection = self.cursor.currentPosition
      local button = self.buttons[ selection ]
      self.nextState = button["gotoState"]
    end
  end,
}

import 'CoreLibs/graphics.lua'
local gfx = playdate.graphics

currentVersion = "2.0"
--build = "debug"
build = "release"

dataManager = {
  data = {},

  ResetGameData = function( self )
    print( "Refreshing game data..." )
    self.data.version = currentVersion

    self.data.bass.title_bg    = "packages/bass/images/title.png"
    self.data.bass.cursor      = "packages/bass/images/cursor.png"
    self.data.bass.endscreen   = "packages/bass/images/win.png"
    self.data.bass.song        = "audio/song_bassball"
    self.data.bass.gameState   = "bass_gameScreen"
    self.data.bass.levelIcons = "images/levelicons"
    self.data.bass.help     = {
      "Help the penguin play Bass Ball!",
      "Use the crank to aim your throw",
      "then press UP to shoot!"
    }

    self.data.crank.title_bg    = "packages/crank/images/title.png"
    self.data.crank.cursor      = "packages/crank/images/cursor.png"
    self.data.crank.song        = "audio/song_bassball"
    self.data.crank.gameState   = "crank_gameScreen"
    self.data.crank.song        = "audio/song_bassball"
    self.data.crank.levelIcons = "images/levelicons"
    self.data.crank.help     = {
      "Clarissa needs to get out of the office!",
      "Use coffee cups to distract coworkers",
      "so she can make a clean break! (Maybe",
      "also grab some spare change on the way)",
      "CRANK to reset the level."
    }


    self.data.highfive.title_bg    = "packages/highfive/images/title.png"
    self.data.highfive.cursor      = "packages/highfive/images/cursor.png"
    self.data.highfive.song        = "audio/song_dude"
    self.data.highfive.gameState   = "highfive_gameScreen"
    self.data.highfive.levelIcons = "packages/highfive/images/levelicons"
    self.data.highfive.help     = {
      "Start with the crank in the DOWN position.",
      "Your challenger will wait for a good moment,",
      "and then put their hand up for a HIGH FIVE!",
      "CRANK your arm up to give a HIGH FIVE.",
      "If you miss, you lose the round.",
      "If you give them a high five in time",
      "you win the round!"
    }

    self.data.lasers.title_bg    = "packages/lasers/images/title.png"
    self.data.lasers.cursor      = "packages/lasers/images/cursor.png"
    self.data.lasers.song        = "audio/song_dude"
    self.data.lasers.gameState   = "lasers_gameScreen"
    self.data.lasers.levelIcons = "images/levelicons"
    self.data.lasers.help     = {
      "A DISTRESS SIGNAL HAS BEEN RECEIVED FROM A",
      "STRANGE PLANET. WE ARE EN ROUTE TO INVESTIGATE...",
      "",
      "USE CRANK TO MOVE, DOWN TO SHOOT LASERS."
    }

    self.data.motor.title_bg    = "packages/motor/images/title.png"
    self.data.motor.cursor      = "packages/motor/images/cursor.png"
    self.data.motor.song        = "audio/song_dude"
    self.data.motor.gameState   = "motor_gameScreen"
    self.data.motor.levelIcons = "images/levelicons"
    self.data.motor.help     = {
      "Use the crank to propel SpyHorse upward.",
      "Stop cranking to let gravity take over",
      "and make you move downward.",
      "Collect birds, avoid clouds and elves."
    }

    self.data.finkit.title_bg    = "packages/finkit/images/title.png"
    self.data.finkit.cursor      = "packages/finkit/images/cursor.png"
    self.data.finkit.song        = "audio/song_flapjacks"
    self.data.finkit.gameState   = "finkit_gameScreen"
    self.data.finkit.levelIcons = "images/levelicons"
    self.data.finkit.help     = {
      "Help Fin the Dolphin and Kit the Cat",
      "collect delicious fishies while avoiding",
      "obstacles! Press B to make Fin jump,",
      "and A to make Kit jump!"
    }

    self.data.pup.title_bg    = "packages/pup/images/title.png"
    self.data.pup.cursor      = "packages/pup/images/cursor.png"
    self.data.pup.song        = "audio/song_dude"
    self.data.pup.gameState   = "pup_gameScreen"
    self.data.pup.help     = {
      "Is the pup a good boy?",
      "Vote on each pup using",
      "Crank LEFT for REJECT and RIGHT for LOVE",
      "",
      "Want your doggo in this game?",
      "Email photo to Rachel@Moosader.com",
      "include dog's name and your name or username"
    }

    self.data.skele.title_bg    = "packages/skele/images/title.png"
    self.data.skele.cursor      = "packages/skele/images/cursor.png"
    self.data.skele.song        = "audio/song_dude"
    self.data.skele.gameState   = "skele_gameScreen"
    self.data.skele.help     = {
      "You are the mailer daemon at a corporate office.",
      "Deliver messages between employees across",
      "departments. Minimap shows ! new delivery. Deliver",
      "all mail to finish your workday and go home.",
      "Avoid Ghost Associates who only have more",
      "work to give you."
    }

    self.data.tpcat.title_bg    = "packages/tpcat/images/title.png"
    self.data.tpcat.cursor      = "packages/tpcat/images/cursor.png"
    self.data.tpcat.song        = "audio/song_flapjacks"
    self.data.tpcat.gameState   = "tpcat_gameScreen"
    self.data.tpcat.help     = {
      "Try to unroll all the toilet paper as fast",
      "as possible and try to get the high score!",
      "",
      "You MUST hold the [DOWN] key while cranking!",
      "Dedicated to Rose, Natalie, Ruby, and Basel"
    }
  end,

  ResetUserData = function( self )
    self.data["sound_volume"] = 100
    self.data["music_volume"] = 20

    self.data.bass.levelStatuses = {
      -- -- 1 = locked, 2 = no stars, 3 = one star, 4 = two stars, 5 = three stars
      { name = "Level 1", icon = 1, gotoLevel = 1, state = 2 },
      { name = "Level 2", icon = 2, gotoLevel = 2, state = 2 },
      { name = "Level 3", icon = 3, gotoLevel = 3, state = 2 },
      { name = "Level 4", icon = 4, gotoLevel = 4, state = 2 },
      { name = "Level 5", icon = 5, gotoLevel = 5, state = 2 },
      { name = "Level 6", icon = 6, gotoLevel = 6, state = 2 },
      { name = "Level 7", icon = 7, gotoLevel = 7, state = 2 },
      { name = "Level 8", icon = 8, gotoLevel = 8, state = 2 },
      { name = "Level 9", icon = 9, gotoLevel = 9, state = 2 },
    }

    self.data.crank.levelStatuses = {
      -- -- 1 = locked, 2 = no stars, 3 = one star, 4 = two stars, 5 = three stars
      { name = "Level 1", icon = 1, gotoLevel = 1, state = 2 },
      { name = "Level 2", icon = 2, gotoLevel = 2, state = 2 },
      { name = "Level 3", icon = 3, gotoLevel = 3, state = 2 },
      { name = "Level 4", icon = 4, gotoLevel = 4, state = 2 },
      { name = "Level 5", icon = 5, gotoLevel = 5, state = 2 },
      { name = "Level 6", icon = 6, gotoLevel = 6, state = 2 },
      { name = "Level 7", icon = 7, gotoLevel = 7, state = 2 },
      { name = "Level 8", icon = 8, gotoLevel = 8, state = 2 },
      { name = "Level 9", icon = 9, gotoLevel = 9, state = 2 },
    }

    self.data.highfive.levelStatuses = {
      -- -- 1 = locked, 2 = no stars, 3 = one star, 4 = two stars, 5 = three stars
      { name = "Challenger: Ozha",          icon = 2, gotoLevel = 1, state = 2 },
      { name = "Challenger: Ayda",          icon = 1, gotoLevel = 2, state = 2 },
      { name = "Challenger: Rawr",          icon = 6, gotoLevel = 3, state = 2 },
      { name = "Challenger: Luha",          icon = 3, gotoLevel = 4, state = 2 },
      { name = "Challenger: Mx. Singh",     icon = 4, gotoLevel = 5, state = 2 },
      { name = "Challenger: Mr. Singh",     icon = 5, gotoLevel = 6, state = 2 },
      { name = "Challenger: Piro Kavaliro", icon = 7, gotoLevel = 7, state = 2 },
      { name = "Challenger: Rose",          icon = 8, gotoLevel = 8, state = 2 },
      { name = "Challenger: Rebekah",       icon = 9, gotoLevel = 9, state = 2 },
    }

    self.data.lasers.levelStatuses = {
      -- -- 1 = locked, 2 = no stars, 3 = one star, 4 = two stars, 5 = three stars
      { name = "Level 1", icon = 1, gotoLevel = 1, state = 2 },
      { name = "Level 2", icon = 2, gotoLevel = 2, state = 2 },
      { name = "Level 3", icon = 3, gotoLevel = 3, state = 2 },
      { name = "Level 4", icon = 4, gotoLevel = 4, state = 2 },
      { name = "Level 5", icon = 5, gotoLevel = 5, state = 2 },
      { name = "Level 6", icon = 6, gotoLevel = 6, state = 2 },
      { name = "Level 7", icon = 7, gotoLevel = 7, state = 2 },
      { name = "Level 8", icon = 8, gotoLevel = 8, state = 2 },
      { name = "Level 9", icon = 9, gotoLevel = 9, state = 2 },
    }

    self.data.levelStatuses = {
      -- -- 1 = locked, 2 = no stars, 3 = one star, 4 = two stars, 5 = three stars
      { name = "Level 1", icon = 1, gotoLevel = 1, state = 2 },
      { name = "Level 2", icon = 2, gotoLevel = 2, state = 2 },
      { name = "Level 3", icon = 3, gotoLevel = 3, state = 2 },
      { name = "Level 4", icon = 4, gotoLevel = 4, state = 2 },
      { name = "Level 5", icon = 5, gotoLevel = 5, state = 2 },
      { name = "Level 6", icon = 6, gotoLevel = 6, state = 2 },
      { name = "Level 7", icon = 7, gotoLevel = 7, state = 2 },
      { name = "Level 8", icon = 8, gotoLevel = 8, state = 2 },
      { name = "Level 9", icon = 9, gotoLevel = 9, state = 2 },
    }

    self.data.finkit.levelStatuses = {
      -- 1 = locked, 2 = no stars, 3 = one star, 4 = two stars, 5 = three stars
      { name = "Level 1", icon = 1, gotoLevel = 1, state = 2 },
      { name = "Level 2", icon = 2, gotoLevel = 2, state = 2 },
      { name = "Level 3", icon = 3, gotoLevel = 3, state = 2 },
      { name = "Level 4", icon = 4, gotoLevel = 4, state = 2 },
      { name = "Level 5", icon = 5, gotoLevel = 5, state = 2 },
      { name = "Level 6", icon = 6, gotoLevel = 6, state = 2 },
      { name = "Level 7", icon = 7, gotoLevel = 7, state = 2 },
      { name = "Level 8", icon = 8, gotoLevel = 8, state = 2 },
      { name = "Level 9", icon = 9, gotoLevel = 9, state = 2 },
    }
  end,

  ResetData = function( self )
    self.data = {
      bass = {},
      crank = {},
      highfive = {},
      lasers = {},
      motor = {},
      finkit = {},
      pup = {},
      skele = {},
      tpcat = {},
    }

    self:ResetGameData()
    self:ResetUserData()
  end,

  GetHighScore = function( self, gameKey )
    if ( self.data[gameKey] ~= nil ) then
      return self.data[gameKey]["highscores"]
    end
    return nil
  end,

  SetHighScore = function( self, gameKey, highScoreInfo )
    self.data[gameKey]["highscores"] = highScoreInfo
    self:SaveData()
  end,

  LoadData = function( self )
    self.data = playdate.datastore.read()

    if ( self.data == nil ) then
      print( "No data found, reset game data" )
      self:ResetData()
    elseif ( self.data.version ~= currentVersion ) then
      print( "Reset game data due to version change" )
      self:ResetData()
    end
    print( "File version:", self.data.version )

    if ( build == "debug" ) then
      self:ResetData()
    end
  end,

  SaveData = function( self )
    playdate.datastore.write( self.data )
  end,
}

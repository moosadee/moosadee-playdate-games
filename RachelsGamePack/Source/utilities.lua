local gfx = playdate.graphics

function DoesTableContainElement( theTable, theElement )
  for key, element in ipairs( theTable ) do
    if ( element == theElement ) then
      return true
    end
  end
  return false
end

function DoesTableContainKey( theTable, theKey )
  return ( theTable[theKey] ~= nil )
end

function GetDistance( obj1, obj2 )
  local xDiff = obj1["x"] - obj2["x"]
  local yDiff = obj1["y"] - obj2["y"]
  local dist = math.sqrt( xDiff * xDiff + yDiff * yDiff )
  return dist
end

function GetCenterDistance( obj1, obj2 )
  local obj1CenterX = obj1["x"] + obj1["width"] / 2
  local obj1CenterY = obj1["y"] + obj1["height"] / 2

  local obj2CenterX = obj2["x"] + obj2["width"] / 2
  local obj2CenterY = obj2["y"] + obj2["height"] / 2

  local xDiff = obj1CenterX - obj2CenterX
  local yDiff = obj1CenterY - obj2CenterY
  local dist = math.sqrt( xDiff * xDiff + yDiff * yDiff )
  return dist
end

function GetDistance3D( obj1, obj2 )
  local xDiff = obj1["x"] - obj2["x"]
  local yDiff = obj1["y"] - obj2["y"]
  local zDiff = obj1["z"] - obj2["z"]
  local dist = math.sqrt( xDiff * xDiff + yDiff * yDiff + zDiff * zDiff )
  return dist
end

function GetBoundingBoxCollision( obj1, obj2 )
  local box1 = {
    left   = obj1.x + obj1.collisionRect.x,
    right  = obj1.x + obj1.collisionRect.x + obj1.collisionRect.width,
    top    = obj1.y + obj1.collisionRect.y,
    bottom = obj1.y + obj1.collisionRect.y + obj1.collisionRect.height }

  local box2 = {
    left   = obj2.x + obj2.collisionRect.x,
    right  = obj2.x + obj2.collisionRect.x + obj2.collisionRect.width,
    top    = obj2.y + obj2.collisionRect.y,
    bottom = obj2.y + obj2.collisionRect.y + obj2.collisionRect.height }

  return ( box1.left    < box2.right and
           box1.right   > box2.left and
           box1.top     < box2.bottom and
           box1.bottom  > box2.top )
end

function GetBoundingBoxCollisionFull( obj1, obj2 )
--  DebugTable( "obj1", obj1 )
--  DebugTable( "obj2", obj2 )
  local box1 = {
    left   = obj1.x,
    right  = obj1.x + obj1.width,
    top    = obj1.y,
    bottom = obj1.y + obj1.height }

  local box2 = {
    left   = obj2.x,
    right  = obj2.x + obj2.width,
    top    = obj2.y,
    bottom = obj2.y + obj2.height }

  return ( box1.left    < box2.right and
           box1.right   > box2.left and
           box1.top     < box2.bottom and
           box1.bottom  > box2.top )
end

function DebugLog( label, value )
  print( "DEBUG:", label, "=", value )
end

function DebugTable( label, theTable, level )
  if ( level == nil ) then level = 0 end

  space = ""
  spacer = "-"
  for i=0, level do
    space = space .. spacer
  end
  print( "" )
  print( space .. "DEBUG \"" .. tostring( label ) .. "\"" )
  for key, value in pairs( theTable ) do

    print( space .. space .. tostring( key ) .. "=" .. tostring( value ) )
    if ( type( value ) == "table" ) then
      -- RECURSE!!
      DebugTable( label .. "-" .. key, value, level+1 )
    end
  end
end

function DeleteItemsWithKeys( tableStructure, keyList )
  for i = #keyList, 1, -1 do
  --for k, deleteKey in pairs( keyList ) do
    local deleteKey = keyList[i]
    table.remove( tableStructure, deleteKey )
  end
end

function TryLoadImageTable( path )
  imgTable, result = gfx.imagetable.new( path )
  if ( result ~= nil ) then print( "ERROR:", result ) end
  return imgTable, result
end

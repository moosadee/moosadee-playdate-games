import 'CoreLibs/graphics.lua'
import 'global.lua'
local gfx = playdate.graphics

levelSelectScreen = {  
  ---------------------------------------------------------------------- INIT
  Init = function( self )
    self.nextState = ""
    self.id = "levelSelectScreen"
    
    self.gameData = dataManager["data"][global.gameKey]
    
    self.images = {
      background = gfx.image.new( "images/menubg.png" ),
      cursor     = gfx.image.new( self.gameData.cursor ),
      button     = gfx.image.new( "images/buttonbg.png" ),
    }
    
    self.tables = {}
    self.tables.levelButtons, result = TryLoadImageTable( "images/btnlevel" )
    self.tables.levelIcons, result = TryLoadImageTable( self.gameData.levelIcons )
    
    self.cursor = {
      width = 25,
      height = 25,
      positions = { 
      },
      currentPosition = 1
    }
    
    x = 43
    xInc = 131
    y = 24
    yInc = 54
    local index = 1
    
    self.buttons = {}
    
    -- Create level select buttons
    for key, level in pairs( self.gameData.levelStatuses ) do
      local newButton = {}
      newButton["type"] = "table"
      newButton["x"] = x
      newButton["y"] = y
      newButton["width"] = 50
      newButton["height"] = 50
      newButton["iconOffsetX"] = 50/2-30/2
      newButton["iconOffsetY"] = 5
      newButton["cursorOffsetX"] = -20
      newButton["cursorOffsetY"] = 20
      newButton["state"] = level.state
      newButton["icon"] = level.icon
      newButton["gotoLevel"] = level.gotoLevel
      newButton["gotoState"] = self.gameData.gameState
      
      if ( index - 1 > 0 ) then
        newButton.left = index - 1
      else
        newButton.left = index
      end
      
      if ( index + 1 < 11 ) then
        newButton.right = index + 1
      else
        newButton.right = index
      end
      
      if ( index - 3 < 0 ) then
        newButton.up = 10
      else
        newButton.up = index - 3
      end
      
      if ( index + 3 > 10 ) then
        newButton.down = 10
      else
        newButton.down = index + 3
      end
      
      table.insert( self.buttons, newButton )
      
      x += xInc
      if ( x > 400 ) then
        y += yInc
        x = 43
      end
      
      index += 1
    end
        
    -- Add back button
    local backButton = {
      text = "Back",
      type = "default",
      x = 10,
      y = 200,
      width = 180,
      height = 30,
      textOffsetX = 50,
      textOffsetY = 7,
      cursorOffsetX = 5,
      cursorOffsetY = 8,
      gotoState = "titleScreen",
      up = 7,
      down = 1,
      left = 9,
      right = 1
    }
    table.insert( self.buttons, backButton )
  end,
  
  ---------------------------------------------------------------------- CLEANUP
  Cleanup = function( self )
    -- Free space here as needed
    self.images = nil
    self.tables = nil
    self.buttons = nil
    self.cursor = nil
    self.nextState = nil
    self.id = nil
  end,
  
  ---------------------------------------------------------------------- UPDATE
  Update = function( self )
    self:HandleInput()
  end,
  
  ---------------------------------------------------------------------- DRAW
  Draw = function( self )
    gfx.setFont( fonts.pedallica )
    self.images.background:draw( 0, 0 )
    
    gfx.drawTextAligned( "LEVEL SELECT", 25, 3, kTextAlignment.left )
    
    -- Draw buttons
    for key, button in pairs( self.buttons ) do
      if ( button.type == "default" ) then
        -- Back button and such
        self.images.button:draw( button["x"], button["y"] )
        gfx.drawTextAligned( button["text"], button["x"] + button["textOffsetX"], button["y"] + button["textOffsetY"], kTextAlignment.left )
      
      else
        -- Level select buttons
        self.tables.levelButtons:getImage( button.state ):draw( button["x"], button["y"] )
        if ( button.state ~= 1 ) then
          self.tables.levelIcons:getImage( button.icon ):draw( button["x"] + button["iconOffsetX"], button["y"] + button["iconOffsetY"] )
        end
      end
    end
    
    -- Draw cursor
    local selection = self.cursor.currentPosition
    local button = self.buttons[ selection ]
    self.images.cursor:draw( button["x"] + button["cursorOffsetX"], button["y"] + button["cursorOffsetY"] )
  end,
  
  ---------------------------------------------------------------------- HANDLE INPUT
  HandleInput = function( self )
    if ( inputManager:IsKeyPressRelease( "down" ) ) then
      self:GoDown()
      
    elseif ( inputManager:IsKeyPressRelease( "up" ) ) then
      self:GoUp()
      
    elseif ( inputManager:IsKeyPressRelease( "right" ) ) then
      self:GoRight()
      
    elseif ( inputManager:IsKeyPressRelease( "left" ) ) then  
      self:GoLeft()
      
    elseif ( inputManager:IsKeyPressRelease( "a" ) ) then
      local cursorPosition = self.cursor["currentPosition"]
      local selection = self.cursor.currentPosition
      local button = self.buttons[ selection ]
      
      if ( button.state == 1 ) then
        -- Locked; play invalid noise
      else
        self.nextState = button["gotoState"]
        global.levelKey = button["gotoLevel"]
      end
    end
  end,
  
  ---------------------------------------------------------------------- GO DOWN
  GoDown = function( self )
    local selection = self.cursor.currentPosition
    local button = self.buttons[ selection ]
    self.cursor.currentPosition = button.down
  end,
  
  ---------------------------------------------------------------------- GO UP
  GoUp = function( self )
    local selection = self.cursor.currentPosition
    local button = self.buttons[ selection ]
    self.cursor.currentPosition = button.up
  end,
  
  ---------------------------------------------------------------------- GO LEFT
  GoLeft = function( self )
    local selection = self.cursor.currentPosition
    local button = self.buttons[ selection ]
    self.cursor.currentPosition = button.left
  end,
  
  ---------------------------------------------------------------------- GO RIGHT
  GoRight = function( self )
    local selection = self.cursor.currentPosition
    local button = self.buttons[ selection ]
    self.cursor.currentPosition = button.right
  end,
}

import 'CoreLibs/graphics.lua'
import 'global.lua'
local gfx = playdate.graphics

mainMenuScreen = {

  ---------------------------------------------------------------------- INIT
  Init = function( self )
    self.nextState = ""
    gfx.setFont( fonts.pedallica )

    self.key = "title"   -- A string identifier for this state

    self.images = {
      background = gfx.image.new( "images/menubgmain.png" ),
      cursor     = gfx.image.new( "images/cursor.png" ),
      button     = gfx.image.new( "images/buttonbgbig.png" ),
      rachel     = gfx.image.new( "images/rachel.png" ),
      prevTable = nil,
    }

    self.images.prevTable, result = gfx.imagetable.new( "images/previews" )
    if ( result ~= nil ) then print( "ERROR:", result ) end

    local spacer = 20

    -- gamekeys:
    -- tpcat
    -- bass
    -- skele
    -- lasers
    -- highfive
    -- motor
    -- crank
    -- pup
    -- finkit

    buttonIndex = 0
    buttonX = 25
    buttonY = 50 + spacer * buttonIndex

    self.buttons = {}
    table.insert( self.buttons,
      {
        text = tostring( buttonIndex+1 ) .. ". Deskapades", origIndex = 7,
        x = buttonX, y = buttonY, width = 180, height = 30,
        textOffsetX = 0, textOffsetY = 0, cursorOffsetX = -20,
        gotoState = "titleScreen", gameKey = "crank"
      } )
    buttonIndex += 1
    buttonY = 50 + spacer * buttonIndex

    table.insert( self.buttons,
      {
        text = tostring( buttonIndex+1 ) .. ". High Five King", origIndex = 5,
        x = buttonX, y = buttonY, width = 180, height = 30,
        textOffsetX = 0, textOffsetY = 0, cursorOffsetX = -20,
        gotoState = "titleScreen", gameKey = "highfive"
      } )
    buttonIndex += 1
    buttonY = 50 + spacer * buttonIndex

    table.insert( self.buttons,
      {
        text = tostring( buttonIndex+1 ) .. ". Pupreciation", origIndex = 8,
        x = buttonX, y = buttonY, width = 180, height = 30,
        textOffsetX = 0, textOffsetY = 0, cursorOffsetX = -20,
        gotoState = "titleScreen", gameKey = "pup"
      } )
    buttonIndex += 1
    buttonY = 50 + spacer * buttonIndex

    table.insert( self.buttons,
      {
        text = tostring( buttonIndex+1 ) .. ". We Have Lasers", origIndex = 4,
        x = buttonX, y = buttonY, width = 180, height = 30,
        textOffsetX = 0, textOffsetY = 0, cursorOffsetX = -20,
        gotoState = "titleScreen", gameKey = "lasers"
      } )
    buttonIndex += 1
    buttonY = 50 + spacer * buttonIndex

    table.insert( self.buttons,
      {
        text = tostring( buttonIndex+1 ) .. ". Bass Ball", origIndex = 2,
        x = buttonX, y = buttonY, width = 180, height = 30,
        textOffsetX = 0, textOffsetY = 0, cursorOffsetX = -20,
        gotoState = "titleScreen", gameKey = "bass"
      } )
    buttonIndex += 1
    buttonY = 50 + spacer * buttonIndex

    table.insert( self.buttons,
      {
        text = tostring( buttonIndex+1 ) .. ". Motor Horse", origIndex = 6,
        x = buttonX, y = buttonY, width = 180, height = 30,
        textOffsetX = 0, textOffsetY = 0, cursorOffsetX = -20,
        gotoState = "titleScreen", gameKey = "motor"
      } )
    buttonIndex += 1
    buttonY = 50 + spacer * buttonIndex

    table.insert( self.buttons,
      {
        text = tostring( buttonIndex+1 ) .. ". Fin 'N' Kit", origIndex = 9,
        x = buttonX, y = buttonY, width = 180, height = 30,
        textOffsetX = 0, textOffsetY = 0, cursorOffsetX = -20,
        gotoState = "titleScreen", gameKey = "finkit"
      } )
    buttonIndex += 1
    buttonY = 50 + spacer * buttonIndex

    table.insert( self.buttons,
      {
        text = tostring( buttonIndex+1 ) .. ". Toilet Paper Cat", origIndex = 1,
        x = buttonX, y = buttonY, width = 180, height = 30,
        textOffsetX = 0, textOffsetY = 0, cursorOffsetX = -20,
        gotoState = "titleScreen", gameKey = "tpcat"
      } )
    buttonIndex += 1
    buttonY = 50 + spacer * buttonIndex

    table.insert( self.buttons,
      {
        text = tostring( buttonIndex+1 ) .. ". Lairmail", origIndex = 3,
        x = buttonX, y = buttonY, width = 180, height = 30,
        textOffsetX = 0, textOffsetY = 0, cursorOffsetX = -20,
        gotoState = "titleScreen", gameKey = "skele"
      } )
    buttonIndex += 1
    buttonY = 50 + spacer * buttonIndex


    self.cursor = {
      width = 25,
      height = 25,
      positions = {
      },
      currentPosition = 1
    }
  end,

  ---------------------------------------------------------------------- CLEANUP
  Cleanup = function( self )
    -- Free space here as needed
    self.cursor = nil
    self.buttons = nil
    self.images = nil
    self.nextState = nil
    self.key = nil
  end,

  ---------------------------------------------------------------------- UPDATE
  Update = function( self )
    self:HandleInput()
  end,

  ---------------------------------------------------------------------- DRAW
  Draw = function( self )
    self.images.background:draw( 0, 0 )
    gfx.setFont( fonts.pedallica )
    gfx.drawTextAligned( "GAME PACK v" .. currentVersion, 400/2-23, 8, kTextAlignment.left )

    self.images.rachel:draw( 400-40, 240-40-14 )

    gfx.setFont( fonts.rains )
    gfx.setColor( gfx.kColorBlack )

    -- Draw preview of whichever game
    -- self.tables.player:getImage( self.player.handPosition ):draw( self.phase.playerX, 37 )
    local previewIndex = self.buttons[ self.cursor.currentPosition ][ "origIndex" ]
    self.images.prevTable:getImage( previewIndex ):draw( 175, 60 )
    gfx.drawRect( 175, 60, 210, 110 )
    gfx.drawRect( 175-2, 60-2, 210+4, 110+4 )

    gfx.drawTextAligned( "Thx for playing my game!",  350, 195, kTextAlignment.right )
    gfx.drawTextAligned( "Report bugs to",            350, 205, kTextAlignment.right )
    gfx.drawTextAligned( "moosadee.itch.io",          350, 215, kTextAlignment.right )

    gfx.setFont( fonts.roobert10 )
    -- Draw buttons
    for key, button in pairs( self.buttons ) do
      gfx.drawTextAligned( button["text"], button["x"] + button["textOffsetX"], button["y"] + button["textOffsetY"], kTextAlignment.left )
    end

    -- Draw cursor
    local selection = self.cursor.currentPosition
    local button = self.buttons[ selection ]
    self.images.cursor:draw( button["x"] + button["cursorOffsetX"], button["y"] )
  end,

  ---------------------------------------------------------------------- HANDLE INPUT
  HandleInput = function( self )
    if ( inputManager:IsKeyPressRelease( "down" ) ) then
      self.cursor["currentPosition"] = self.cursor["currentPosition"] + 1
      if ( self.cursor["currentPosition"] > #self.buttons ) then
        self.cursor["currentPosition"] = 1
      end

    elseif ( inputManager:IsKeyPressRelease( "up" ) ) then
      self.cursor["currentPosition"] = self.cursor["currentPosition"] - 1
      if ( self.cursor["currentPosition"] < 1 ) then
        self.cursor["currentPosition"] = #self.buttons
      end

    elseif ( inputManager:IsKeyPressRelease( "right" ) ) then
      self.cursor["currentPosition"] += 7 -- #self.buttons / 2
      if ( self.cursor["currentPosition"] > #self.buttons ) then
        self.cursor["currentPosition"] = 1
      end

    elseif ( inputManager:IsKeyPressRelease( "left" ) ) then
      self.cursor["currentPosition"] -= 7 -- #self.buttons / 2
      if ( self.cursor["currentPosition"] < 1 ) then
        self.cursor["currentPosition"] = #self.buttons
      end

    elseif ( inputManager:IsKeyPressRelease( "a" ) ) then
      local cursorPosition = self.cursor["currentPosition"]
      local selection = self.cursor.currentPosition
      local button = self.buttons[ selection ]
      self.nextState = button["gotoState"]
      global.gameKey = button["gameKey"]
    end
  end,
}

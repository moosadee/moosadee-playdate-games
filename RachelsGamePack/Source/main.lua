import 'manager_state.lua'
import 'manager_gamedata.lua'
import 'manager_input.lua'
import 'global.lua'
import 'utilities.lua'

local gfx = playdate.graphics
playdate.display.setRefreshRate( 30 )

gfx.setBackgroundColor( gfx.kColorWhite )

dataManager:LoadData()
dataManager:ResetGameData()

global.gameKey = "skele"
global.levelKey = 1
ChangeState( "mainMenuScreen" )
--ChangeState( "skele_gameScreen" )
--ChangeState( "tpcat_gameScreen" )

function playdate.update()
  UpdateState()
  DrawState()
  inputManager:Update()
end

function GoHome()
  ChangeState( "mainMenuScreen" )
  if ( global.song_playing ~= nil ) then
    global.song_playing:stop()
    global.song_playing = nil
  end
end

function GoToOptions()
  ChangeState( "optionsScreen" )
  if ( global.song_playing ~= nil ) then
    global.song_playing:stop()
    global.song_playing = nil
  end
end

function GoToCredits()
  ChangeState( "creditsScreen" )
  if ( global.song_playing ~= nil ) then
    global.song_playing:stop()
    global.song_playing = nil
  end
end
playdate.getSystemMenu():addMenuItem( "About", GoToCredits )
playdate.getSystemMenu():addMenuItem( "Options", GoToOptions )
playdate.getSystemMenu():addMenuItem( "Game select", GoHome )

-- Callbacks
function playdate.upButtonDown()    inputManager.Handle_upButtonDown()    end
function playdate.upButtonUp()      inputManager.Handle_upButtonUp()      end

function playdate.downButtonDown()  inputManager.Handle_downButtonDown()  end
function playdate.downButtonUp()    inputManager.Handle_downButtonUp()    end

function playdate.leftButtonDown()  inputManager.Handle_leftButtonDown()  end
function playdate.leftButtonUp()    inputManager.Handle_leftButtonUp()    end

function playdate.rightButtonDown() inputManager.Handle_rightButtonDown() end
function playdate.rightButtonUp()   inputManager.Handle_rightButtonUp()   end

function playdate.AButtonDown()     inputManager.Handle_AButtonDown()     end
function playdate.AButtonUp()       inputManager.Handle_AButtonUp()       end

function playdate.BButtonDown()     inputManager.Handle_BButtonDown()     end
function playdate.BButtonUp()       inputManager.Handle_BButtonUp()       end

import 'CoreLibs/graphics.lua'
import 'manager_gamedata.lua'
local gfx = playdate.graphics

helpScreen = {
  ---------------------------------------------------------------------- INIT
  Init = function( self )
    self.nextState  = ""
    self.gameData   = dataManager["data"][global.gameKey]
    --DebugLog( "gameKey", global.gameKey )

    self.key = "title"    -- A string identifier for this state

    self.images = {
      background = gfx.image.new( "images/menubg.png" ),
      button     = gfx.image.new( "images/buttonbg.png" ),
      cursor     = gfx.image.new( self.gameData.cursor ),
    }

    self.buttons = {
      {
        text = "Back",
        x = 10,
        y = 200,
        width = 180,
        height = 30,
        textOffsetX = 50,
        textOffsetY = 7,
        cursorOffsetX = -10,
        gotoState = "titleScreen"
      },
    }

    self.cursor = {
      width = 30,
      height = 30,
      currentPosition = 1
    }

    DebugTable( "gameData.help", self.gameData.help, 0 )
  end,

  ---------------------------------------------------------------------- CLEANUP
  Cleanup = function( self )
    -- Free space here as needed
    self.key = nil
    self.nextState = nil
    self.gameData = nil
    self.buttons = nil
    self.images = nil
    self.cursor = nil
  end,

  ---------------------------------------------------------------------- UPDATE
  Update = function( self )
    self:HandleInput()
  end,

  ---------------------------------------------------------------------- DRAW
  Draw = function( self )
    self.images.background:draw( 0, 0 )

    gfx.setFont( fonts.pedallica )
    gfx.drawTextAligned( "HELP SCREEN", 25, 3, kTextAlignment.left )

    -- Instructions text
    gfx.setFont( fonts.pedallica )
    x = 10
    y = 25
    inc = 25

    for key, instruction in pairs( self.gameData.help ) do
      gfx.drawText( instruction, x, y )
      y += inc
    end

    -- Draw buttons
    for key, button in pairs( self.buttons ) do
      self.images.button:draw( button["x"], button["y"] )
      gfx.drawTextAligned( button["text"], button["x"] + button["textOffsetX"], button["y"] + button["textOffsetY"], kTextAlignment.center )
    end

    -- Draw cursor
    local selection = self.cursor.currentPosition
    local button = self.buttons[ selection ]
    self.images.cursor:draw( button["x"] + button["cursorOffsetX"], button["y"] )
  end,

  ---------------------------------------------------------------------- HANDLE INPUT
  HandleInput = function( self )
    if ( inputManager:IsKeyPressRelease( "down" ) ) then
      self.cursor["currentPosition"] = self.cursor["currentPosition"] + 1
      if ( self.cursor["currentPosition"] > #self.buttons ) then
        self.cursor["currentPosition"] = 1
      end

    elseif ( inputManager:IsKeyPressRelease( "up" ) ) then
      self.cursor["currentPosition"] = self.cursor["currentPosition"] - 1
      if ( self.cursor["currentPosition"] == 0 ) then
        self.cursor["currentPosition"] = #self.buttons
      end

    elseif ( inputManager:IsKeyPressRelease( "a" ) ) then
      local cursorPosition = self.cursor["currentPosition"]
      local selection = self.cursor.currentPosition
      local button = self.buttons[ selection ]
      self.nextState = button["gotoState"]
    end
  end, -- HandleInput = function( self )
}

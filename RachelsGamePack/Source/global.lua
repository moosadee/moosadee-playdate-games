import 'CoreLibs/graphics.lua'
import 'manager_gamedata.lua'
local gfx = playdate.graphics
local snd = playdate.sound

fonts = {
  rains  = gfx.font.new( "fonts/font-rains-1x" ),
  sasser  = gfx.font.new( "fonts/Sasser-Slab" ),
  roobert  = gfx.font.new( "fonts/Roobert-20-Medium" ),
  roobert10 = gfx.font.new( "fonts/Roobert-10-Bold" ),
  nontendo  = gfx.font.new( "fonts/Nontendo-Bold" ),
  pedallica = gfx.font.new( "fonts/font-pedallica-fun-14" ),
}

global = {
  song_playing = nil,
  song_playing_name = nil
}

function PlaySong( song )
  -- If we're already playing the desired song,
  -- then ignore this
  print( "song", song )
  print( "global.song_playing_name", global.song_playing_name )

  if ( global.song_playing_name == song ) then
    return
  end

  -- If a song actually is playing right now, stop it.
  if ( global.song_playing ~= nil ) then
    global.song_playing:stop()
    global.song_playing = nil
    global.song_playing_name = ""
  end

  global.song_playing, result  = snd.fileplayer.new( song )
  if ( result ~= nil ) then print( "ERROR:", result ) end

  global.song_playing_name = song
  global.song_playing:setVolume( dataManager.data.music_volume / 100.0 )
  global.song_playing:play( 0 )
end

function StopSong()
  if ( global.song_playing ~= nil ) then
    global.song_playing:stop()
    global.song_playing = nil
    global.song_playing_name = ""
  end
end

import 'CoreLibs/graphics.lua'
import 'manager_gamedata.lua'
import 'manager_input.lua'
import 'global.lua'
local gfx = playdate.graphics
local snd = playdate.sound

pup_gameScreen = {  
  ---------------------------------------------------------------------- INIT
  Init = function( self )
    nextState = ""   -- Set this when you're ready to go to another state
    
    self.gameData = nil
    
    self.images = {
      background = gfx.image.new( "packages/pup/images/background.png" )
    }
    
    self.tables = {
      pups = nil,
      buttons = nil
    }
    
    self.fonts = {
      roobert  = gfx.font.new( "fonts/Roobert-20-Medium" ),
      nontendo  = gfx.font.new( "fonts/Nontendo-Bold" ),
      pedallica = gfx.font.new( "fonts/font-pedallica-fun-14" ),
    }
    
    self.cursor = {
      x = 52,
      y = 22,
      defaultX = 52,
      defaultY = 22,
      pupIndex = -1
    }
    
    self.audio = {
      reject = snd.sampleplayer.new( "audio/sfx4.wav" ),
      love = snd.sampleplayer.new( "audio/sfx3.wav" ),
    }
    
    self.nextState = ""
    self.gameData = dataManager["data"][global.gameKey]
    
    self.gameData.pups = {
          { image = 1,  dog = "Unknown",    person = "Wallace Yadis" },
          { image = 2,  dog = "Watson",     person = "klickaffen" },
          { image = 3,  dog = "Tally",      person = "hellopdpodcast" },
          { image = 4,  dog = "Rosie",      person = "Ollie Coe" },
          { image = 5,  dog = "Merlin",     person = "jeffamphetamines" },
          { image = 6,  dog = "Artemis",    person = "Kari Jane" },
          { image = 7,  dog = "Bernie",     person = "fosterdouglas" },
          { image = 8,  dog = "Boo Reedly", person = "Guv Bubbs" },
          { image = 9,  dog = "Buck",       person = "Raina Elmore" },
          { image = 10, dog = "Chulo",      person = "wo1vin" },
          { image = 11, dog = "Izzie",      person = "Justin Hiltz" },
          { image = 12, dog = "Buddy",      person = "Fyrhino" },
          { image = 13, dog = "Freya",      person = "Fyrhino" },
          { image = 14, dog = "Daisy",      person = "Fyrhino" },
          { image = 15, dog = "Freya",      person = "Grubrot" },
        }
    
    gfx.setFont( fonts.pedallica )
    
    self.tables["pups"], result = gfx.imagetable.new( "packages/pup/images/pup" )
    if ( result ~= nil ) then print( "ERROR:", result ) end
    
    self.tables["buttons"], result = gfx.imagetable.new( "packages/pup/images/button" )
    if ( result ~= nil ) then print( "ERROR:", result ) end
    
    --self.pupList = {}
    --for i=1, #self.tables["pups"] do
    --  table.insert( self.pupList, i )
    --end
    self:NextPup()
  end, -- Init = function( self )
  
  ---------------------------------------------------------------------- CLEANUP
  Cleanup = function( self )
    -- Free space here as needed, stop music
    self.fonts = nil
    self.nextState = nil
    self.gameData = nil
    self.tables = nil
    self.images = nil
  end, -- Cleanup = function( self )
  
  ---------------------------------------------------------------------- UPDATE
  Update = function( self )
    self:HandleInput()
  end, -- Update = function( self )
  
  ---------------------------------------------------------------------- DRAW
  Draw = function( self )    
    -- Draw background
    self.images.background:draw( 0, 0 )
    
    gfx.setColor( gfx.kColorWhite )
    gfx.fillRect( self.cursor.defaultX, self.cursor.defaultY, 296, 196 )
    
    gfx.setFont( fonts.pedallica )
    -- Draw current pup
    if ( #self.gameData.pups > 0 ) then
      self.tables.pups:getImage( self.gameData.pups[ self.cursor.pupIndex ].image ):draw( self.cursor.x, self.cursor.y )
      gfx.drawTextAligned( self.gameData.pups[ self.cursor.pupIndex ].dog, 50, 223, kTextAlignment.left )
      gfx.drawTextAligned( self.gameData.pups[ self.cursor.pupIndex ].person, 350, 223, kTextAlignment.right )
    else
      gfx.drawTextAligned( "That's all!", self.cursor.x, self.cursor.y, kTextAlignment.center )
    end
    
    gfx.drawTextAligned( "Pupreciation", 200, 2, kTextAlignment.center )
    
    -- Draw buttons5
    self.tables.buttons:getImage( 1 ):draw( 0, 68 )
    self.tables.buttons:getImage( 2 ):draw( 332, 68 )
  end, -- Draw = function( self )
  
  ---------------------------------------------------------------------- HANDLE INPUT
  HandleInput = function( self )
    inputManager:Handle_Crank()
    
    if     ( inputManager.crank.changeDegrees > 0 or inputManager.crank.changeDegrees < 0 ) then
      self.cursor.x += inputManager.crank.changeDegrees
    end
    
    if ( self.cursor.x > 400 ) then
      -- Love
      if ( self.audio.love:isPlaying() == false ) then
        self.audio.love:setVolume( dataManager.data.sound_volume / 100.0 )
        self.audio.love:play()
      end
      
      self.cursor.x = self.cursor.defaultX
      self.cursor.y = self.cursor.defaultY
      
      self:NextPup()
    elseif ( self.cursor.x < -296 ) then
      -- Reject
      if ( self.audio.reject:isPlaying() == false ) then
        self.audio.reject:setVolume( dataManager.data.sound_volume / 100.0 )
        self.audio.reject:play()
      end
      
      self.cursor.x = self.cursor.defaultX
      self.cursor.y = self.cursor.defaultY
      
      self:NextPup()
    end
  end,
  
  ---------------------------------------------------------------------- NEXT PUP
  NextPup = function( self )
    -- function DebugTable( label, tab, level )
    --print( "self.pupList size:", #self.pupList )
    --DebugTable( "self.pupList", self.pupList, 0 )
    print( "Pup index:", self.cursor.pupIndex )
    DebugTable( "self.gameData.pups", self.gameData.pups, 0 )
    if ( self.cursor.pupIndex ~= -1 ) then
      -- remove previous pup from the table
      --table.remove( self.pupList, self.cursor.pupIndex )
      table.remove( self.gameData.pups, self.cursor.pupIndex )
      print( "REMOVE INDEX", self.cursor.pupIndex, "FROM TABLE" )
    end
    
    if ( #self.gameData.pups > 0 ) then
      self.cursor.pupIndex = math.random( 1, #self.gameData.pups )
    else
      self.cursor.pupIndex = -1
    end
  end,
}

import 'CoreLibs/graphics.lua'
import 'manager_gamedata.lua'
import 'manager_input.lua'
local gfx = playdate.graphics
local snd = playdate.sound

motor_gameScreen = {
  ---------------------------------------------------------------------- INIT
  Init = function( self )
    self.nextState = ""   -- Set this when you're ready to go to another state

    self.gameData = nil

    self.images = {
      cloud   = gfx.image.new( "packages/motor/images/cloud.png" ),
      textbox = gfx.image.new( "images/textbox.png" ),
      shadow  = gfx.image.new( "images/transparent.png" ),
      win     = gfx.image.new( "packages/motor/images/win.png" ),
    }

    self.audio = {
      collect = snd.sampleplayer.new( "audio/sfx2.wav" ),
      success = snd.sampleplayer.new( "audio/sfx3.wav" ),
      failure = snd.sampleplayer.new( "audio/oof.wav" ),
      fan     = snd.sampleplayer.new( "audio/fan.wav" ),
      levelComplete = snd.sampleplayer.new( "audio/yay.wav" ),
    }

    self.tables = {
      horse = nil,
      birds = nil,
      elves = nil,
      santa = nil,
      bullet = nil,
      stars = nil,
    }

    self.obstacles = {
    }

    self.player = {}

    self.gravity = 0.05
    self.deAccRate = 1

    self.game = {}
    
    self.gameData = dataManager["data"][global.gameKey]

    self.tables.stars, result = TryLoadImageTable( "images/stars" )
    self.tables.horse, result = TryLoadImageTable( "packages/motor/images/horse" )
    self.tables.birds, result = TryLoadImageTable( "packages/motor/images/birds" )
    self.tables.elves, result = TryLoadImageTable( "packages/motor/images/elfs" )
    self.tables.santa, result = TryLoadImageTable( "packages/motor/images/santa" )
    self.tables.bullet, result = TryLoadImageTable( "images/bullet" )
    
    self.allLevelData = self:SetupLevelData()
    self.levelData = self.allLevelData[global.levelKey]
    
    self.gravity = self.levelData.gravity

    self.player = {
      x = 30,
      y = 100,
      width = 67,
      height = 91,
      physics = {
        acceleration = 0,
        accelerationMax = self.levelData.accelerationMax,
        accIncrement = self.levelData.accIncrement,
        velocity = 0,
        velocityMax = self.levelData.velocityMax,
      },
      frame = 1,
      frameSpeed = 0.5,
      frameMax = 3,
      rotation = 0,
      collisionRect = {
        x = -15, y = -17,
        width = 25, height = 40
      },
    }
    
    if ( self.levelData.startX ~= nil ) then
      self.player.x = self.levelData.startX
    end

    self.game = {
      level = global.levelKey,
      state = "level_intro",
      scrollX = 0,
      scrollSpeed = 0,
      instructions = {},
      deathAnimation = 0,
      dead = false,
      score = 0,
      scoreMax = 0,
      stars = 0
    }

    self:PrepareLevel()
    
    PlaySong( dataManager["data"][global.gameKey].song )
  end, -- Init = function( self )

  ---------------------------------------------------------------------- CLEANUP
  Cleanup = function( self )
    -- Free space here as needed, stop music
    StopSong()
    self.nextState = nil
    self.gameData = nil
    self.game = nil
    self.audio = nil
    self.tables = nil
    self.gravity = nil
    self.player = nil
    self.gravity = nil
    self.deAccRate = nil
  end, -- Cleanup = function( self )

  ---------------------------------------------------------------------- CREATE OBSTACLE
  CreateObstacle = function( self, type, x, y )
    obstacle = {}
    obstacle.x = x
    obstacle.y = y
    obstacle.type = type

    if ( type == "cloud" or type == "cloudborder" ) then
      obstacle.width = 50
      obstacle.height = 50
      obstacle.score = 0
      obstacle.subtype = "enemy"
      obstacle.collisionRect = {
        x = 10, y = 12,
        width = 30, height = 24
      }
      
    elseif ( type == "cloud2" ) then
      obstacle.width = 50
      obstacle.height = 50
      obstacle.score = 0
      obstacle.moveCounter = 0
      obstacle.moveCounterMax = 360
      obstacle.originalY = obstacle.y
      obstacle.subtype = "enemy"
      obstacle.collisionRect = {
        x = 10, y = 12,
        width = 30, height = 24
      }
      
    elseif ( type == "elf" ) then
      obstacle.width = 56
      obstacle.height = 64
      obstacle.score = 0
      obstacle.subtype = "enemy"
      obstacle.speed = -3
      obstacle.imageIndex = 1
      obstacle.collisionRect = {
        x = 19, y = 11,
        width = 27, height = 42
      }
      
    elseif ( type == "elf2" ) then
      obstacle.width = 56
      obstacle.height = 64
      obstacle.score = 0
      obstacle.subtype = "enemy"
      obstacle.speed = 0
      obstacle.imageIndex = 2
      obstacle.collisionRect = {
        x = 10, y = 11,
        width = 27, height = 42
      }
      
    elseif ( type == "santa" ) then
      obstacle.width = 108
      obstacle.height = 107
      obstacle.score = 0
      obstacle.subtype = "enemy"
      obstacle.imageIndex = 1
      obstacle.bulletCooldown = 0
      obstacle.collisionRect = {
        x = 0, y = 0,
        width = 108, height = 107
      }
      
    elseif ( type == "bullet" ) then
      obstacle.y += 50
      obstacle.width = 15
      obstacle.height = 15
      obstacle.frameCounter = 1
      obstacle.subtype = "enemy"
      obstacle.collisionRect = {
        x = 0, y = 0,
        width = 15, height = 15
      }
      
    elseif ( type == "bird1" ) then
      obstacle.width = 40
      obstacle.height = 40
      obstacle.score = 10
      obstacle.imageIndex = 1
      obstacle.subtype = "trinket"
      obstacle.collisionRect = {
        x = 10, y = 10,
        width = 20, height = 20
      }
      self.game.scoreMax += obstacle.score -- these are worth 10 points
      
    elseif ( type == "bird2" ) then
      obstacle.width = 40
      obstacle.height = 40
      obstacle.score = 20
      obstacle.imageIndex = 2
      obstacle.moveCounter = 0
      obstacle.moveCounterMax = 360
      obstacle.originalY = obstacle.y
      obstacle.subtype = "trinket"
      obstacle.collisionRect = {
        x = 10, y = 10,
        width = 20, height = 20
      }
      self.game.scoreMax += obstacle.score
      
    elseif ( type == "bird3" ) then
      obstacle.width = 40
      obstacle.height = 40
      obstacle.score = 50
      obstacle.imageIndex = 3
      obstacle.moveCounter = 0
      obstacle.moveCounterMax = 360
      obstacle.originalY = obstacle.y
      obstacle.subtype = "trinket"
      obstacle.collisionRect = {
        x = 10, y = 10,
        width = 20, height = 20
      }
      self.game.scoreMax += obstacle.score
    end
    table.insert( self.obstacles, obstacle )
  end,

  ---------------------------------------------------------------------- PREPARE LEVEL
  PrepareLevel = function( self )
    self.game.instructions = {}
    self.obstacles = {}

    local levelData = self.levelData --self.gameData.levels[ self.game.level ]

    self.game.scrollSpeed = levelData.scrollSpeed

    -- Generate a set of clouds
    -- Optimization: Don't create clouds for the entire length
    -- of the level, duh. Just do one screen worth and have them
    -- wrap around :P
    for x = -25, 450, 35 do
      self:CreateObstacle( "cloudborder", x, -25 )
      self:CreateObstacle( "cloudborder", x, 240-15 )
    end

    for key, obstacle in pairs( levelData.obstacles ) do
      self:CreateObstacle( obstacle.type, obstacle.x, obstacle.y )
    end

    for key, instruction in pairs( levelData.instructions ) do
      table.insert( self.game.instructions, instruction )
    end

    table.insert( self.game.instructions, "" )
    table.insert( self.game.instructions, "Press A to start" )
  end,

  ---------------------------------------------------------------------- BEGIN LEVEL
  BeginLevel = function( self )
    self.game.state = "play"
  end,

  ---------------------------------------------------------------------- UPDATE
  Update = function( self )
    self:HandleInput()

    if ( inputManager:IsKeyPressed( "a" ) and self.game.state == "level_intro" ) then
      self:BeginLevel()
    end

    if ( self.game.state == "play" ) then
      self:Update_Game_Physics()
    end
  end, -- Update = function( self )

  ---------------------------------------------------------------------- UPDATE GAME PHYSICS
  Update_Game_Physics = function( self )
    -- Dying
    if ( self.game.dead ) then
      self:DeathAnimation()
      return
    end
    
    self:ScreenScroll()
    self:UpdatePlayerMovement()
    self:UpdateObstacles()
  end,
  
  ---------------------------------------------------------------------- UPDATE PLAYER INPUT
  UpdatePlayerInput = function( self )
    local crankin = false
    
    if ( inputManager.crank.changeDegrees > 10 or inputManager.crank.changeDegrees < -10 ) then
      crankin = true
      self.player.physics.acceleration -= self.player.physics.accIncrement
    end
    
    if ( crankin ) then
      self.player.frame += self.player.frameSpeed
      if ( self.player.frame >= self.player.frameMax+1 ) then
        self.player.frame = 2
      end
      
      if ( self.audio.fan:isPlaying() == false ) then
        self.audio.fan:setVolume( dataManager.data.sound_volume / 100.0 )
        self.audio.fan:play()
      end
      
    else
      self.player.velocity = 0
      self.player.frame = 1
      
      if ( self.audio.fan:isPlaying() == true ) then
        self.audio.fan:stop()
      end
    end
  end,
  
  ---------------------------------------------------------------------- UPDATE PLAYER MOVEMENT
  UpdatePlayerMovement = function( self )
    -- Update the player velocity
    self.player.physics.velocity += self.player.physics.acceleration
    
    -- Update the player position
    self.player.y += self.player.physics.velocity
  
    -- Gravity always affects the player!
    self.player.physics.acceleration += self.gravity
    
    -- Limit acceleration
    if ( self.player.physics.acceleration > self.player.physics.accelerationMax ) then
      self.player.physics.acceleration = self.player.physics.accelerationMax
    elseif ( self.player.physics.acceleration < -self.player.physics.accelerationMax ) then
      self.player.physics.acceleration = -self.player.physics.accelerationMax
    end
    
    -- Limit velocity
    if ( self.player.physics.velocity > self.player.physics.velocityMax ) then
      self.player.physics.velocity = self.player.physics.velocityMax
    elseif ( self.player.physics.velocity < -self.player.physics.velocityMax ) then
      self.player.physics.velocity = -self.player.physics.velocityMax
    end

  end,
  
  ---------------------------------------------------------------------- DEATH ANIMATION
  DeathAnimation = function( self )
    self.player.rotation += 10
    self.player.y += 5
    self.player.x += 5
    self.game.deathAnimation -= 1

    if ( self.game.deathAnimation == 0 ) then
      -- Return to level select
      self.nextState = "levelSelectScreen"
    end
  end,
  
  ---------------------------------------------------------------------- SCREEN SCROLL
  ScreenScroll = function( self )
    local levelData = self.levelData --self.gameData.levels[ self.game.level ]
    -- Scroll level
    if ( -self.game.scrollX < levelData.levelWidth ) then
      self.game.scrollX += self.game.scrollSpeed
      
    else
      self.player.x += -self.game.scrollSpeed
    end

    -- Is player off screen?
    if ( self.player.x > 425 ) then
      self:PlayerWin()
    end
  end,
    
  ---------------------------------------------------------------------- UPDATE OBSTACLES
  UpdateObstacles = function( self )
    local levelData = self.levelData --self.gameData.levels[ self.game.level ]
    -- Obstacle update
    for key, obstacle in pairs( self.obstacles ) do
      -- Scroll 'em
      if ( -self.game.scrollX < levelData.levelWidth ) then
        obstacle.x += self.game.scrollSpeed
        
        -- Loop clouds back around when they go off screen
        if ( obstacle.type == "cloudborder" and obstacle.x < -50 ) then
          obstacle.x += 500
        end
      end

      -- Move character based on behavior
      if ( obstacle.x > -50 and obstacle.x < 450 ) then
        if ( obstacle.type == "bird2" ) then
          
          if ( obstacle.moveCounter < obstacle.moveCounterMax/2 ) then
            obstacle.y -= 0.5
          else
            obstacle.y += 0.5
          end
          
          obstacle.moveCounter += 5
          
        elseif ( obstacle.type == "bird3" ) then
          
          if ( obstacle.moveCounter < obstacle.moveCounterMax/2 ) then
            obstacle.y -= 1
          else
            obstacle.y += 1
          end
          
          obstacle.x -= 1 -- additional quick movement
          obstacle.moveCounter += 5
          
        elseif ( obstacle.type == "cloud2" ) then

          if ( obstacle.moveCounter < obstacle.moveCounterMax/2 ) then
            obstacle.y -= 1
          else
            obstacle.y += 1
          end
          
          obstacle.moveCounter += 5
          
        elseif ( obstacle.type == "elf" ) then
          obstacle.x += obstacle.speed -- additional quick movement
          
        elseif ( obstacle.type == "elf2" ) then
          obstacle.x -= self.game.scrollSpeed -- No movement horizontally
          
          if ( self.player.y < obstacle.y ) then
            obstacle.y -= 1
          elseif ( self.player.y > obstacle.y ) then
            obstacle.y += 1
          end
          
        elseif ( obstacle.type == "santa" ) then
          obstacle.x -= self.game.scrollSpeed -- No movement horizontally
          
          local playerMidpoint = self.player.y + self.player.height / 2
          local santaMidpoint = obstacle.y + obstacle.collisionRect.height / 2
                      
          if ( obstacle.bulletCooldown <= 0
                and (santaMidpoint <= 120-15
                or santaMidpoint >= 120+15 )
                ) then
            -- Shoot at the horse
            obstacle.bulletCooldown = 100 --- TEMP RE ADD
            self:CreateObstacle( "bullet", obstacle.x, obstacle.y )
          end
          
          if ( playerMidpoint < santaMidpoint ) then
            obstacle.y -= 0.5
          elseif ( playerMidpoint > santaMidpoint ) then
            obstacle.y += 0.5
          end
          
          if ( obstacle.bulletCooldown > 0 ) then
            obstacle.bulletCooldown -= 1
          end
          
        elseif ( obstacle.type == "bullet" ) then
          obstacle.x -= self.game.scrollSpeed/2 -- Make it slower
          obstacle.frameCounter += 0.1
          if ( obstacle.frameCounter >= 3 ) then
            obstacle.frameCounter = 1
          end
        end
      end
  
      if ( obstacle.moveCounter ~= nil and obstacle.moveCounter > obstacle.moveCounterMax ) then
        obstacle.moveCounter = 0
      end
      
      -- Check collision with obstacles
      local collision = GetBoundingBoxCollision( self.player, obstacle )
      if ( collision == true and string.find( obstacle.subtype, "enemy" ) ) then
        self:PlayerDie()
      elseif ( collision == true and ( string.find( obstacle.subtype, "trinket" ) ) ) then
        self:CollectTrinket( key )
      end
    end

    -- Remove obstacles that are off screen
    for key, obstacle in pairs( self.obstacles ) do
      if ( obstacle.x <= -100 ) then
        self.obstacles[key] = nil
      end
    end
  end,
  
  ---------------------------------------------------------------------- COLLECT TRINKET
  CollectTrinket = function( self,  key )
    -- Move it off-screen so it will be deleted
    self.obstacles[key].x = -100
    self.game.score += self.obstacles[key].score

    -- Play happy sound
    if ( self.audio.collect:isPlaying() == false ) then
      self.audio.collect:setVolume( dataManager.data.sound_volume / 100.0 )
      self.audio.collect:play()
    end
  end,

  ---------------------------------------------------------------------- PLAYER DIE
  PlayerDie = function( self )
    self.game.deathAnimation = 50
    self.game.dead = true

    if ( self.audio.failure:isPlaying() == false ) then
      self.audio.failure:setVolume( dataManager.data.sound_volume / 100.0 )
      self.audio.failure:play()
    end
  end,

  ---------------------------------------------------------------------- PLAYER WIN
  PlayerWin = function( self )
    -- Status and end
    local levelData = self.levelData --self.gameData.levels[ self.game.level ]

    self.game.stars = 0
    
    -- Full score for collecting all the trinkets
    if ( self.game.score >= self.game.scoreMax ) then
      self.game.stars = 3
    elseif ( self.game.score >= self.game.scoreMax/2 ) then
      self.game.stars = 2
    else
      self.game.stars = 1
    end

    -- 1 = locked, 2 = no stars, 3 = one star, 4 = two stars, 5 = three stars
    local score = self.game.stars + 2

    ---- Save score
    if ( dataManager["data"][global.gameKey]["levelStatuses"][global.levelKey].state < score ) then
      dataManager["data"][global.gameKey]["levelStatuses"][global.levelKey].state = score
    end
    
    -- Unlock next level
    if ( dataManager["data"][global.gameKey]["levelStatuses"][global.levelKey+1] ~= nil and dataManager["data"][global.gameKey]["levelStatuses"][global.levelKey+1].state == 1 ) then
      dataManager["data"][global.gameKey]["levelStatuses"][global.levelKey+1].state = 2
    end
    
    dataManager:SaveData()

    if ( self["audio"]["success"]:isPlaying() == false ) then
      self["audio"]["success"]:setVolume( dataManager.data.sound_volume / 100.0 )
      self["audio"]["success"]:play()
    end

    self.game.state = "level_end"
  end,

  ---------------------------------------------------------------------- DRAW
  Draw = function( self )
    local this = motor_gameScreen

    gfx.setColor( gfx.kColorBlack )
    gfx.fillRect( 0, 0, 400, 240 )

    if ( self.game.state == "level_intro" ) then
      self:Draw_LevelIntro()
    elseif ( self.game.state == "play" ) then
      self:Draw_Game()
    elseif ( self.game.state == "level_end" ) then
      self:Draw_LevelEnd()
    elseif ( self.game.state == "game_end" ) then
      self:Draw_GameEnd()
    end
  end, -- Draw = function( self )

  ---------------------------------------------------------------------- DRAW LEVEL INTRO
  Draw_LevelIntro = function( self )
    self.images.textbox:draw( 50, 30 )
    gfx.setFont( fonts.pedallica )
    gfx.drawTextAligned( "LEVEL " .. tostring( self.game.level ), 400/2, 50, kTextAlignment.center )

    local y = 90
    local inc = 20

    for key, inst in pairs( self.game.instructions ) do
      gfx.drawTextAligned( inst, 400/2, y, kTextAlignment.center )
      y += inc
    end
  end,

  ---------------------------------------------------------------------- DRAW GAME
  Draw_Game = function( self )
    -- Draw horse
    local horseImage = self.tables.horse:getImage( math.floor( self.player.frame ) )
    horseImage:drawRotated( self.player.x, self.player.y, self.player.rotation )

    -- Draw collision rectangles (TEMP)
    --gfx.setColor( gfx.kColorXOR )
    --gfx.drawRect( self.player.x + self.player.collisionRect.x, self.player.y + self.player.collisionRect.y, self.player.collisionRect.width, self.player.collisionRect.height )

    -- Draw obstacles
    for key, obstacle in pairs( self.obstacles ) do
      -- Only draw if it's on screen!
      if ( obstacle.x > -50 and obstacle.x < 450 ) then

          if ( string.find( obstacle.type, "cloud" ) ) then
            self.images.cloud:draw( obstacle.x, obstacle.y )
          elseif ( string.find( obstacle.type, "bird" ) ) then
            self.tables.birds:getImage( obstacle.imageIndex ):draw( obstacle.x, obstacle.y )
          elseif ( string.find( obstacle.type, "elf" ) ) then
            self.tables.elves:getImage( obstacle.imageIndex ):draw( obstacle.x, obstacle.y )
          elseif ( string.find( obstacle.type, "santa" ) ) then
            self.tables.santa:getImage( obstacle.imageIndex ):draw( obstacle.x, obstacle.y )
          elseif ( string.find( obstacle.type, "bullet" ) ) then
            self.tables.bullet:getImage( math.floor( obstacle.frameCounter ) ):draw( obstacle.x, obstacle.y )
          end

          -- Collision rectangle (TEMP)
          --gfx.setColor( gfx.kColorXOR )
          --gfx.drawRect( obstacle.x + obstacle.collisionRect.x, obstacle.y + obstacle.collisionRect.y, obstacle.collisionRect.width, obstacle.collisionRect.height )
      end
    end

    -- HUD
    gfx.setFont( fonts.rains )
    gfx.setColor( gfx.kColorWhite )
    gfx.fillRect( 0, 0, 400, 11 )
    gfx.setColor( gfx.kColorXOR )
    gfx.drawText( "Score " .. tostring( self.game.score ) .. "/" .. tostring( self.game.scoreMax), 1, 1 )
    playdate.drawFPS( 380, 1 )
  end,

  ---------------------------------------------------------------------- DRAW LEVEL END
  Draw_LevelEnd = function( self )
    self.images.shadow:draw( 0, 0 )
    self.images.textbox:draw( 50, 30 )

    -- Display level summary
    gfx.setFont( fonts.pedallica )
    gfx.drawTextAligned( "SUCCESS!", 400/2, 50, kTextAlignment.center )
    gfx.drawTextAligned( "Press 'A' to continue!" , 400/2, 175, kTextAlignment.center )
    
    -- Draw stars
    local starWidth = 32
    self.tables.stars:getImage( 1 ):draw( 400/2 - (starWidth*2) - (starWidth/2), 100 )
    
    if ( self.game.stars >= 2 ) then
      self.tables.stars:getImage( 1 ):draw( 400/2 - (starWidth/2), 100 )
    else
      self.tables.stars:getImage( 2 ):draw( 400/2 - (starWidth/2), 100 )
    end
    
    if ( self.game.stars >= 3 ) then
      self.tables.stars:getImage( 1 ):draw( 400/2 + (starWidth*2) - (starWidth/2), 100 )
    else
      self.tables.stars:getImage( 2 ):draw( 400/2 + (starWidth*2) - (starWidth/2), 100 )
    end
  end,

  ---------------------------------------------------------------------- DRAW GAME END
  Draw_GameEnd = function( self )
    self.images.win:draw( 0, 0 )  
  end,

  ---------------------------------------------------------------------- HANDLE INPUT
  HandleInput = function( self )
    inputManager:Handle_Crank()

    if ( self.game.state == "play" ) then
      self:UpdatePlayerInput()
            
    elseif ( self.game.state == "level_end" ) then
      if ( inputManager:IsKeyPressRelease( "a" ) ) then
        if ( self.game.level == 9 ) then
          self.game.state = "game_end"
        else
          self.nextState = "levelSelectScreen"
        end
      end
      
    elseif ( self.game.state == "game_end" ) then
      if ( inputManager:IsKeyPressRelease( "a" ) ) then
        self.nextState = "levelSelectScreen"
      end

    end
  end, -- HandleInput = function( self )
  
  
  ---------------------------------------------------------------------- SETUP LEVEL DATA
  SetupLevelData = function( self )
    levels = {
          { 
            instructions = {
              "I am SpyHorse. I stealthily float thru",
              "the clouds using my propeller backpack.",
              "I crank to go up, and stop to go down.",
              "Sometimes, I grab a bird for a snack."
            },
            scrollSpeed = -3,
            levelWidth = 600,
            gravity = 0.05,
            accelerationMax = 0.5,
            velocityMax = 1,
            accIncrement = 0.2,
            obstacles = {
              { type = "bird1", x = 400, y = 100 },
              { type = "bird1", x = 500, y = 150 },
              { type = "bird1", x = 600, y = 100 },
              { type = "bird1", x = 700, y = 50 },
              { type = "bird1", x = 800, y = 100 },
            },
          },
          {
            instructions = {
              "My job is tough, but I am a professional.",
              "I must reach my goal and find information.",
              "I must navigate around these noxious",
              "clouds, toward my destination..."
            },
            scrollSpeed = -4,
            levelWidth = 1000,
            gravity = 0.05,
            accelerationMax = 0.5,
            velocityMax = 1,
            accIncrement = 0.2,
            obstacles = {
              { type = "cloud", x = 200, y = 175, },
              { type = "cloud", x = 400, y = 25,  },
              { type = "bird1", x = 400, y = 100 },

              { type = "cloud", x = 600, y = 175, },
              { type = "cloud", x = 600, y = 150, },
              { type = "bird1", x = 600, y = 100 },

              { type = "cloud", x = 900, y = 25,  },
              { type = "cloud", x = 900, y = 50,  },
              { type = "bird1", x = 900, y = 100 },

              { type = "bird1", x = 1000, y = 100 },
              { type = "bird1", x = 1100, y = 140 },
              { type = "bird1", x = 1200, y = 180 },
              { type = "bird1", x = 1300, y = 140 },
            },
          },
          {
            instructions = {
              "Birds are my only sustenance up here",
              "in the cold, unwelcoming sky.",
              "All birds are",
              "equally delicious."
            },
            scrollSpeed = -4,
            levelWidth = 1400,
            gravity = 0.05,
            accelerationMax = 0.5,
            velocityMax = 1,
            accIncrement = 0.2,
            obstacles = {
              { type = "cloud", x = 200, y = 175, },
              { type = "cloud", x = 225, y = 150, },
              { type = "cloud", x = 250, y = 175, },

              { type = "bird2", x = 400, y = 120 },

              { type = "cloud", x = 600, y = 25, },
              { type = "cloud", x = 625, y = 50, },
              { type = "cloud", x = 650, y = 25, },

              { type = "bird2", x = 800, y = 120 },

              { type = "bird1", x = 1200, y = 120 },
              { type = "bird1", x = 1300, y = 175 },
              { type = "bird1", x = 1400, y = 120 },
              { type = "bird1", x = 1500, y = 75 },
              { type = "bird1", x = 1600, y = 120 },
            },
          },
          {
            instructions = {
              "I miss my wife and children, but",
              "I cannot go back until I've",
              "helped ensure the success of",
              "our mission..."
            },
            scrollSpeed = -4,
            levelWidth = 2000,
            gravity = 0.05,
            accelerationMax = 0.5,
            velocityMax = 1,
            accIncrement = 0.2,
            obstacles = {
              { type = "cloud", x = 400, y = 175, },
              { type = "cloud", x = 400, y = 25, },

              { type = "bird1", x = 600, y = 120 },
              { type = "bird1", x = 700, y = 70 },
              { type = "bird1", x = 800, y = 70 },

              { type = "cloud", x = 800, y = 150, },
              { type = "cloud", x = 800, y = 175, },
              { type = "cloud", x = 800, y = 200, },

              { type = "bird1", x = 900,  y = 70 },
              { type = "bird1", x = 1000, y = 120 },
              { type = "bird1", x = 1150, y = 150 },
              { type = "bird1", x = 1300, y = 170 },
              { type = "bird1", x = 1400, y = 170 },

              { type = "cloud", x = 1400, y = 25, },
              { type = "cloud", x = 1400, y = 50, },
              { type = "cloud", x = 1400, y = 75, },

              { type = "bird1", x = 1700, y = 170 },
              { type = "bird1", x = 1800, y = 120 },
              { type = "bird1", x = 1900, y = 120 },

              { type = "cloud", x = 2000, y = 25, },
              { type = "cloud", x = 2000, y = 50, },
              { type = "cloud", x = 2000, y = 200, },
              { type = "cloud", x = 2000, y = 175, },

              { type = "bird2", x = 2200, y = 120 },
            },
          },
          {
            instructions = {
              "So far no sight of the enemy.",
              "That is comforting.",
              "Just me, some birds,",
              "and these poisonous clouds. Ugh."
            },
            scrollSpeed = -5,
            levelWidth = 3000,
            gravity = 0.05, accelerationMax = 0.5, velocityMax = 1, accIncrement = 0.2,
            obstacles = {
              { type = "cloud2",  x = 500,  y = 40, },
              { type = "cloud2",  x = 550,  y = 40, },
              { type = "cloud2",  x = 600,  y = 40, },
              { type = "bird3",   x = 800,  y = 120 },

              { type = "cloud2",  x = 1000, y = 170, },
              { type = "cloud2",  x = 1050, y = 170, },
              { type = "cloud2",  x = 1100, y = 170, },
              { type = "bird3",   x = 1300, y = 120 },

              { type = "bird1",   x = 1500, y = 120 },
              { type = "bird1",   x = 1600, y = 100 },
              { type = "bird1",   x = 1700, y = 90 },
              { type = "cloud2",  x = 1700, y = 170, },
              { type = "cloud2",  x = 1750, y = 160, },
              { type = "cloud2",  x = 1800, y = 170, },

              { type = "bird1",   x = 1900, y = 100 },
              { type = "bird1",   x = 2000, y = 120 },
              { type = "bird1",   x = 2100, y = 140 },
              { type = "cloud2",  x = 2100, y = 50, },
              { type = "cloud2",  x = 2150, y = 60, },
              { type = "cloud2",  x = 2200, y = 70, },

              { type = "bird2",   x = 2400, y = 120 },
              { type = "bird2",   x = 2450, y = 140 },
              { type = "bird2",   x = 2500, y = 120 },
              { type = "bird2",   x = 2550, y = 100 },

              { type = "bird2",   x = 3000, y = 120 },
              { type = "bird2",   x = 3100, y = 140 },
              { type = "bird2",   x = 3200, y = 120 },
              { type = "bird2",   x = 3350, y = 100 },
            },
          },
          {
            instructions = {
              "I'm getting closer to my destination.",
              "Patrols are out, watching for",
              "intruders. I need to avoid them."
            },
            scrollSpeed = -5,
            levelWidth = 2500,
            gravity = 0.05, accelerationMax = 0.5, velocityMax = 1, accIncrement = 0.2,
            obstacles = {
              { type = "cloud",  x = 450,  y = 80, },
              { type = "elf",    x = 600,  y = 60, },

              { type = "cloud",  x = 1000,  y = 180, },
              { type = "cloud",  x = 1025,  y = 160, },
              { type = "cloud",  x = 1050,  y = 140, },
              { type = "elf",    x = 1200,  y = 160, },

              { type = "bird1",  x = 1400,  y = 60 },
              { type = "cloud",  x = 1600,  y = 40, },
              { type = "cloud",  x = 1650,  y = 60, },
              { type = "cloud",  x = 1700,  y = 80, },
              { type = "elf",    x = 1600,  y = 150, },

              { type = "bird1",  x = 2000,  y = 160 },
              { type = "cloud",  x = 2200,  y = 180, },
              { type = "cloud",  x = 2250,  y = 160, },
              { type = "cloud",  x = 2300,  y = 140, },
              { type = "elf",    x = 2200,  y = 50, },

              { type = "bird2", x = 2700, y = 120 },
            }
          },
          {
            instructions = {
              "I secured the intel,",
              "but the alarm was tripped.",
              "I have to get this data",
              "back to HQ, far from here."
            },
            scrollSpeed = -6,
            levelWidth = 3400,
            gravity = 0.1, accelerationMax = 1, velocityMax = 2, accIncrement = 0.2,
            startX = 150,
            obstacles = {
              { type = "elf2",   x = 0,  y = 120, },

              { type = "cloud2", x = 500,    y = 10, },
              { type = "elf",    x = 1000,   y = 20, },

              { type = "cloud2", x = 1000,  y = 200, },
              { type = "elf",    x = 1500,  y = 180, },

              { type = "cloud",  x = 1750,  y = 10, },
              { type = "elf",    x = 2000,  y = 20, },

              { type = "cloud2", x = 2250,  y = 10, },
              { type = "cloud2", x = 2250,  y = 200, },
              { type = "elf",    x = 2500,  y = 20, },
              { type = "elf",    x = 2500,  y = 180, },

              { type = "cloud2", x = 2800,  y = 180, },
              { type = "cloud2", x = 2750,  y = 200, },
              { type = "bird1",  x = 2900,  y = 40 },
              { type = "elf",    x = 3000,  y = 100, },
              { type = "elf",    x = 3000,  y = 180, },

              { type = "cloud2", x = 3100,  y = 180, },
              { type = "cloud2", x = 3150,  y = 200, },
              { type = "bird1",  x = 3300,  y = 140 },
              { type = "elf",    x = 3400,  y = 60, },
              { type = "elf",    x = 3400,  y = 20, },
            }
          },
          {
            instructions = {
              "I can't return to HQ now.",
              "It's too hot.",
              "I need to shake these elves."
            },
            scrollSpeed = -7,
            levelWidth = 3000,
            startX = 100,
            gravity = 0.1, accelerationMax = 1, velocityMax = 2, accIncrement = 0.2,
            obstacles = {
              { type = "elf2",   x = 0,     y = 20, },
              { type = "elf2",   x = 20,    y = 200, },

              { type = "bird3",  x = 500,   y = 120 },

              { type = "bird1",  x = 750,   y = 60 },
              { type = "bird1",  x = 1000,  y = 40 },
              { type = "cloud",  x = 1000,  y = 180, },
              { type = "cloud",  x = 1000,  y = 150, },
              { type = "cloud",  x = 1000,  y = 120, },

              { type = "bird1",  x = 1550,   y = 150 },
              { type = "bird1",  x = 1650,   y = 170 },
              { type = "cloud",  x = 1800,   y = 80, },
              { type = "cloud",  x = 1800,   y = 50, },
              { type = "cloud",  x = 1800,   y = 20, },

              { type = "bird1",  x = 2000,   y = 1100 },
              { type = "bird1",  x = 2100,   y = 1100 },
              { type = "bird1",  x = 2200,   y = 1100 },
              { type = "cloud",  x = 2200,   y = 20, },
              { type = "cloud",  x = 2250,   y = 40, },
              { type = "cloud",  x = 2300,   y = 40, },
              { type = "cloud",  x = 2350,   y = 40, },
              --{ type = "cloud",  x = 2200,   y = 180, },
              --{ type = "cloud",  x = 2250,   y = 160, },
              --{ type = "cloud",  x = 2300,   y = 160, },
              --{ type = "cloud",  x = 2350,   y = 160, },

              { type = "cloud",  x = 2400,   y = 60, },
              { type = "cloud",  x = 2450,   y = 60, },
              { type = "cloud",  x = 2500,   y = 60, },
              --{ type = "cloud",  x = 2400,   y = 180, },
              --{ type = "cloud",  x = 2450,   y = 180, },
              --{ type = "cloud",  x = 2500,   y = 180, },

              --{ type = "cloud",  x = 2550,   y = 80, },
              --{ type = "cloud",  x = 2600,   y = 80, },
              --{ type = "cloud",  x = 2650,   y = 80, },
              { type = "cloud",  x = 2550,   y = 200, },
              { type = "cloud",  x = 2600,   y = 200, },
              { type = "cloud",  x = 2650,   y = 200, },

              --{ type = "cloud",  x = 2700,   y = 60, },
              --{ type = "cloud",  x = 2750,   y = 60, },
              --{ type = "cloud",  x = 2800,   y = 60, },
              { type = "cloud",  x = 2700,   y = 180, },
              { type = "cloud",  x = 2750,   y = 180, },
              { type = "cloud",  x = 2800,   y = 180, },

              { type = "cloud",  x = 2850,   y = 30, },
              { type = "cloud",  x = 2900,   y = 30, },
              { type = "cloud",  x = 2950,   y = 30, },
              { type = "cloud",  x = 2850,   y = 170, },
              { type = "cloud",  x = 2900,   y = 170, },
              { type = "cloud",  x = 2950,   y = 170, },

              { type = "bird1",  x = 3100,   y = 110 },
            }
          },
          {
            instructions = {
              "It's hard to write morse code",
              "while fleeing enemies in the sky.",
              "I hope they got my message.",
              "I don't think I will make it back."
            },
            scrollSpeed = -7,
            startX = 100,
            levelWidth = 4000,
            --gravity = 0.05, accelerationMax = 0.5, velocityMax = 1, accIncrement = 0.2,
            gravity = 0.1, accelerationMax = 1, velocityMax = 2, accIncrement = 0.2,
            obstacles = {
              { type = "elf2",    x = 0,     y = 20, },
              { type = "santa",   x = 292,   y = 120, },

              { type = "cloud",  x = 1000,  y = 180, },
              { type = "cloud",  x = 1000,  y = 150, },

              { type = "cloud",  x = 1500,  y = 20, },
              { type = "cloud",  x = 1500,  y = 50, },

              { type = "cloud",  x = 2000,  y = 180, },
              { type = "cloud",  x = 2000,  y = 150, },

              { type = "cloud",  x = 2250,  y = 20, },
              { type = "cloud",  x = 2250,  y = 50, },

              { type = "cloud",  x = 2750,  y = 180, },
              { type = "cloud",  x = 2750,  y = 150, },

              { type = "cloud",  x = 3000,  y = 180, },
              { type = "cloud",  x = 3000,  y = 20, },

              { type = "cloud",  x = 3250,  y = 180, },
              { type = "cloud",  x = 3500,  y = 20, },

              { type = "cloud",  x = 3750,  y = 170, },
              { type = "cloud",  x = 3750,  y = 30, },

              { type = "cloud",  x = 4200,  y = 100, },
            }
          },
        }
    return levels
  end,
}

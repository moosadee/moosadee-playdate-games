import 'CoreLibs/graphics.lua'
import 'manager_gamedata.lua'
import 'manager_input.lua'
local gfx = playdate.graphics
local snd = playdate.sound

highfive_gameScreen = {    
  ---------------------------------------------------------------------- INIT
  Init = function( self )
    self.nextState = ""   -- Set this when you're ready to go to another state
    
    self.gameData = nil
    
    self.images = {
      background = gfx.image.new( "packages/highfive/images/background.png" ),
      smack = gfx.image.new( "packages/highfive/images/smack.png" ),
      textbg = gfx.image.new( "packages/highfive/images/textbg.png" ),
      textbox = gfx.image.new( "images/textbox.png" ),
      textbox_small = gfx.image.new( "images/textbox-small.png" ),
      textbox_medium = gfx.image.new( "images/textbox-medium.png" ),
      fail = gfx.image.new( "packages/highfive/images/fail.png" ),
      success = gfx.image.new( "packages/highfive/images/yeah.png" ),
      early = gfx.image.new( "packages/highfive/images/early.png" ),
      wait = gfx.image.new( "packages/highfive/images/wait.png" ),
      now = gfx.image.new( "packages/highfive/images/now.png" ),
      speech_balloon = gfx.image.new( "packages/highfive/images/speech_balloon.png" ),
    }
    
    self.tables = {
      player = nil,
      challenger = nil,
      stars = nil,
    }
    
    self.audio = {
      punch = snd.sampleplayer.new( "audio/punch.wav" ),
      oof = snd.sampleplayer.new( "audio/oof.wav" ),
    }
    
    self.player = {
      handPosition = 1, -- 1 = down, 2 = halfway, 3 = high five
      wins = 0,
    }
    
    self.challenger = {
      handPosition = 1, -- 1 = intro, 2 = ready, 3 = high five, 4 = success, 5 = failure
      timeToHighFive = 0,
      timeToWait = 0,
      wins = 0
    }
    
    self.game = {
      state = "reset_position",
      round = 0,
      cooldown = 0
    }
    
    self.gameData = dataManager["data"][global.gameKey]
    self.allLevelData = self:SetupLevelData()
    self.levelData = self.allLevelData[global.levelKey]
    
    self.tables.stars, result = TryLoadImageTable( "images/stars" )
    self.tables.player, result = TryLoadImageTable( "packages/highfive/images/player" )
    self.tables.challenger, result = TryLoadImageTable( "packages/highfive/images/challenger_" .. self.levelData.image )
    
    
    self:RoundPhase1_ChallengerIntroduction_Init()
    
    PlaySong( dataManager["data"][global.gameKey].song )
  end, -- Init = function( self )
  
  ---------------------------------------------------------------------- CLEANUP
  Cleanup = function( self )
    -- Free space here as needed, stop music
    StopSong()
    self.tables = nil
    self.images = nil
    self.audio = nil
    self.gameData = nil
    self.levelData = nil
    self.game = nil
    self.challenger = nil
    self.nextState = nil
  end, -- Cleanup = function( self )
  
  ---------------------------------------------------------------------- UPDATE
  Update = function( self )
    self:HandleInput()
        
    if ( self.phase.name == "challenger_introduction" ) then
      self:RoundPhase1_ChallengerIntroduction()
    elseif ( self.phase.name == "introduction" ) then
      self:RoundPhase2_Introduction()
    elseif ( self.phase.name == "play" ) then
      self:RoundPhase3_Play()
    elseif ( self.phase.name == "wonround" ) then
      self:RoundPhase4_WonRound()
    elseif ( self.phase.name == "tooearly" ) then
      self:RoundPhase4_TooEarly()
    elseif ( self.phase.name == "toolate" ) then
      self:RoundPhase4_TooLate()
    elseif ( self.phase.name == "challenge_end" ) then
      self: RoundPhase5_ChallengeEnd()
    end
  end, -- Update = function( self )
  
  ---------------------------------------------------------------------- PHASE 1
  RoundPhase1_ChallengerIntroduction_Init = function( self )
    self.phase = {}
    self.phase.name = "challenger_introduction"
    self.phase.counter = 0
    self.phase.counterMax = 150
    self.phase.x = -300
    self.phase.challengerX = -214
    self.phase.playerX = -214
    
    self.player.wins = 0
    self.challenger.wins = 0
  end,
  
  RoundPhase1_ChallengerIntroduction = function( self )
    -- Display
    if ( self.phase.counter < 70 ) then
      self.phase.x += 5
    elseif ( self.phase.counter > 70 and self.phase.counter < 90 ) then
      -- No movement
    else
      self.phase.x += 5
    end  
    
    if ( self.phase.challengerX < 186 ) then
      self.phase.challengerX += 10
      if ( self.phase.challengerX > 186 ) then
        self.phase.challengerX = 186
      end
    end
    
    if ( self.phase.playerX < 0 ) then
      self.phase.playerX += 5
      if ( self.phase.playerX > 0 ) then
        self.phase.playerX = 0
      end
    end
      
    self.images.background:draw( 0, 0 )
    self.tables.challenger:getImage( 2 ):draw( self.phase.challengerX, 35 )
    self.tables.player:getImage( self.player.handPosition ):draw( self.phase.playerX, 37 )
    
    gfx.setColor( gfx.kColorWhite )
    gfx.fillRect( 0, 0, 400, 40 )
    
    gfx.setFont( fonts.sasser )
    gfx.drawTextAligned( dataManager["data"][global.gameKey]["levelStatuses"][global.levelKey].name, self.phase.x + 284/2, 10, kTextAlignment.center )
        
    -- Counter update
    self.phase.counter += 1
    if ( self.phase.counter > self.phase.counterMax or inputManager:IsKeyPressRelease( "a" ) ) then
      -- Next phase
      self:RoundPhase2_Introduction_Init()
    end
  end,
  
  ---------------------------------------------------------------------- PHASE 2
  RoundPhase2_Introduction_Init = function( self )
    self.phase = {}
    self.phase.name = "introduction"
    self.phase.talkIndex = math.random( 1, #self.levelData.instructions )
    
    if ( self.player.wins == 2 or self.challenger.wins == 2 ) then
      self:RoundPhase5_ChallengeEnd_Init()
    end
  end,
  
  RoundPhase2_Introduction = function( self )
    self.images.background:draw( 0, 0 )  
    
    self.tables.challenger:getImage( 1 ):draw( 186, 35 )
    self.tables.player:getImage( self.player.handPosition ):draw( 0, 37 )
    
    self.images.speech_balloon:draw( 0, 0 )  
    gfx.setFont( fonts.pedallica )

    
    gfx.drawTextAligned( self.levelData.instructions[ self.phase.talkIndex ], 400/2, 30, kTextAlignment.center )
    
    gfx.setColor( gfx.kColorBlack )
    gfx.fillRect( 120-107, 200-12, 214, 49 )
    gfx.setColor( gfx.kColorWhite )
    gfx.fillRect( 120-105, 200-10, 210, 45 )
    gfx.drawTextAligned( "Move crank DOWN.", 120, 195, kTextAlignment.center )
    gfx.drawTextAligned( "and press A to begin.", 120, 215, kTextAlignment.center )
    
    -- Go to next phase
    if ( inputManager:IsKeyPressRelease( "a" ) and self.player.handPosition == 1 ) then
      self:RoundPhase3_Play_Init()
    end
  end,
  
  ---------------------------------------------------------------------- PHASE 3
  RoundPhase3_Play_Init = function( self )
    self.phase = {}
    self.phase.name = "play"
    self.challenger.timeToHighFive = self.levelData.timeToHighFive
    self.challenger.timeToWait = self.levelData.timeToWait
    self.challenger.handPosition = 2
  end,
  
  RoundPhase3_Play = function( self )
    -- High five timers count down
    if ( self.challenger.timeToHighFive > 0 ) then
      self.challenger.timeToHighFive -= 1
    elseif ( self.challenger.timeToWait > 0 ) then
      self.challenger.handPosition = 3
      self.challenger.timeToWait -= 1
    end
    
    -- Check for early high-five
    if ( self.player.handPosition == 3 and self.challenger.timeToHighFive > 0 ) then
      -- TOO EARLY!
      self:RoundPhase4_TooEarly_Init()
    end
    
    -- Check for appropriately timed high-five
    if ( self.player.handPosition == 3 and self.challenger.timeToHighFive <= 0 and self.challenger.timeToWait > 0 ) then
      -- GOT IT!
      self:RoundPhase4_WonRound_Init()
    end
    
    -- Check for too late high-five
    if ( self.challenger.timeToWait <= 0 ) then
      -- TOO LATE!
      self:RoundPhase4_TooLate_Init()
    end
    
    -- Drawing    
    self.images.background:draw( 0, 0 )  
    
    self.tables.challenger:getImage( self.challenger.handPosition ):draw( 186, 35 )
    self.tables.player:getImage( self.player.handPosition ):draw( 0, 37 )
    
    -- HUD
    gfx.setColor( gfx.kColorWhite )
    gfx.fillRect( 0, 0, 400, 20 )
    gfx.setFont( fonts.pedallica )
    gfx.drawTextAligned( "Your wins: " .. tostring( self.player.wins ), 10, 2, kTextAlignment.left )
    gfx.drawTextAligned( self.levelData.challenger .. "'s wins: " .. tostring( self.challenger.wins ), 390, 2, kTextAlignment.right )
    gfx.drawTextAligned( "Round: " .. tostring( self.game.round+1 ), 150, 2, kTextAlignment.center )
  end,
  
  ---------------------------------------------------------------------- PHASE 4
  RoundPhase4_WonRound_Init = function( self )
    self.phase = {}
    self.phase.name = "wonround"
    self.challenger.handPosition = 4
    self.player.wins += 1
    self["audio"]["punch"]:setVolume( dataManager.data.sound_volume / 100.0 )
    self["audio"]["punch"]:play()
  end,
  
  RoundPhase4_WonRound = function( self )
    self.images.background:draw( 0, 0 )      
    self.tables.challenger:getImage( self.challenger.handPosition ):draw( 186, 35 )
    self.tables.player:getImage( self.player.handPosition ):draw( 0, 37 )
    
    self.images.success:draw( 115, 15 )
    
    gfx.setColor( gfx.kColorBlack )
    gfx.fillRect( 120-107, 200-12, 214, 34 )
    gfx.setColor( gfx.kColorWhite )
    gfx.fillRect( 120-105, 200-10, 210, 30 )
    gfx.drawTextAligned( "Press A to continue.", 120, 200, kTextAlignment.center )
    
    -- Go to next phase
    if ( inputManager:IsKeyPressRelease( "a" ) ) then
      self.game.round += 1
      self:RoundPhase2_Introduction_Init()
    end
  end,
  
  RoundPhase4_TooEarly_Init = function( self )
    self.phase = {}
    self.phase.name = "tooearly"
    self.challenger.wins += 1
    self.challenger.handPosition = 5
    self["audio"]["oof"]:setVolume( dataManager.data.sound_volume / 100.0 )
    self["audio"]["oof"]:play()
  end,
  
  RoundPhase4_TooEarly = function( self )
    self.images.background:draw( 0, 0 )      
    self.tables.challenger:getImage( self.challenger.handPosition ):draw( 186, 35 )
    self.tables.player:getImage( self.player.handPosition ):draw( 0, 37 )
    self.images.early:draw( 10, 6 )
    
    gfx.setColor( gfx.kColorBlack )
    gfx.fillRect( 120-107, 200-12, 214, 34 )
    gfx.setColor( gfx.kColorWhite )
    gfx.fillRect( 120-105, 200-10, 210, 30 )
    gfx.drawTextAligned( "Press A to continue.", 120, 200, kTextAlignment.center )
    
    -- Go to next phase
    if ( inputManager:IsKeyPressRelease( "a" ) ) then
      self.game.round += 1
      self:RoundPhase2_Introduction_Init()
    end
  end,
  
  RoundPhase4_TooLate_Init = function( self )
    self.phase = {}
    self.phase.name = "toolate"
    self.challenger.wins += 1
    self.challenger.handPosition = 5
    self["audio"]["oof"]:setVolume( dataManager.data.sound_volume / 100.0 )
    self["audio"]["oof"]:play()
  end,
  
  RoundPhase4_TooLate = function( self )
    self.images.background:draw( 0, 0 )      
    self.tables.challenger:getImage( self.challenger.handPosition ):draw( 186, 35 )
    self.tables.player:getImage( self.player.handPosition ):draw( 0, 37 )
    self.images.fail:draw( 104, 16 )
    
    gfx.setColor( gfx.kColorBlack )
    gfx.fillRect( 120-107, 200-12, 214, 34 )
    gfx.setColor( gfx.kColorWhite )
    gfx.fillRect( 120-105, 200-10, 210, 30 )
    gfx.drawTextAligned( "Press A to continue.", 120, 200, kTextAlignment.center )
    
    -- Go to next phase
    if ( inputManager:IsKeyPressRelease( "a" ) ) then
      self.game.round += 1
      self:RoundPhase2_Introduction_Init()
    end
  end,
  
  ---------------------------------------------------------------------- PHASE 5
  RoundPhase5_ChallengeEnd_Init = function( self )
    self.phase = {}
    self.phase.name = "challenge_end"
  end,
  
  RoundPhase5_ChallengeEnd = function( self )
    self.images.background:draw( 0, 0 )      
    self.tables.challenger:getImage( self.challenger.handPosition ):draw( 186, 35 )
    self.tables.player:getImage( self.player.handPosition ):draw( 0, 37 )
    
    if ( self.player.wins == 2 ) then -- player won
      self.tables.challenger:getImage( 4 ):draw( 186, 35 )
    else -- challenger won
      self.tables.challenger:getImage( 5 ):draw( 186, 35 )
    end
    
    self.tables.player:getImage( self.player.handPosition ):draw( 0, 37 )
    
    self.images.speech_balloon:draw( 0, 0 )  
    gfx.setFont( fonts.pedallica )
    
    if ( self.player.wins == 2 ) then
      gfx.drawTextAligned( self.levelData.player_win, 400/2, 30, kTextAlignment.center )
    else
      gfx.drawTextAligned( self.levelData.player_lose, 400/2, 30, kTextAlignment.center )
    end
    
    -- Save score
    -- 1 = locked, 2 = no stars, 3 = one star, 4 = two stars, 5 = three stars
    local score = 0
    if ( self.player.wins == 2 and self.challenger.wins == 0 ) then
      score = 5 -- 3 stars
    elseif ( self.player.wins >= 2 and self.challenger.wins > 0 ) then
      score = 4 -- 2 stars
    else
      score = 3 -- 1 star for... I guess completing the challenge? :P
    end
    
    ---- Save score
    if ( dataManager["data"][global.gameKey]["levelStatuses"][global.levelKey].state < score ) then
      dataManager["data"][global.gameKey]["levelStatuses"][global.levelKey].state = score
    end
    
    -- Unlock next level
    if ( dataManager["data"][global.gameKey]["levelStatuses"][global.levelKey+1] ~= nil and dataManager["data"][global.gameKey]["levelStatuses"][global.levelKey+1].state == 1 ) then
      dataManager["data"][global.gameKey]["levelStatuses"][global.levelKey+1].state = 2
    end
    
    dataManager:SaveData()
    
    
    gfx.setColor( gfx.kColorBlack )
    gfx.fillRect( 120-107, 200-12, 214, 34 )
    gfx.setColor( gfx.kColorWhite )
    gfx.fillRect( 120-105, 200-10, 210, 30 )
    gfx.drawTextAligned( "Press A to continue.", 120, 200, kTextAlignment.center )
    
    -- Go to next phase
    if ( inputManager:IsKeyPressRelease( "a" ) ) then
      self.nextState = "levelSelectScreen"
    end
  end,
  
  
  ---------------------------------------------------------------------- DRAW
  Draw = function( self )
    -- Not using this
  end,  
  
  ---------------------------------------------------------------------- BEGIN HIGH FIVE
  BeginHighFive = function( self )
    self.challenger.handPosition = 2
    
  end,
  
  ---------------------------------------------------------------------- HANDLE INPUT
  HandleInput = function( self )
    inputManager:Handle_Crank()
    
    if ( inputManager.crank.angleDegrees > 90 and inputManager.crank.angleDegrees < 270 ) then
      self.player.handPosition = 1
    elseif ( inputManager.crank.angleDegrees > 45 and inputManager.crank.angleDegrees < 315 ) then
      self.player.handPosition = 2
    else
      self.player.handPosition = 3
    end
  end, -- HandleInput = function( self )
  
  
  ---------------------------------------------------------------------- SETUP LEVEL DATA
  SetupLevelData = function( self )
    levels = {
          {
            challenger = "Ozha",
            image = "ozha",
            timeToHighFive = 50,
            timeToWait = 100,
            instructions = {
              "Win or lose, let's just have fun!",
              "Where's the hand sanitizer? Better safe than sorry...",
            },
            player_win = "You win! Good job!",
            player_lose = "It's OK, you'll win next time!",
          },
          {
            challenger = "Ayda",
            image = "ayda",
            timeToHighFive = 75,
            timeToWait = 75,
            instructions = {
              "Are you a bad enough dude to challenge ME?",
              "Is it lunch time yet?",
              "I think I left the stove on at home."
            },
            player_win = "OK, I guess you win. It's fine.",
            player_lose = "Wow... You really need to practice more.",
          },
          {
            challenger = "Rawr",
            image = "rawr",
            timeToHighFive = 50,
            timeToWait = 50,
            instructions = {
              "Rawr?",
              "Rawr.",
              "Roar?"
            },
            player_win = "Raawwrrrr...",
            player_lose = "Ra-warrr?",
          },
          {
            challenger = "Luha",
            image = "luha",
            timeToHighFive = 40,
            timeToWait = 40,
            instructions = {
              "I'm here I guess.",
              "I was just following Ayda here.",
              "I don't understand this game.",
            },
            player_win = "You won. I'm going to find Ayda.",
            player_lose = "Wow. I can't believe you lost.",
          },
          {
            challenger = "Mx. Singh",
            image = "mxsingh",
            timeToHighFive = 25,
            timeToWait = 50,
            instructions = {
              "I can't code while cats are on my PC.",
              "Hang on, I need to test this on the hardware.",
              "To refactor, or to not refactor...",
              "It's really cool that you downloaded my game."
            },
            player_win = "OK you won, now my system will be safe.",
            player_lose = "Looks like YOU have some bugs.",
          },
          {
            challenger = "Mr. Singh",
            image = "mrsingh",
            timeToHighFive = 50,
            timeToWait = 25,
            instructions = {
              "Wait, this isn't CSGO...",
              "Where are my cats?",
              "I'm going to make parathas for dinner."
            },
            player_win = "You win! Time to cuddle kitties!!",
            player_lose = "I think my cats could do a better job.",
          },
          {
            challenger = "Piro Kavaliro",
            image = "piro",
            timeToHighFive = 40,
            timeToWait = 20,
            instructions = {
              "Mi batalis drakon!",
              "Mi chiam venkas!!",
              "Mi volas dormi!"
            },
            player_win = "Ho, ve.",
            player_lose = "Mi sukcesas!!",
          },
          {
            challenger = "Rose",
            image = "rose",
            timeToHighFive = 30,
            timeToWait = 20,
            instructions = {
              "Have an idea for a drawing?",
              "Maybe I'll practice my 3D modeling.",
              "Today's a good day for clay!",
            },
            player_win = "You  might have what it takes!",
            player_lose = "Well, back to the drawing board.",
          },
          {
            challenger = "Rebekah",
            image = "rebekah",
            timeToHighFive = 50,
            timeToWait = 15,
            instructions = {
              "Programmers drool, QAs rule!",
              "Where's the nearest taco truck??",
              "Where's my coffee? I'm falling asleep here!"
            },
            player_win = "Clearly there's a bug in the game...",
            player_lose = "Your PR is DENIED!",
          },
        }
    return levels
  end,
}

import 'CoreLibs/graphics.lua'
import 'CoreLibs/crank.lua'
import 'CoreLibs/sprites'
import 'CoreLibs/animation'
import 'CoreLibs/timer'
import 'manager_gamedata.lua'
import 'manager_input.lua'
local gfx = playdate.graphics
local snd = playdate.sound

tpcat_gameScreen = {
  ---------------------------------------------------------------------- INIT
  Init = function( self )  
    self.key = "title"    -- A string identifier for this state
    self.nextState = ""   -- Set this when you're ready to go to another state

    self.imgBackground = gfx.image.new( "packages/tpcat/images/background.png" )
    self.imgCat        = gfx.image.new( "packages/tpcat/images/catbase.png" )
    self.imgTpHolder   = gfx.image.new( "packages/tpcat/images/tpholder.png" )
    self.imgTpHolder2  = gfx.image.new( "packages/tpcat/images/tpholder2.png" )
    self.imgSuccess    = gfx.image.new( "packages/tpcat/images/youdidit.png" )
    self.imgShade      = gfx.image.new( "images/transparent.png" )
    self.imgTextbox    = gfx.image.new( "images/textbox.png" )
    
    self.armsTable = nil
    self.tpTable = nil
    self.sheetTable = nil
    self.countdownTable = nil
    
    self.soundRoll = snd.sampleplayer.new( "audio/sfx1.wav" )
    self.soundCount = snd.sampleplayer.new( "audio/sfx2.wav" )
    self.soundFinish = snd.sampleplayer.new( "audio/sfx3.wav" )
    self.music = nil
    
    self.safetyButtonDown = false

    self.buttonPressing = {
      up    = false,
      down  = false,
      left  = false,
      right = false,
      a     = false,
      b     = false
    }

    self.data = {
      rotateCounter = 1,
      rotateCounterMax = 2,
      totalDelta = 0,
      increment = 1000,
      finalValue = 5000,
      tpOffset = 0,
      phase = "countdown",
      secondCounter = 0,
      timer = nil,
      audioCounter = 0,
    }    
    
    self.gameData = dataManager["data"][global.gameKey]
    self.currentHighScore = dataManager:GetHighScore( global.gameKey )
    
    -- Initialize any data that must be reset every time we return to this state here
    self.buttonPressing["up"] = false
    self.buttonPressing["down"] = false
    self.buttonPressing["left"] = false
    self.buttonPressing["a"] = false
    self.buttonPressing["b"] = false
    
    self.data = {
      rotateCounter = 1,
      rotateCounterMax = 2,
      totalDelta = 0,
      finalValue = 3000,
      tpOffset = 0,
      phase = "countdown",
      secondCounter = 0,
      timer = nil,
      audioCounter = 0,
    }

    self.armsTable, result     = gfx.imagetable.new( "packages/tpcat/images/arms" )
    if ( result ~= nil ) then print( "ERROR:", result ) end

    self.tpTable, result       = gfx.imagetable.new( "packages/tpcat/images/tp" )
    if ( result ~= nil ) then print( "ERROR:", result ) end

    self.sheetTable, result    = gfx.imagetable.new( "packages/tpcat/images/sheet" )
    if ( result ~= nil ) then print( "ERROR:", result ) end
    print( "Sheet size:", #self.sheetTable )

    self.countdownTable, result    = gfx.imagetable.new( "packages/tpcat/images/countdown" )
    if ( result ~= nil ) then print( "ERROR:", result ) end

    if ( self.soundRoll == nil ) then print( "ERROR: soundRoll is null!" ) end
    if ( self.soundCount == nil ) then print( "ERROR: soundCount is null!" ) end

    -- Set up second timer
    self.soundCount:setVolume( dataManager.data.sound_volume / 100.0 )
    self.soundCount:play()
    self.data.timer = playdate.timer.new( 1000, tpcat_gameScreen.SecondTimer )
    
    PlaySong( self.gameData.song )
  end,

  ---------------------------------------------------------------------- CLEANUP
  Cleanup = function( self )
    -- Free space here as needed
    for key, timery in pairs( playdate.timer.allTimers() ) do
      timery:remove()
    end
    self.key = nil
    self.nextState = nil
    self.data = nil
    self.buttonPressing = nil
    self.sheetTable = nil
    self.imgBackground = nil
    self.imgCat        = nil
    self.imgTpHolder   = nil
    self.imgTpHolder2  = nil
    self.imgSuccess    = nil
    self.imgShade      = nil
    self.armsTable = nil
    self.tpTable = nil
    self.sheetTable = nil
    self.countdownTable = nil
    self.soundRoll    = nil
    self.soundCount   = nil
    self.soundFinish  = nil
    self.music = nil
  end,

  ---------------------------------------------------------------------- UPDATE
  Update = function( self )
    self:HandleInput()
    
    if ( self.safetyButtonDown == false ) then
      return
    end
    
    if ( self.data.secondCounter >= 3 and self.data.phase == "countdown" ) then
      self.data.secondCounter = 0
      self.data.phase = "play"
    end
    
    playdate.timer.updateTimers()
  end,

  ---------------------------------------------------------------------- DRAW
  Draw = function( self )
    local this = tpcat_gameScreen
    tpcat_gameScreen.imgBackground:draw( 0, 0 )
    tpcat_gameScreen.imgCat:draw( 186, 39 )
    tpcat_gameScreen.imgTpHolder:draw( 83, 68 )
    
    -- HUD and text
    gfx.setColor( gfx.kColorWhite )
    gfx.fillRect( 0, 0, 400, 20 )
    gfx.drawText( "TIME: " .. tostring( tpcat_gameScreen.data.secondCounter ) , 0, 0 )
    gfx.drawText( "DONE: " .. ( math.floor( tpcat_gameScreen.data.totalDelta / tpcat_gameScreen.data.finalValue * 100 ) ) .. "%", 300, 0 )

    if ( self.currentHighScore ~= nil and type( self.currentHighScore ) ~= "table" ) then
      gfx.drawTextAligned( "(HIGH SCORE: " .. tostring( self.currentHighScore ) .. ")", 100, 0 , kTextAlignment.left )
    end

    if ( tpcat_gameScreen.data.phase == "countdown" ) then
    
      tpcat_gameScreen.sheetTable:getImage(1):draw( 113, 71 )
      tpcat_gameScreen.tpTable:getImage(1):draw( 90, 30 )
      
      tpcat_gameScreen.imgShade:draw( 0, 0 )
      
      if ( tpcat_gameScreen.data.secondCounter < 3 ) then
        tpcat_gameScreen.countdownTable:getImage( math.floor( tpcat_gameScreen.data.secondCounter ) + 1 ):drawCentered( 200, 120 )
      end
      
    elseif ( tpcat_gameScreen.data.phase == "play" ) then
      tpcat_gameScreen.sheetTable:getImage(tpcat_gameScreen.data.rotateCounter):draw( 113, 71 )
      tpcat_gameScreen.tpTable:getImage(tpcat_gameScreen.data.rotateCounter + tpcat_gameScreen.data.tpOffset):draw( 90, 30 )
      tpcat_gameScreen.armsTable:getImage(tpcat_gameScreen.data.rotateCounter):draw( 145, 51 )
      tpcat_gameScreen.imgTpHolder2:draw( 83, 68 )

    elseif ( tpcat_gameScreen.data.phase == "gameover" ) then
      tpcat_gameScreen.tpTable:getImage(tpcat_gameScreen.data.tpOffset):draw( 90, 30 )
      tpcat_gameScreen.armsTable:getImage(1):draw( 145, 51 )
      tpcat_gameScreen.imgSuccess:drawCentered( 200, 120 )

      gfx.drawTextAligned( "TIME:" .. tostring( tpcat_gameScreen.data.secondCounter ) .. " seconds", 200, 172, kTextAlignment.center )
      gfx.drawTextAligned( "PRESS ANY BUTTON", 200, 189, kTextAlignment.center )
      gfx.drawTextAligned( "TO GO BACK TO THE MAIN MENU", 200, 205, kTextAlignment.center )
    end
    
    if ( self.safetyButtonDown == false ) then
      self:ControlReminder()
    end

  end,

  ---------------------------------------------------------------------- TRIGGER GAME OVER
  Trigger_GameOver = function( self )  
    self.data.tpOffset = 5
    self.data.phase = "gameover"
    self.soundFinish:setVolume( dataManager.data.sound_volume / 100.0 )
    self.soundFinish:play()
    
    -- Check for high score    
    local highscore = dataManager:GetHighScore( global.gameKey )
    if ( highscore ~= nil and type( highscore ) ~= "table" ) then
      if ( self.data.secondCounter <= highscore ) then
        dataManager:SetHighScore( global.gameKey, self.data.secondCounter )
      end
    else
      dataManager:SetHighScore( global.gameKey, self.data.secondCounter )
    end
  end,
  
  ---------------------------------------------------------------------- SECOND TIMER
  SecondTimer = function( self )
    if ( tpcat_gameScreen.data.phase == "countdown" ) then
      if ( tpcat_gameScreen.soundCount ~= nil ) then 
        tpcat_gameScreen.soundCount:setVolume( dataManager.data.sound_volume / 100.0 )
        tpcat_gameScreen.soundCount:play()
      end
    end
  
    if ( tpcat_gameScreen.data.phase ~= "gameover" ) then
      tpcat_gameScreen.data.secondCounter += 1
      tpcat_gameScreen.data.timer = playdate.timer.new( 1000, tpcat_gameScreen.SecondTimer )
    end
  end,

  ---------------------------------------------------------------------- HANDLE INPUT
  HandleInput = function( self )
    self.safetyButtonDown = inputManager:IsKeyPressed( "down" )
  
    if ( self.safetyButtonDown == false ) then
      return
    end
    
    inputManager:Handle_Crank()
    
    if ( self.data.phase == "play" ) then
      -- Changing it so you can crank in any direction:
      -- young children were getting confused
      local crankAngleDegrees = math.abs( inputManager.crank.angleDegrees )
      local crankChangeDegrees = math.abs( inputManager.crank.changeDegrees )
      self.data.rotateCounter = math.floor( crankAngleDegrees % 2 + 1 )
      
      self.data.audioCounter += crankChangeDegrees
      if ( self.data.audioCounter >= 50 ) then
        self.soundRoll:setVolume( dataManager.data.sound_volume / 100.0 )
        self.soundRoll:play()
        self.data.audioCounter = 0
      end
      
      self.data.totalDelta += crankChangeDegrees / 10
      
    elseif ( self.data.phase == "gameover" ) then
      if ( inputManager:IsKeyPressRelease( "a" ) ) then
        self.nextState = "titleScreen"
      end
    end

    if ( self.data.totalDelta >= self.data.finalValue and self.data.phase ~= "gameover" ) then
      self:Trigger_GameOver()
    
    elseif ( self.data.totalDelta >= self.data.finalValue * 0.5 ) then
      self.data.tpOffset = 2
    
    elseif ( self.data.totalDelta < self.data.finalValue * 0.5 ) then
      self.data.tpOffset = 0
      
    end
  end,
  
  ---------------------------------------------------------------------- CONTROL REMINDER
  ControlReminder = function( self )
    tpcat_gameScreen.imgShade:draw( 0, 0 )
    self.imgTextbox:draw( 50, 30 )
    gfx.setFont( fonts.rains )
    gfx.drawTextAligned( "HOLD THE DOWN BUTTON AND CRANK", 200, 90, kTextAlignment.center )
    gfx.drawTextAligned( "TO UNROLL THE TOILET PAPER!", 200, 110, kTextAlignment.center )
    gfx.drawTextAligned( "HOLD THE DOWN BUTTON TO START", 200, 160, kTextAlignment.center )
  end
}

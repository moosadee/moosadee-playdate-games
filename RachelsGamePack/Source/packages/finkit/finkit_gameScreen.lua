import 'CoreLibs/graphics.lua'
import 'manager_gamedata.lua'
import 'manager_input.lua'
local gfx = playdate.graphics
local snd = playdate.sound

finkit_gameScreen = {
  nextState = "",   -- Set this when you're ready to go to another state
  gameData = nil,

  ---------------------------------------------------------------------- INIT
  Init = function( self )
    local this = potions_gameScreen
    self.nextState = ""
    self.gameData = dataManager["data"][global.gameKey]

    self.images = {
      wave        = gfx.image.new( "packages/finkit/images/wave.png" ),
      fish        = gfx.image.new( "packages/finkit/images/fish.png" ),
      bell        = gfx.image.new( "packages/finkit/images/bell.png" ),
      balloon     = gfx.image.new( "packages/finkit/images/balloon.png" ),
      mine        = gfx.image.new( "packages/finkit/images/mine.png" ),
      hawk        = gfx.image.new( "packages/finkit/images/hawk.png" ),
      ending      = gfx.image.new( "packages/finkit/images/flag.png" ),
      heart       = gfx.image.new( "images/hudheart.png" ),
      textbox     = gfx.image.new( "images/textbox.png" ),
      shadow      = gfx.image.new( "images/transparent.png" ),
      win         = gfx.image.new( "packages/finkit/images/win.png" ),
    }

    self.audio = {
      finJump = snd.sampleplayer.new( "audio/jump_fin.wav" ),
      kitJump = snd.sampleplayer.new( "audio/jump_kit.wav" ),
      collect = snd.sampleplayer.new( "audio/sfx2.wav" ),
      ouch = snd.sampleplayer.new( "audio/sfx4.wav" ),
      failure = snd.sampleplayer.new( "audio/oof.wav" ),
      levelComplete = snd.sampleplayer.new( "audio/yay.wav" ),
    }

    self.tables = {}
    self.tables.fin = TryLoadImageTable( "packages/finkit/images/fin" )
    self.tables.kit = TryLoadImageTable( "packages/finkit/images/kit" )

    self.phase = "level_intro"

    self.game = {
      scrollSpeed = -10,
      scrollX = 0,
      waveCount = 0,
      waveCountMax = 60,
      score = 0,
      lives = 3,
      trinkets = {},
      obstacles = {},
      trinketsCollected = 0,
      totalTrinkets = 0,
      fin = {
        name = "Fin",
        x = 0,
        y = 240-75,
        width = 132,
        height = 56,
        collisionRect = {
          x = 45,
          y = 5,
          width = 47,
          height = 49,
        },
        velocity = 0,
        initialVelocity = -13,
        gravity = 0.75,
        launchCounter = 0,
        jumpCount = 0,
        jumpCounter = 0,
        canDoubleJump = false,
        maxY = 240-75,
        frame = 1,
      },
      kit = {
        name = "Kit",
        x = 50,
        y = 75,
        width = 52,
        height = 46,
        collisionRect = {
          x = 6,
          y = 20,
          width = 47,
          height = 28,
        },
        velocity = 0,
        initialVelocity = -20,
        gravity = 1,
        launchCounter = 0,
        jumpCount = 0,
        jumpCounter = 0,
        canDoubleJump = true,
        maxY = 240-75,
        frame = 1
      }
    }

    self.deleteList = {}

    self.allLevelData = self:SetupLevelData()
    self.levelData = self.allLevelData[global.levelKey]

    self:SetupLevel()

    self.game.instructions = {}

    for key, instruction in pairs( self.levelData.instructions ) do
      table.insert( self.game.instructions, instruction )
    end

    table.insert( self.game.instructions, "" )
    table.insert( self.game.instructions, "Press A to start" )

    self.tables.stars, result = TryLoadImageTable( "images/stars" )

    PlaySong( dataManager["data"][global.gameKey].song )
  end, -- Init = function( self )

  ---------------------------------------------------------------------- CLEANUP
  Cleanup = function( self )
    -- Free space here as needed, stop music
    self.images = nil
    self.audio = nil
    self.tables = nil
    self.game = nil
    self.deleteList = nil
  end, -- Cleanup = function( self )

  ---------------------------------------------------------------------- SETUP LEVEL
  SetupLevel = function( self )
    self.levelData.spawns = {}
    self.game.totalTrinkets = 0

    for key, item in pairs( self.levelData.placements ) do
      self.levelData.spawns[ item.x ] = key
    end

    --self.game.totalTrinkets = #self.game.trinkets

    --for key, item in pairs( self.levelData.placements ) do
    --  if ( item.subtype == "trinket" ) then
    --    self:CreateTrinket( item )
    --  elseif ( item.subtype == "obstacle" ) then
    --    self:CreateObstacle( item )
    --  end
    --end
    --
    --self.game.totalTrinkets = #self.game.trinkets
  end, -- SetupLevel = function( self )

  ---------------------------------------------------------------------- CREATE TRINKET
  CreateTrinket = function( self, item )
    local newTrinket = {}
    newTrinket.active = true
    newTrinket.enterX = item.x
    newTrinket.x = 400--item.x
    newTrinket.y = item.y
    newTrinket.type = item.type
    newTrinket.subtype = item.subtype
    newTrinket.counter = 0
    newTrinket.counterMax = 20
    newTrinket.hSpeed = item.hSpeed
    newTrinket.vSpeed = item.vSpeed
    newTrinket.collisionRect = {}
    newTrinket.collisionRect.x = 0
    newTrinket.collisionRect.y = 0
    newTrinket.collisionRect.width = 0
    newTrinket.collisionRect.height = 0

    if ( item.type == "fish" ) then
      newTrinket.width = 36
      newTrinket.height = 22
      newTrinket.points = 10

      newTrinket.collisionRect.x = 0
      newTrinket.collisionRect.y = 0
      newTrinket.collisionRect.width = 36
      newTrinket.collisionRect.height = 22

    elseif ( item.type == "balloon" ) then
      newTrinket.width = 26
      newTrinket.height = 36
      newTrinket.points = 15

      newTrinket.collisionRect.x = 0
      newTrinket.collisionRect.y = 0
      newTrinket.collisionRect.width = 26
      newTrinket.collisionRect.height = 36

    elseif ( item.type == "bell" ) then
      newTrinket.width = 36
      newTrinket.height = 34
      newTrinket.points = 5

      newTrinket.collisionRect.x = 0
      newTrinket.collisionRect.y = 0
      newTrinket.collisionRect.width = 36
      newTrinket.collisionRect.height = 34

    elseif ( item.type == "ending" ) then
      newTrinket.collisionRect.width = 40
      newTrinket.collisionRect.height = 240
    end

    if ( newTrinket.collisionRect.width == 0 ) then
      newTrinket.collisionRect.width = newTrinket.width
      newTrinket.collisionRect.height = newTrinket.height
    end

    table.insert( self.game.trinkets, newTrinket )
    self.game.totalTrinkets += 1
  end,

  ---------------------------------------------------------------------- CREATE OBSTACLE
  CreateObstacle = function( self, item )
    local newObstacle = {}
    newObstacle.active = true
    newObstacle.enterX = item.x
    newObstacle.x = 400--item.x
    newObstacle.y = item.y
    newObstacle.type = item.type
    newObstacle.subtype = item.subtype
    newObstacle.counter = 0
    newObstacle.counterMax = 20
    newObstacle.hSpeed = item.hSpeed
    newObstacle.vSpeed = item.vSpeed
    newObstacle.collisionRect = {}
    newObstacle.collisionRect.x = 0
    newObstacle.collisionRect.y = 0
    newObstacle.collisionRect.width = 0
    newObstacle.collisionRect.height = 0

    if ( item.type == "hawk" ) then
      newObstacle.width = 44
      newObstacle.height = 40
      newObstacle.points = -5

      newObstacle.collisionRect.x = 5
      newObstacle.collisionRect.y = 11
      newObstacle.collisionRect.width = 36
      newObstacle.collisionRect.height = 23

    elseif ( item.type == "mine" ) then
      newObstacle.width = 64
      newObstacle.height = 76
      newObstacle.points = -10

      newObstacle.collisionRect.x = 7
      newObstacle.collisionRect.y = 19
      newObstacle.collisionRect.width = 51
      newObstacle.collisionRect.height = 44
    end

    if ( newObstacle.collisionRect.width == 0 ) then
      newObstacle.collisionRect.width = newObstacle.width
      newObstacle.collisionRect.height = newObstacle.height
    end

    table.insert( self.game.obstacles, newObstacle )
  end,

  ---------------------------------------------------------------------- UPDATE
  Update = function( self )
    self:HandleInput()

    if ( self.phase == "play" ) then
      self:Update_Play()
    end
  end, -- Update = function( self )

  ---------------------------------------------------------------------- UPDATE - Spawn items
  Update_Items = function( self )
    -- self.levelData.spawns[ item.x ] = key
    if ( DoesTableContainKey( self.levelData.spawns, self.game.scrollX ) == true ) then
      local itemKey = self.levelData.spawns[ self.game.scrollX ]
      local item = self.levelData.placements[ itemKey ]
      if ( item.subtype == "trinket" ) then
        self:CreateTrinket( item )
      elseif ( item.subtype == "obstacle" ) then
        self:CreateObstacle( item )
      end
    end
  end,

  ---------------------------------------------------------------------- UPDATE - Play
  Update_Play = function( self )
    self:Update_ScrollLevel()
    self:Update_Items()
    self:Update_Character( self.game.fin )
    self:Update_Character( self.game.kit )

    -- Update trinkets
    for key, item in pairs( self.game.trinkets ) do
      self:Update_Object( item )
    end

    -- Update obstacles
    for key, item in pairs( self.game.obstacles ) do
      self:Update_Object( item )
    end

    -- Is there a collision?
    if ( GetBoundingBoxCollisionFull( self.game.fin, self.game.kit ) ) then
      -- Collision between fin and kit
      self:Update_CharacterHitGround( self.game.kit )

      if ( self.game.kit.y + self.game.kit.height > self.game.fin.y ) then
        self:Update_CharacterPushUp( self.game.kit, self.game.fin )
      end
    else
      -- No collision
      self:Update_CharacterFall( self.game.kit )
    end

    self:Update_HandleCollision()
    self:Update_DeleteStaleObjects()

    -- Player out of lives?
    if ( self.game.lives < 0 ) then
      self:Update_LoseLevel()
    end
  end, -- Update_Play = function( self )

  ---------------------------------------------------------------------- UPDATE - Scroll level
  Update_ScrollLevel = function( self )
    self.game.waveCount += 1
    if ( self.game.waveCount >= self.game.waveCountMax ) then
      self.game.waveCount = 0
    end

    self.game.scrollX += 1
  end, -- Update_ScrollLevel = function( self )

  ---------------------------------------------------------------------- UPDATE - Delete stale objects
  Update_DeleteStaleObjects = function( self )
  end,

  ---------------------------------------------------------------------- UPDATE - Win level
  Update_WinLevel = function( self )
    self.phase = "level_end_win"

    self.game.stars = 3

    -- Remove one star if you didn't collect all the trinkets
    if ( self.game.trinketsCollected ~= self.game.totalTrinkets ) then
      self.game.stars -= 1
    end

    -- Remove one star if you lost a life
    if ( self.game.lives < 3 ) then
      self.game.stars -= 1
    end


    -- 1 = locked, 2 = no stars, 3 = one star, 4 = two stars, 5 = three stars
    local score = self.game.stars + 2

    ---- Save score
    if ( dataManager["data"][global.gameKey]["levelStatuses"][global.levelKey].state < score ) then
      dataManager["data"][global.gameKey]["levelStatuses"][global.levelKey].state = score
    end

    -- Unlock next level
    if ( dataManager["data"][global.gameKey]["levelStatuses"][global.levelKey+1] ~= nil and dataManager["data"][global.gameKey]["levelStatuses"][global.levelKey+1].state == 1 ) then
      dataManager["data"][global.gameKey]["levelStatuses"][global.levelKey+1].state = 2
    end

    dataManager:SaveData()

    self.audio.levelComplete:setVolume( dataManager.data.sound_volume / 100.0 )
    self.audio.levelComplete:play()
  end,

  ---------------------------------------------------------------------- UPDATE - Lose level
  Update_LoseLevel = function( self )
    self.phase = "level_end_lose"
  end,

  ---------------------------------------------------------------------- UPDATE - Handle collision
  Update_HandleCollision = function( self )
    for key, item in pairs( self.game.trinkets ) do
      if ( item.active and ( GetBoundingBoxCollision( self.game.kit, item ) or GetBoundingBoxCollision( self.game.fin, item ) ) ) then

        self.game.trinketsCollected += 1

        if ( item.type == "ending" ) then
          -- Finished this level! :)
          self:Update_WinLevel()
        else
          -- Collect the item! :)
          -- Mark down for deleting this item
          table.insert( self.deleteList, key )
          item.active = false
          -- Play sound effect
          self.audio.collect:play()
          -- Add to score
          self.game.score += item.points
        end

      end
    end

    for key, item in pairs( self.game.obstacles ) do
      if ( item.active and ( GetBoundingBoxCollision( self.game.kit, item ) or GetBoundingBoxCollision( self.game.fin, item ) ) ) then
        -- Get hurt :|
        -- Mark down for deleting this item

        table.insert( self.deleteList, key )
        item.active = false
        -- Play sound effect
        self.audio.ouch:play()
        -- Subtract score
        self.game.score += item.points
        -- Remove a life
        self.game.lives -= 1
      end
    end
  end, -- Update_HandleCollision = function( self )

  ---------------------------------------------------------------------- UPDATE - Character
  Update_Character = function( self, char )
    char.y += char.velocity
    if ( char.y >= char.maxY ) then
      char.y = char.maxY
      self:Update_CharacterHitGround( char )
    elseif ( char.y < -100 ) then
      char.y = -100
    else
      self:Update_CharacterFall( char )
    end

    if ( char.jumpCount > 0 ) then
      char.frame = 2
    elseif ( char.launchCounter <= 0 ) then
      char.frame = 1
    elseif ( char.launchCounter > 0 ) then
      char.launchCounter -= 0.1
    end
  end, -- Update_Character = function( self, char )

  ---------------------------------------------------------------------- UPDATE - Character begin jump
  Update_CharacterBeginJump = function( self, char )
    if ( char.jumpCount == 0 ) then
      char.velocity = char.initialVelocity
      char.jumpCount = 1

      if ( char.name == "Fin" ) then
        self.audio.finJump:play()
      elseif ( char.name == "Kit" ) then
        self.audio.kitJump:play()
      end

    elseif ( char.canDoubleJump and char.jumpCount == 1 ) then
      char.velocity = char.initialVelocity
      char.jumpCount = 2
      self.audio.kitJump:play()
    end
  end, -- Update_CharacterBeginJump = function( self, char )

  ---------------------------------------------------------------------- UPDATE - Character launch
  Update_CharacterLaunch = function( self, char )
    char.launchCounter = 10
    char.frame = 2
  end, -- Update_CharacterLaunch = function( self, char )

  ---------------------------------------------------------------------- UPDATE - Character fall
  Update_CharacterFall = function( self, char )
    char.velocity += char.gravity
  end, -- Update_CharacterFall = function( self, char )

  ---------------------------------------------------------------------- UPDATE - Character hit ground
  Update_CharacterHitGround = function( self, char )
    char.velocity = 0
    char.jumpCount = 0
    char.frame = 1
  end, -- Update_CharacterHitGround = function( self, char )

  ---------------------------------------------------------------------- UPDATE - Character push up
  -- Fin will push Kit upward so she's not clipping through.
  Update_CharacterPushUp = function( self, char, pusher )
    char.y = pusher.y - char.height
  end, -- Update_CharacterPushUp = function( self, char, pusher )

  ---------------------------------------------------------------------- UPDATE - Fin jump
  Update_FinJump = function( self )
    if ( self.game.kit.jumpCount > 0 ) then
      -- Kit is in the air; Fin can jump
      self:Update_CharacterBeginJump( self.game.fin )
    else
      -- Kit isn't in the air; launch kit first.
      self:Update_CharacterLaunch( self.game.fin )
      self:Update_KitJump()
    end
  end, -- Update_FinJump = function( self )

  ---------------------------------------------------------------------- UPDATE - Kit jump
  Update_KitJump = function( self )
    self:Update_CharacterBeginJump( self.game.kit )
  end, -- Update_KitJump = function( self )

  ---------------------------------------------------------------------- UPDATE - Object
  Update_Object = function( self, item )
    item.counter += 1
    if ( item.counter >= item.counterMax ) then
      item.counter = 1
    end

    if ( item.enterX > self.game.scrollX ) then
      -- It is off-screen
    else
      -- It is now on-screen

      -- FISH BEHAVIOR
      if ( item.type == "fish" ) then
        -- Scroll
        item.x -= item.hSpeed

        -- Up and down
        if ( item.counter <= 10 ) then
          item.y -= item.vSpeed
        else
          item.y += item.vSpeed
        end

      -- BALLOON BEHAVIOR
      elseif ( item.type == "balloon" ) then
        item.x -= item.hSpeed
        item.y -= item.vSpeed

      -- BELL BEHAVIOR
      elseif ( item.type == "bell" ) then
        -- Only horizontal movement
        item.x -= item.hSpeed

      -- MINE BEHAVIOR
      elseif ( item.type == "mine" ) then
        item.x -= item.hSpeed

        -- Up and down
        if ( item.counter <= 10 ) then
          item.y -= item.vSpeed
        else
          item.y += item.vSpeed
        end

      -- HAWK BEHAVIOR
      elseif ( item.type == "hawk" ) then
        item.x -= item.hSpeed

        -- Up and down
        if ( item.counter <= 10 ) then
          item.y -= item.vSpeed
        else
          item.y += item.vSpeed
        end

      else
        item.x -= item.hSpeed

      end -- if ( item.type == "fish" ) then
    end -- if ( item.enterX > self.game.scrollX ) then
  end,

  ---------------------------------------------------------------------- DRAW
  Draw = function( self )
    if ( self.phase == "play" or self.phase == "level_intro" ) then
      self:Draw_Gameplay()
    elseif ( self.phase == "level_end_lose" or self.phase == "level_end_win" ) then
      self:Draw_LevelEnd()
    elseif ( self.phase == "game_end" ) then
      self:Draw_GameEnd()
    end
  end, -- Draw = function( self )

  ---------------------------------------------------------------------- DRAW - GAMEPLAY
  Draw_Gameplay = function( self )
    -- Temporary background
    gfx.setColor( gfx.kColorWhite )
    gfx.fillRect( 0, 0, 400, 240 )

    playdate.drawFPS( 10, 10 )

    -- Draw trinkets
    for key, item in pairs( self.game.trinkets ) do
      if ( item.active ) then

        if ( item.type == "ending" ) then
          gfx.setColor( gfx.kColorXOR )
          gfx.fillRect( item.x, 0, 50, 240 ) -- shaded region
          gfx.drawRect( item.x, 0, 50, 240 ) -- outline
          self.images[ item.type ]:draw( item.x, 240/2 - 20 )

        else
          self.images[ item.type ]:draw( item.x, item.y )
        end

        -- DEBUG: Position
        --gfx.drawTextAligned( tostring( item.enterX ), item.x, item.y - 20 )
      end
    end

    -- Draw obstacles
    for key, item in pairs( self.game.obstacles ) do
      if ( item.active ) then
        self.images[ item.type ]:draw( item.x, item.y )
      end
    end

    self.tables.fin:getImage( self.game.fin.frame ):draw( self.game.fin.x, self.game.fin.y )
    self.tables.kit:getImage( self.game.kit.frame ):draw( self.game.kit.x, self.game.kit.y )

    -- Draw waves
    for i = -60, 400, 60 do
      self.images.wave:draw( (60-self.game.waveCount)+i, 240-42 )
    end

    -- Draw HUD
    gfx.drawTextAligned( "Score: " .. tostring( self.game.score ), 400/2, 10, kTextAlignment.center )
    --gfx.drawTextAligned( "SCROLL " .. tostring( self.game.scrollX ), 10, 10, kTextAlignment.left )

    if ( self.game.lives >= 1 ) then
      self.images.heart:draw( 400-30-5, 5 )
    end

    if ( self.game.lives >= 2 ) then
      self.images.heart:draw( 400-60-5, 5 )
    end

    if ( self.game.lives >= 3 ) then
      self.images.heart:draw( 400-90-5, 5 )
    end

    if ( self.phase == "level_intro" ) then
      self.images.textbox:draw( 50, 30 )
      gfx.setFont( fonts.pedallica )
      gfx.drawTextAligned( "LEVEL " .. tostring( global.levelKey ), 400/2, 50, kTextAlignment.center )

      local y = 90
      local inc = 20

      for key, inst in pairs( self.game.instructions ) do
        gfx.drawTextAligned( inst, 400/2, y, kTextAlignment.center )
        y += inc
      end
    end
  end, -- Draw_Gameplay = function( self )

  ---------------------------------------------------------------------- DRAW - LEVEL END
  Draw_LevelEnd = function( self )
    self.images.shadow:draw( 0, 0 )
    self.images.textbox:draw( 50, 30 )

    if ( self.phase == "level_end_win" ) then
      -- Display level summary
      gfx.setFont( fonts.pedallica )
      -- Display level summary
      gfx.setFont( fonts.pedallica )
      gfx.drawTextAligned( "SUCCESS!", 400/2, 50, kTextAlignment.center )
      gfx.drawTextAligned( "Press 'A' to continue!" , 400/2, 175, kTextAlignment.center )

      -- Draw stars
      local starWidth = 32
      self.tables.stars:getImage( 1 ):draw( 400/2 - (starWidth*2) - (starWidth/2), 100 )

      if ( self.game.stars >= 2 ) then
        self.tables.stars:getImage( 1 ):draw( 400/2 - (starWidth/2), 100 )
      else
        self.tables.stars:getImage( 2 ):draw( 400/2 - (starWidth/2), 100 )
      end

      if ( self.game.stars >= 3 ) then
        self.tables.stars:getImage( 1 ):draw( 400/2 + (starWidth*2) - (starWidth/2), 100 )
      else
        self.tables.stars:getImage( 2 ):draw( 400/2 + (starWidth*2) - (starWidth/2), 100 )
      end

    elseif ( self.phase == "level_end_lose" ) then
      gfx.setFont( fonts.pedallica )
      gfx.drawTextAligned( "FAILURE!", 400/2, 50, kTextAlignment.center )
      gfx.drawTextAligned( "What a buzz-krill!", 400/2, 200, kTextAlignment.center )
      gfx.drawTextAligned( "Press 'A' to continue!" , 400/2, 175, kTextAlignment.center )

    end

  end, -- Draw_LevelEnd = function( self )

  ---------------------------------------------------------------------- DRAW - GAME END
  Draw_GameEnd = function( self )
    -- Draw win screen
    self.images.win:draw( 0, 0 )
  end, -- Draw_GameEnd = function( self )

  ---------------------------------------------------------------------- HANDLE INPUT
  HandleInput = function( self )
    inputManager:Handle_Crank()

    if ( self.phase == "play" ) then
      self:HandleInput_Gameplay()
    elseif ( self.phase == "level_intro" ) then
      self:HandleInput_LevelIntro()
    elseif ( self.phase == "level_end_lose" or self.phase == "level_end_win" ) then
      self:HandleInput_LevelEnd()
    elseif ( self.phase == "game_end" ) then
      self:HandleInput_GameEnd()
    end

  end, -- HandleInput = function( self )

  ---------------------------------------------------------------------- HANDLE INPUT - GAMEPLAY
  HandleInput_Gameplay = function( self )
    if ( inputManager:IsKeyPressed( "b" ) ) then -- FIN jump
      self:Update_FinJump()
    end -- if ( inputManager:IsKeyPressed( "b" ) ) then

    if ( inputManager:IsKeyPressed( "a" ) ) then -- KIT jump
      self:Update_KitJump()
    end -- if ( inputManager:IsKeyPressed( "a" ) ) then
  end, -- HandleInput_Gameplay = function( self )

  ---------------------------------------------------------------------- HANDLE INPUT - LEVEL END
  HandleInput_LevelEnd = function( self )
    if ( inputManager:IsKeyPressRelease( "a" ) ) then
      if ( global.levelKey == 9 ) then
        self.phase = "game_end"
      else
        self.nextState = "levelSelectScreen"
      end
    end -- if ( inputManager:IsKeyPressRelease( "a" ) ) then
  end, -- HandleInput_LevelEnd = function( self )

  ---------------------------------------------------------------------- HANDLE INPUT - LEVEL INTRO
  HandleInput_LevelIntro = function( self )
    if ( inputManager:IsKeyPressed( "a" ) ) then
      self.phase = "play"
    end -- if ( inputManager:IsKeyPressed( "a" ) ) then
  end, -- HandleInput_LevelIntro = function( self )

  ---------------------------------------------------------------------- HANDLE INPUT - GAME END
  HandleInput_GameEnd = function( self )
    if ( inputManager:IsKeyPressRelease( "a" ) ) then
      self.nextState = "levelSelectScreen"
    end
  end, -- HandleInput_GameEnd = function( self )


  ---------------------------------------------------------------------- SETUP LEVEL DATA
  SetupLevelData = function( self )
    levels = {
          {
            instruction_font = "rains",
            instructions = {
              "Fin 'N' Kit both love tasty fish",
              "and cute trinkets!",
              "Together they begin their \"sea-rch\"!",
              "(B) Fin jump, (A) Kit jump"
            },
            placements = {

              { type = "fish",    subtype="trinket",  x = 50,      y = 240-50,   hSpeed = 3,   vSpeed = 1     },
              { type = "fish",    subtype="trinket",  x = 75,      y = 240-50,   hSpeed = 3,   vSpeed = 1     },
              { type = "fish",    subtype="trinket",  x = 100,     y = 240-50,   hSpeed = 3,   vSpeed = 1     },

              { type = "balloon", subtype="trinket",  x = 140,     y = 120,      hSpeed = 3,   vSpeed = 1     },
              { type = "balloon", subtype="trinket",  x = 180,     y = 120,      hSpeed = 3,   vSpeed = 1     },


              { type = "fish",    subtype="trinket",  x = 250,     y = 240-50,   hSpeed = 3,   vSpeed = 1     },
              { type = "balloon", subtype="trinket",  x = 250,     y = 200,      hSpeed = 3,   vSpeed = 1     },

              { type = "fish",    subtype="trinket",  x = 275,     y = 240-50,   hSpeed = 3,   vSpeed = 1     },
              { type = "balloon", subtype="trinket",  x = 275,     y = 150,      hSpeed = 3,   vSpeed = 1     },

              { type = "fish",    subtype="trinket",  x = 300,     y = 240-50,   hSpeed = 3,   vSpeed = 1     },
              { type = "balloon", subtype="trinket",  x = 300,     y = 150,      hSpeed = 3,   vSpeed = 1     },

              -- "V" shape
              { type = "balloon", subtype="trinket",  x = 350,     y = 160,      hSpeed = 3,   vSpeed = 1     },
              { type = "balloon", subtype="trinket",  x = 350,     y = 200,      hSpeed = 3,   vSpeed = 1     },

              { type = "balloon", subtype="trinket",  x = 375,     y = 160,      hSpeed = 3,   vSpeed = 1     },
              { type = "balloon", subtype="trinket",  x = 375,     y = 200,      hSpeed = 3,   vSpeed = 1     },

              { type = "balloon", subtype="trinket",  x = 400,     y = 160,      hSpeed = 3,   vSpeed = 1     },
              { type = "balloon", subtype="trinket",  x = 400,     y = 200,      hSpeed = 3,   vSpeed = 1     },

              { type = "balloon", subtype="trinket",  x = 425,     y = 130,      hSpeed = 3,   vSpeed = 1     },
              { type = "balloon", subtype="trinket",  x = 425,     y = 170,      hSpeed = 3,   vSpeed = 1     },

              { type = "balloon", subtype="trinket",  x = 450,     y = 90,       hSpeed = 3,   vSpeed = 1     },
              { type = "balloon", subtype="trinket",  x = 450,     y = 130,      hSpeed = 3,   vSpeed = 1     },

              -- Ending bells
              { type = "bell",    subtype="trinket",  x = 550,     y = 120,      hSpeed = 3,   vSpeed = 0     },
              { type = "bell",    subtype="trinket",  x = 575,     y = 50,      hSpeed = 3,   vSpeed = 0     },
              { type = "bell",    subtype="trinket",  x = 600,     y = 120,      hSpeed = 3,   vSpeed = 0     },

              { type = "ending",  subtype="trinket",  x = 650,     y = 0,        hSpeed = 3,   vSpeed = 1     },
            },
          },
          {
            instruction_font = "rains",
            instructions = {
              "Not everything in the ocean is friendly!",
              "Don't get hit by obstacles",
              "like hawks and mines!",
              "Running into these will be a \"fish-ue\"."
            },
            placements = {

              { type = "bell",    subtype="trinket",  x = 50,     y = 120,      hSpeed = 5,   vSpeed = 0     },
              { type = "bell",    subtype="trinket",  x = 75,     y = 50,       hSpeed = 5,   vSpeed = 0     },
              { type = "bell",    subtype="trinket",  x = 100,    y = 120,      hSpeed = 5,   vSpeed = 0     },

              { type = "mine",    subtype="obstacle", x = 125,    y = 240-75,   hSpeed = 5,   vSpeed = 1     },
              { type = "bell",    subtype="trinket",  x = 125,    y = 35,       hSpeed = 5,   vSpeed = 0     },
              { type = "bell",    subtype="trinket",  x = 125,    y = 75,       hSpeed = 5,   vSpeed = 0     },
              { type = "bell",    subtype="trinket",  x = 150,    y = 120,      hSpeed = 5,   vSpeed = 0     },
              { type = "bell",    subtype="trinket",  x = 175,    y = 120,      hSpeed = 5,   vSpeed = 0     },
              { type = "mine",    subtype="obstacle", x = 200,    y = 240-75,   hSpeed = 5,   vSpeed = 1     },
              { type = "bell",    subtype="trinket",  x = 200,    y = 35,       hSpeed = 5,   vSpeed = 0     },
              { type = "bell",    subtype="trinket",  x = 200,    y = 75,       hSpeed = 5,   vSpeed = 0     },

              { type = "balloon", subtype="trinket",  x = 235,    y = 120,      hSpeed = 5,   vSpeed = 1     },

              { type = "hawk",    subtype="obstacle", x = 275,    y = 50,       hSpeed = 5,   vSpeed = 1     },
              { type = "hawk",    subtype="obstacle", x = 350,    y = 120,      hSpeed = 5,   vSpeed = 1     },

              { type = "balloon", subtype="trinket",  x = 312,    y = 120,      hSpeed = 5,   vSpeed = 1     },

              { type = "bell",    subtype="trinket",  x = 275,    y = 120,      hSpeed = 5,   vSpeed = 0     },
              { type = "bell",    subtype="trinket",  x = 350,    y = 50,       hSpeed = 5,   vSpeed = 0     },


              { type = "bell",    subtype="trinket",  x = 375,    y = 70,      hSpeed = 5,   vSpeed = 0     },
              { type = "bell",    subtype="trinket",  x = 400,    y = 120,      hSpeed = 5,   vSpeed = 0     },

              { type = "bell",    subtype="trinket",  x = 440,    y = 80,       hSpeed = 5,   vSpeed = 0     },
              { type = "bell",    subtype="trinket",  x = 460,    y = 50,       hSpeed = 5,   vSpeed = 0     },
              { type = "mine",    subtype="obstacle", x = 460,    y = 240-75,   hSpeed = 5,   vSpeed = 1     },
              { type = "bell",    subtype="trinket",  x = 480,    y = 80,       hSpeed = 5,   vSpeed = 0     },

              { type = "bell",    subtype="trinket",  x = 510,    y = 100,      hSpeed = 5,   vSpeed = 0     },

              { type = "bell",    subtype="trinket",  x = 540,    y = 50,      hSpeed = 5,   vSpeed = 0     },
              { type = "bell",    subtype="trinket",  x = 560,    y = 120,     hSpeed = 5,   vSpeed = 0     },
              { type = "hawk",    subtype="obstacle", x = 560,    y = 50,      hSpeed = 5,   vSpeed = 1     },
              { type = "bell",    subtype="trinket",  x = 580,    y = 50,      hSpeed = 5,   vSpeed = 0     },

              { type = "ending",  subtype="trinket",  x = 600,     y = 0,       hSpeed = 3,   vSpeed = 1     },
            },
          },
          {
            instruction_font = "rains",
            instructions = {
              "Fin 'N' Kit's \"*tail*\" continues...",
              "But the distant waters get more dangerous...",
            },
            placements = {

              { type = "bell",    subtype="trinket",  x = 50,     y = 120,      hSpeed = 5,   vSpeed = 0     },
              { type = "bell",    subtype="trinket",  x = 75,     y = 50,       hSpeed = 5,   vSpeed = 0     },
              { type = "bell",    subtype="trinket",  x = 100,    y = 120,      hSpeed = 5,   vSpeed = 0     },

              { type = "bell",    subtype="trinket",  x = 150,    y = 120,      hSpeed = 5,   vSpeed = 0     },
              { type = "bell",    subtype="trinket",  x = 175,    y = 50,       hSpeed = 5,   vSpeed = 0     },
              { type = "mine",    subtype="obstacle", x = 176,    y = 240-75,   hSpeed = 5,   vSpeed = 1     },
              { type = "bell",    subtype="trinket",  x = 200,    y = 120,      hSpeed = 5,   vSpeed = 0     },

              { type = "bell",    subtype="trinket",  x = 250,    y = 50,       hSpeed = 5,   vSpeed = 0     },
              { type = "bell",    subtype="trinket",  x = 275,    y = 120,      hSpeed = 5,   vSpeed = 0     },
              { type = "bell",    subtype="trinket",  x = 300,    y = 50,       hSpeed = 5,   vSpeed = 0     },

              { type = "bell",    subtype="trinket",  x = 325,    y = 50,       hSpeed = 5,   vSpeed = 0     },
              { type = "bell",    subtype="trinket",  x = 350,    y = 120,      hSpeed = 5,   vSpeed = 0     },
              { type = "hawk",    subtype="obstacle", x = 351,    y = 50,       hSpeed = 5,   vSpeed = 1     },
              { type = "bell",    subtype="trinket",  x = 375,    y = 50,       hSpeed = 5,   vSpeed = 0     },

              { type = "hawk",    subtype="obstacle", x = 451,    y = 25,       hSpeed = 6,   vSpeed = 1     },
              { type = "hawk",    subtype="obstacle", x = 452,    y = 50,       hSpeed = 6,   vSpeed = 1     },
              { type = "hawk",    subtype="obstacle", x = 453,    y = 75,       hSpeed = 6,   vSpeed = 1     },
              { type = "bell",    subtype="trinket",  x = 425,    y = 120,      hSpeed = 6,   vSpeed = 0     },
              { type = "bell",    subtype="trinket",  x = 450,    y = 120,      hSpeed = 6,   vSpeed = 0     },
              { type = "bell",    subtype="trinket",  x = 475,    y = 120,      hSpeed = 6,   vSpeed = 0     },

              { type = "hawk",    subtype="obstacle", x = 512,    y = 50,       hSpeed = 5,   vSpeed = 1     },
              { type = "bell",    subtype="trinket",  x = 513,    y = 50,       hSpeed = 6,   vSpeed = 0     },

              { type = "hawk",    subtype="obstacle", x = 550,    y = 25,       hSpeed = 6,   vSpeed = 1     },
              { type = "hawk",    subtype="obstacle", x = 551,    y = 50,       hSpeed = 6,   vSpeed = 1     },
              { type = "hawk",    subtype="obstacle", x = 552,    y = 75,       hSpeed = 6,   vSpeed = 1     },
              { type = "bell",    subtype="trinket",  x = 553,    y = 120,      hSpeed = 6,   vSpeed = 0     },
              { type = "bell",    subtype="trinket",  x = 575,    y = 120,      hSpeed = 6,   vSpeed = 0     },

              { type = "balloon", subtype="trinket",  x = 600,    y = 130,      hSpeed = 6,   vSpeed = 1     },
              { type = "mine",    subtype="obstacle", x = 601,    y = 240-75,   hSpeed = 6,   vSpeed = 1     },

              { type = "hawk",    subtype="obstacle", x = 650,    y = 70,      hSpeed = 6,   vSpeed = 1     },
              { type = "bell",    subtype="trinket",  x = 651,    y = 120,       hSpeed = 6,   vSpeed = 0     },

              { type = "hawk",    subtype="obstacle", x = 700,    y = 120,      hSpeed = 6,   vSpeed = 1     },
              { type = "bell",    subtype="trinket",  x = 701,    y = 40,       hSpeed = 6,   vSpeed = 0     },

              { type = "hawk",    subtype="obstacle", x = 750,    y = 70,      hSpeed = 6,   vSpeed = 1     },
              { type = "bell",    subtype="trinket",  x = 751,    y = 120,       hSpeed = 6,   vSpeed = 0     },

              { type = "hawk",    subtype="obstacle", x = 800,    y = 120,      hSpeed = 6,   vSpeed = 1     },
              { type = "bell",    subtype="trinket",  x = 801,    y = 40,       hSpeed = 6,   vSpeed = 0     },

              { type = "ending",  subtype="trinket",  x = 820,     y = 0,       hSpeed = 6,   vSpeed = 1     },
            },
          },
          {
            instruction_font = "rains",
            instructions = {
              "Sometimes Kit gets a bit sea sick,",
              "so Fin gives her a \"pocean\"."
            },
            placements = {

              { type = "bell",    subtype="trinket",  x = 50,     y = 120,      hSpeed = 5,   vSpeed = 0     },
              { type = "balloon", subtype="trinket",  x = 75,     y = 130,      hSpeed = 5,   vSpeed = 1     },
              { type = "bell",    subtype="trinket",  x = 76,     y = 130,      hSpeed = 5,   vSpeed = 0     },
              { type = "bell",    subtype="trinket",  x = 100,    y = 120,      hSpeed = 5,   vSpeed = 0     },
              { type = "balloon", subtype="trinket",  x = 125,    y = 130,      hSpeed = 5,   vSpeed = 1     },
              { type = "bell",    subtype="trinket",  x = 126,    y = 130,      hSpeed = 5,   vSpeed = 0     },
              { type = "bell",    subtype="trinket",  x = 150,    y = 120,      hSpeed = 5,   vSpeed = 0     },
              { type = "balloon", subtype="trinket",  x = 175,    y = 130,      hSpeed = 5,   vSpeed = 1     },
              { type = "bell",    subtype="trinket",  x = 176,    y = 130,      hSpeed = 5,   vSpeed = 0     },
              { type = "bell",    subtype="trinket",  x = 200,    y = 120,      hSpeed = 5,   vSpeed = 0     },

              { type = "bell",    subtype="trinket",  x = 250,    y = 120,      hSpeed = 6,   vSpeed = 0     },
              { type = "hawk",    subtype="obstacle", x = 251,    y = 30,       hSpeed = 6,   vSpeed = 1     },
              { type = "hawk",    subtype="obstacle", x = 252,    y = 60,       hSpeed = 6,   vSpeed = 1     },
              { type = "hawk",    subtype="obstacle", x = 253,    y = 90,       hSpeed = 6,   vSpeed = 1     },

              { type = "bell",    subtype="trinket",  x = 300,    y = 30,       hSpeed = 6,   vSpeed = 0    },
              { type = "hawk",    subtype="obstacle", x = 301,    y = 120,      hSpeed = 6,   vSpeed = 1    },

              { type = "bell",    subtype="trinket",  x = 350,    y = 120,      hSpeed = 6,   vSpeed = 0     },
              { type = "hawk",    subtype="obstacle", x = 351,    y = 30,       hSpeed = 6,   vSpeed = 1     },
              { type = "hawk",    subtype="obstacle", x = 352,    y = 60,       hSpeed = 6,   vSpeed = 1     },
              { type = "hawk",    subtype="obstacle", x = 353,    y = 90,       hSpeed = 6,   vSpeed = 1     },

              { type = "bell",    subtype="trinket",  x = 410,    y = 30,       hSpeed = 6,   vSpeed = 0    },
              { type = "bell",    subtype="trinket",  x = 411,    y = 50,       hSpeed = 6,   vSpeed = 0    },
              { type = "bell",    subtype="trinket",  x = 412,    y = 70,       hSpeed = 6,   vSpeed = 0    },
              { type = "hawk",    subtype="obstacle", x = 408,    y = 150,      hSpeed = 6,   vSpeed = 1    },
              { type = "hawk",    subtype="obstacle", x = 413,    y = 150,      hSpeed = 6,   vSpeed = 1    },

              { type = "bell",    subtype="trinket",  x = 460,    y = 30,       hSpeed = 6,   vSpeed = 0    },
              { type = "bell",    subtype="trinket",  x = 461,    y = 50,       hSpeed = 6,   vSpeed = 0    },
              { type = "bell",    subtype="trinket",  x = 462,    y = 70,       hSpeed = 6,   vSpeed = 0    },
              { type = "mine",    subtype="obstacle", x = 458,    y = 170,      hSpeed = 6,   vSpeed = 1    },
              { type = "mine",    subtype="obstacle", x = 462,    y = 170,      hSpeed = 6,   vSpeed = 1    },

              { type = "hawk",    subtype="obstacle", x = 500,    y = 35,       hSpeed = 6,   vSpeed = 1    },
              { type = "bell",    subtype="trinket",  x = 525,    y = 110,      hSpeed = 6,   vSpeed = 0    },
              { type = "mine",    subtype="obstacle", x = 550,    y = 170,      hSpeed = 6,   vSpeed = 1    },
              { type = "bell",    subtype="trinket",  x = 575,    y = 110,      hSpeed = 6,   vSpeed = 0    },
              { type = "hawk",    subtype="obstacle", x = 600,    y = 35,       hSpeed = 6,   vSpeed = 1    },

              { type = "ending",  subtype="trinket",  x = 650,     y = 0,       hSpeed = 6,   vSpeed = 1     },
            },
          },
          {
            instruction_font = "rains",
            instructions = {
              "Kit asks Fin if this is the right way",
              "to find all the fish.",
              "Fin says \"I'm shore.\""
            },
            placements = {
              { type = "hawk",    subtype="obstacle", x = 50,      y = 75,      hSpeed = 10,   vSpeed = 1     },
              { type = "bell",    subtype="trinket",  x = 51,      y = 75,      hSpeed = 6,    vSpeed = 0     },

              { type = "hawk",    subtype="obstacle", x = 100,     y = 30,      hSpeed = 10,   vSpeed = 1     },
              { type = "bell",    subtype="trinket",  x = 101,     y = 30,      hSpeed = 6,    vSpeed = 0     },

              { type = "hawk",    subtype="obstacle", x = 150,     y = 120,     hSpeed = 10,   vSpeed = 1     },
              { type = "bell",    subtype="trinket",  x = 151,     y = 120,     hSpeed = 6,    vSpeed = 0     },

              { type = "hawk",    subtype="obstacle", x = 200,     y = 160,     hSpeed = 10,   vSpeed = 1     },
              { type = "bell",    subtype="trinket",  x = 201,     y = 160,     hSpeed = 6,    vSpeed = 0     },

              { type = "bell",    subtype="trinket",  x = 225,     y = 75,      hSpeed = 10,    vSpeed = 0     },

              { type = "hawk",    subtype="obstacle", x = 250,     y = 25,      hSpeed = 10,   vSpeed = 1     },
              { type = "bell",    subtype="trinket",  x = 260,     y = 75,      hSpeed = 8,    vSpeed = 0     },
              { type = "hawk",    subtype="obstacle", x = 270,     y = 160,     hSpeed = 10,   vSpeed = 1     },


              { type = "bell",    subtype="trinket",  x = 285,     y = 75,      hSpeed = 10,    vSpeed = 0     },


              { type = "mine",    subtype="obstacle", x = 300,     y = 160,     hSpeed = 6,    vSpeed = 1     },
              { type = "balloon", subtype="trinket",  x = 305,     y = 140,     hSpeed = 6,    vSpeed = 1     },

              { type = "fish",    subtype="trinket",  x = 390,     y = 160,    hSpeed = 8,   vSpeed = 2     },
              { type = "hawk",    subtype="obstacle", x = 385,     y = 70,     hSpeed = 8,   vSpeed = 1     },
              { type = "hawk",    subtype="obstacle", x = 390,     y = 50,     hSpeed = 8,   vSpeed = 1     },
              { type = "hawk",    subtype="obstacle", x = 395,     y = 30,     hSpeed = 8,   vSpeed = 1     },
              { type = "hawk",    subtype="obstacle", x = 400,     y = 30,     hSpeed = 8,   vSpeed = 1     },
              { type = "hawk",    subtype="obstacle", x = 405,     y = 50,     hSpeed = 8,   vSpeed = 1     },
              { type = "hawk",    subtype="obstacle", x = 410,     y = 70,     hSpeed = 8,   vSpeed = 1     },
              { type = "fish",    subtype="trinket",  x = 405,     y = 160,    hSpeed = 8,   vSpeed = 2     },

              { type = "hawk",    subtype="obstacle", x = 450,     y = 25,      hSpeed = 10,   vSpeed = 1     },
              { type = "bell",    subtype="trinket",  x = 460,     y = 75,      hSpeed = 8,    vSpeed = 0     },
              { type = "hawk",    subtype="obstacle", x = 470,     y = 160,     hSpeed = 10,   vSpeed = 1     },

              { type = "mine",    subtype="obstacle", x = 500,     y = 160,     hSpeed = 6,    vSpeed = 1     },
              { type = "balloon", subtype="trinket",  x = 505,     y = 140,     hSpeed = 6,    vSpeed = 1     },

              { type = "hawk",    subtype="obstacle", x = 550,     y = 160,     hSpeed = 8,   vSpeed = 1     },
              { type = "hawk",    subtype="obstacle", x = 555,     y = 140,     hSpeed = 8,   vSpeed = 1     },
              { type = "hawk",    subtype="obstacle", x = 560,     y = 160,     hSpeed = 8,   vSpeed = 1     },
              { type = "balloon", subtype="trinket",  x = 555,     y = 120,     hSpeed = 8,    vSpeed = 1     },

              { type = "fish",    subtype="trinket",  x = 605,     y = 160,    hSpeed = 10,   vSpeed = 2     },
              { type = "hawk",    subtype="obstacle", x = 600,     y = 70,     hSpeed = 10,   vSpeed = 2     },
              { type = "hawk",    subtype="obstacle", x = 605,     y = 50,     hSpeed = 10,   vSpeed = 2     },
              { type = "hawk",    subtype="obstacle", x = 610,     y = 30,     hSpeed = 10,   vSpeed = 2     },
              { type = "hawk",    subtype="obstacle", x = 615,     y = 30,     hSpeed = 10,   vSpeed = 2     },
              { type = "hawk",    subtype="obstacle", x = 620,     y = 50,     hSpeed = 10,   vSpeed = 2     },
              { type = "hawk",    subtype="obstacle", x = 625,     y = 70,     hSpeed = 10,   vSpeed = 2     },
              { type = "fish",    subtype="trinket",  x = 620,     y = 160,    hSpeed = 10,   vSpeed = 2     },

              { type = "hawk",    subtype="obstacle", x = 650,     y = 140,     hSpeed = 10,   vSpeed = 2     },
              { type = "hawk",    subtype="obstacle", x = 655,     y = 160,     hSpeed = 10,   vSpeed = 2     },
              { type = "hawk",    subtype="obstacle", x = 660,     y = 140,     hSpeed = 10,   vSpeed = 2     },

              { type = "fish",    subtype="trinket",  x = 655,     y = 50,      hSpeed = 10,   vSpeed = 2     },

              { type = "ending",  subtype="trinket",  x = 700,     y = 0,       hSpeed = 6,   vSpeed = 1     },

            },
          },
          {
            instruction_font = "rains",
            instructions = {
              "Fin asks Kit make sure to \"wave\"",
              "at other dolphin-cat pairs passing by."
            },
            placements = {
              { type = "bell",    subtype="trinket",  x = 50,      y = 60,      hSpeed = 5,   vSpeed = 0     },
              { type = "hawk",    subtype="obstacle", x = 90,      y = 50,      hSpeed = 7,   vSpeed = 3     },
              { type = "bell",    subtype="trinket",  x = 100,     y = 60,      hSpeed = 5,   vSpeed = 0     },

              { type = "bell",    subtype="trinket",  x = 150,     y = 120,     hSpeed = 5,   vSpeed = 0    },
              { type = "hawk",    subtype="obstacle", x = 190,     y = 120,     hSpeed = 7,   vSpeed = 3    },
              { type = "bell",    subtype="trinket",  x = 200,     y = 120,     hSpeed = 5,   vSpeed = 0    },

              { type = "fish",    subtype="trinket",  x = 250,     y = 180,     hSpeed = 5,   vSpeed = 1    },
              { type = "fish",    subtype="trinket",  x = 260,     y = 175,     hSpeed = 5,   vSpeed = 1    },
              { type = "bell",    subtype="trinket",  x = 261,     y = 50,      hSpeed = 5,   vSpeed = 0    },
              { type = "fish",    subtype="trinket",  x = 270,     y = 180,     hSpeed = 5,   vSpeed = 1    },

              { type = "balloon", subtype="trinket",  x = 290,     y = 160,     hSpeed = 5,   vSpeed = 1    },
              { type = "balloon", subtype="trinket",  x = 300,     y = 140,     hSpeed = 5,   vSpeed = 1    },
              { type = "balloon", subtype="trinket",  x = 310,     y = 140,     hSpeed = 5,   vSpeed = 1    },

              { type = "fish",    subtype="trinket",  x = 340,     y = 180,     hSpeed = 5,   vSpeed = 1    },
              { type = "mine",    subtype="obstacle", x = 365,     y = 170,     hSpeed = 5,   vSpeed = 1    },
              { type = "bell",    subtype="trinket",  x = 366,     y = 50,      hSpeed = 5,   vSpeed = 0    },
              { type = "fish",    subtype="trinket",  x = 390,     y = 180,     hSpeed = 5,   vSpeed = 1    },

              { type = "hawk",    subtype="obstacle", x = 450,    y = 120,      hSpeed = 6,   vSpeed = 1     },
              { type = "bell",    subtype="trinket",  x = 425,    y = 50,       hSpeed = 6,   vSpeed = 0     },
              { type = "bell",    subtype="trinket",  x = 451,    y = 50,       hSpeed = 6,   vSpeed = 0     },
              { type = "bell",    subtype="trinket",  x = 475,    y = 50,       hSpeed = 6,   vSpeed = 0     },

              { type = "hawk",    subtype="obstacle", x = 550,    y = 120,      hSpeed = 6,   vSpeed = 1     },
              { type = "bell",    subtype="trinket",  x = 525,    y = 50,       hSpeed = 6,   vSpeed = 0     },
              { type = "bell",    subtype="trinket",  x = 551,    y = 50,       hSpeed = 6,   vSpeed = 0     },
              { type = "bell",    subtype="trinket",  x = 575,    y = 50,       hSpeed = 6,   vSpeed = 0     },

              { type = "hawk",    subtype="obstacle", x = 650,    y = 120,      hSpeed = 6,   vSpeed = 1     },
              { type = "bell",    subtype="trinket",  x = 625,    y = 50,       hSpeed = 6,   vSpeed = 0     },
              { type = "bell",    subtype="trinket",  x = 651,    y = 50,       hSpeed = 6,   vSpeed = 0     },
              { type = "bell",    subtype="trinket",  x = 675,    y = 50,       hSpeed = 6,   vSpeed = 0     },

              { type = "ending",  subtype="trinket",  x = 700,     y = 0,       hSpeed = 6,   vSpeed = 1     },
            },
          },
          {
            instruction_font = "rains",
            instructions = {
              "\"This is turning out to be",
              "a fin-tastic day.\" Fin tells Kit."
            },
            placements = {
              { type = "mine",    subtype="obstacle", x = 50,     y = 170,    hSpeed = 8,    vSpeed = 1     },
              { type = "bell",    subtype="trinket",  x = 70,     y = 120,    hSpeed = 8,    vSpeed = 0     },
              { type = "hawk",    subtype="obstacle", x = 80,     y = 50,     hSpeed = 8,    vSpeed = 2     },
              { type = "bell",    subtype="trinket",  x = 95,     y = 120,    hSpeed = 8,    vSpeed = 0     },
              { type = "mine",    subtype="obstacle", x = 110,    y = 160,    hSpeed = 8,    vSpeed = 1     },
              { type = "bell",    subtype="trinket",  x = 125,    y = 120,    hSpeed = 8,    vSpeed = 0     },
              { type = "hawk",    subtype="obstacle", x = 140,    y = 60,     hSpeed = 8,    vSpeed = 2     },
              { type = "bell",    subtype="trinket",  x = 155,    y = 120,    hSpeed = 8,    vSpeed = 0     },
              { type = "bell",    subtype="trinket",  x = 170,    y = 160,    hSpeed = 8,    vSpeed = 0     },


              { type = "bell",    subtype="trinket",  x = 200,    y = 160,    hSpeed = 8,    vSpeed = 0     },
              { type = "bell",    subtype="trinket",  x = 210,    y = 120,    hSpeed = 8,    vSpeed = 0     },
              { type = "bell",    subtype="trinket",  x = 215,    y = 60,     hSpeed = 8,    vSpeed = 0     },
              { type = "bell",    subtype="trinket",  x = 235,    y = 60,     hSpeed = 8,    vSpeed = 0     },
              { type = "bell",    subtype="trinket",  x = 240,    y = 120,    hSpeed = 8,    vSpeed = 0     },
              { type = "bell",    subtype="trinket",  x = 250,    y = 160,    hSpeed = 8,    vSpeed = 0     },
              { type = "hawk",    subtype="obstacle", x = 226,    y = 160,    hSpeed = 8,    vSpeed = 2     },


              { type = "hawk",    subtype="obstacle", x = 265,    y = 60,    hSpeed = 8,    vSpeed = 6     },
              { type = "hawk",    subtype="obstacle", x = 285,    y = 40,    hSpeed = 8,    vSpeed = 6     },
              { type = "bell",    subtype="trinket",  x = 286,    y = 160,   hSpeed = 8,    vSpeed = 0     },
              { type = "hawk",    subtype="obstacle", x = 305,    y = 60,    hSpeed = 8,    vSpeed = 6     },

              { type = "bell",    subtype="trinket",  x = 325,    y = 120,    hSpeed = 8,    vSpeed = 0     },

              { type = "hawk",    subtype="obstacle", x = 350,    y = 160,    hSpeed = 8,    vSpeed = 0     },
              { type = "hawk",    subtype="obstacle", x = 355,    y = 150,    hSpeed = 8,    vSpeed = 0     },
              { type = "bell",    subtype="trinket",  x = 356,    y = 40,     hSpeed = 8,    vSpeed = 0     },
              { type = "hawk",    subtype="obstacle", x = 360,    y = 160,    hSpeed = 8,    vSpeed = 0     },

              { type = "bell",    subtype="trinket",  x = 380,    y = 120,    hSpeed = 8,    vSpeed = 0     },



              { type = "bell",    subtype="trinket",  x = 400,    y = 160,    hSpeed = 9,    vSpeed = 0     },
              { type = "bell",    subtype="trinket",  x = 410,    y = 120,    hSpeed = 9,    vSpeed = 0     },
              { type = "bell",    subtype="trinket",  x = 415,    y = 60,     hSpeed = 9,    vSpeed = 0     },
              { type = "bell",    subtype="trinket",  x = 425,    y = 40,     hSpeed = 9,    vSpeed = 0     },
              { type = "bell",    subtype="trinket",  x = 435,    y = 60,     hSpeed = 9,    vSpeed = 0     },
              { type = "bell",    subtype="trinket",  x = 440,    y = 120,    hSpeed = 9,    vSpeed = 0     },
              { type = "bell",    subtype="trinket",  x = 450,    y = 160,    hSpeed = 9,    vSpeed = 0     },
              { type = "hawk",    subtype="obstacle", x = 426,    y = 160,    hSpeed = 9,    vSpeed = 2     },


              { type = "bell",    subtype="trinket",  x = 470,     y = 120,   hSpeed = 9,    vSpeed = 0     },
              { type = "hawk",    subtype="obstacle", x = 480,     y = 50,    hSpeed = 9,    vSpeed = 2     },
              { type = "bell",    subtype="trinket",  x = 490,     y = 120,   hSpeed = 9,    vSpeed = 0     },
              { type = "mine",    subtype="obstacle", x = 510,    y = 160,    hSpeed = 9,    vSpeed = 1     },
              { type = "bell",    subtype="trinket",  x = 525,    y = 120,    hSpeed = 9,    vSpeed = 0     },
              { type = "hawk",    subtype="obstacle", x = 540,    y = 60,     hSpeed = 9,    vSpeed = 2     },


              { type = "hawk",    subtype="obstacle", x = 565,    y = 60,     hSpeed = 9,    vSpeed = 6     },
              { type = "hawk",    subtype="obstacle", x = 585,    y = 40,     hSpeed = 9,    vSpeed = 6     },
              { type = "bell",    subtype="trinket",  x = 586,    y = 160,    hSpeed = 9,    vSpeed = 0     },
              { type = "hawk",    subtype="obstacle", x = 605,    y = 60,     hSpeed = 9,    vSpeed = 6     },
              { type = "bell",    subtype="trinket",  x = 625,    y = 120,    hSpeed = 9,    vSpeed = 0     },
              { type = "hawk",    subtype="obstacle", x = 650,    y = 160,    hSpeed = 9,    vSpeed = 0     },
              { type = "hawk",    subtype="obstacle", x = 655,    y = 150,    hSpeed = 9,    vSpeed = 0     },
              { type = "bell",    subtype="trinket",  x = 656,    y = 40,     hSpeed = 9,    vSpeed = 0     },
              { type = "hawk",    subtype="obstacle", x = 660,    y = 160,    hSpeed = 9,    vSpeed = 0     },
              { type = "bell",    subtype="trinket",  x = 680,    y = 120,    hSpeed = 9,    vSpeed = 0     },

              { type = "bell",    subtype="trinket",  x = 700,    y = 40,    hSpeed = 6,    vSpeed = 0     },
              { type = "ending",  subtype="trinket",  x = 720,    y = 0,     hSpeed = 5,   vSpeed = 1     },

            },
          },
          {
            instruction_font = "rains",
            instructions = {
              "Oh \"buoy\"...",
              "This is starting to get out of \"sand\"..."
            },
            placements = {
              { type = "balloon",    subtype="trinket",  x = 25,    y = 140,   hSpeed = 5,    vSpeed = 1     },
              { type = "balloon",    subtype="trinket",  x = 30,    y = 120,   hSpeed = 5,    vSpeed = 1     },
              { type = "balloon",    subtype="trinket",  x = 35,    y = 100,   hSpeed = 5,    vSpeed = 1     },
              { type = "balloon",    subtype="trinket",  x = 40,    y = 115,   hSpeed = 5,    vSpeed = 1     },
              { type = "balloon",    subtype="trinket",  x = 45,    y = 135,   hSpeed = 5,    vSpeed = 1     },

              { type = "mine",       subtype="obstacle", x = 100,    y = 160,   hSpeed = 7,    vSpeed = 1     },

              { type = "bell",       subtype="trinket",  x = 120,    y = 70,    hSpeed = 5,    vSpeed = 0     },
              { type = "bell",       subtype="trinket",  x = 121,    y = 140,   hSpeed = 5,    vSpeed = 0     },
              { type = "bell",       subtype="trinket",  x = 140,    y = 140,   hSpeed = 5,    vSpeed = 0     },
              { type = "bell",       subtype="trinket",  x = 141,    y = 70,    hSpeed = 5,    vSpeed = 0     },

              { type = "mine",       subtype="obstacle", x = 200,    y = 160,  hSpeed = 7,    vSpeed = 1     },
              { type = "hawk",       subtype="obstacle", x = 201,    y = 120,  hSpeed = 7,    vSpeed = 1     },
              { type = "bell",       subtype="trinket",  x = 202,    y = 40,   hSpeed = 7,    vSpeed = 0     },

              { type = "bell",       subtype="trinket",  x = 225,    y = 140,   hSpeed = 7,    vSpeed = 0     },

              { type = "mine",       subtype="obstacle", x = 250,    y = 160,  hSpeed = 7,    vSpeed = 1     },
              { type = "hawk",       subtype="obstacle", x = 251,    y = 120,  hSpeed = 7,    vSpeed = 1     },
              { type = "bell",       subtype="trinket",  x = 252,    y = 40,   hSpeed = 7,    vSpeed = 0     },

              { type = "bell",       subtype="trinket",  x = 275,    y = 140,   hSpeed = 7,    vSpeed = 0     },
              { type = "bell",       subtype="trinket",  x = 295,    y = 140,   hSpeed = 7,    vSpeed = 0     },

              { type = "mine",       subtype="obstacle", x = 320,    y = 160,   hSpeed = 7,    vSpeed = 1     },
              { type = "hawk",       subtype="obstacle", x = 321,    y = 120,   hSpeed = 7,    vSpeed = 1     },
              { type = "bell",       subtype="trinket",  x = 340,    y = 60,    hSpeed = 7,    vSpeed = 0     },
              { type = "mine",       subtype="obstacle", x = 360,    y = 160,   hSpeed = 7,    vSpeed = 1     },

              { type = "bell",       subtype="trinket",  x = 375,    y = 80,    hSpeed = 7,    vSpeed = 0     },
              { type = "hawk",       subtype="obstacle", x = 390,    y = 40,    hSpeed = 7,    vSpeed = 1     },
              { type = "bell",       subtype="trinket",  x = 391,    y = 160,   hSpeed = 7,    vSpeed = 0     },
              { type = "bell",       subtype="trinket",  x = 410,    y = 80,    hSpeed = 7,    vSpeed = 0     },
              { type = "hawk",       subtype="obstacle", x = 420,    y = 160,   hSpeed = 7,    vSpeed = 1     },

              { type = "bell",       subtype="trinket",  x = 435,    y = 80,    hSpeed = 7,    vSpeed = 0     },
              { type = "hawk",       subtype="obstacle", x = 450,    y = 40,    hSpeed = 7,    vSpeed = 1     },
              { type = "bell",       subtype="trinket",  x = 451,    y = 160,    hSpeed = 7,    vSpeed = 0     },
              { type = "bell",       subtype="trinket",  x = 470,    y = 80,    hSpeed = 7,    vSpeed = 0     },
              { type = "hawk",       subtype="obstacle", x = 480,    y = 160,   hSpeed = 7,    vSpeed = 1     },

              { type = "balloon",    subtype="trinket",  x = 500,    y = 120,   hSpeed = 5,    vSpeed = 1     },

              { type = "hawk",       subtype="obstacle", x = 550,    y = 120,   hSpeed = 7,    vSpeed = 1     },
              { type = "bell",       subtype="trinket",  x = 575,    y = 120,   hSpeed = 7,    vSpeed = 0     },
              { type = "hawk",       subtype="obstacle", x = 600,    y = 120,   hSpeed = 7,    vSpeed = 1     },
              { type = "bell",       subtype="trinket",  x = 625,    y = 150,   hSpeed = 7,    vSpeed = 0     },
              { type = "balloon",    subtype="trinket",  x = 626,    y = 120,   hSpeed = 7,    vSpeed = 1     },

              { type = "hawk",       subtype="obstacle", x = 650,    y = 60,   hSpeed = 7,    vSpeed = 1     },
              { type = "bell",       subtype="trinket",  x = 675,    y = 120,   hSpeed = 7,    vSpeed = 0     },
              { type = "hawk",       subtype="obstacle", x = 700,    y = 60,   hSpeed = 7,    vSpeed = 1     },


              { type = "ending",     subtype="trinket",  x = 720,    y = 0,     hSpeed = 5,   vSpeed = 1     },
            },
          },
          {
            instruction_font = "rains",
            instructions = {
              "As the day comes to a close",
              "Fin tells Kit that",
              "they \"otter\" head back to shore..."
            },
            placements = {
              { type = "hawk",       subtype="obstacle", x = 80,     y = 50,     hSpeed = 8,    vSpeed = 2     },
              { type = "hawk",       subtype="obstacle", x = 90,     y = 70,     hSpeed = 8,    vSpeed = 2     },
              { type = "hawk",       subtype="obstacle", x = 100,    y = 90,     hSpeed = 8,    vSpeed = 2     },
              { type = "mine",       subtype="obstacle", x = 120,    y = 160,    hSpeed = 8,    vSpeed = 1     },
              { type = "balloon",    subtype="trinket",  x = 121,    y = 120,    hSpeed = 8,    vSpeed = 1     },

              { type = "mine",       subtype="obstacle", x = 160,    y = 160,    hSpeed = 8,    vSpeed = 1     },
              { type = "balloon",    subtype="trinket",  x = 161,    y = 120,    hSpeed = 8,    vSpeed = 1     },


              { type = "hawk",       subtype="obstacle", x = 200,    y = 120,   hSpeed = 8,    vSpeed = 1     },
              { type = "balloon",    subtype="trinket",  x = 201,    y = 50,    hSpeed = 8,    vSpeed = 1     },

              { type = "hawk",       subtype="obstacle", x = 240,    y = 120,   hSpeed = 8,    vSpeed = 1     },
              { type = "balloon",    subtype="trinket",  x = 241,    y = 50,    hSpeed = 8,    vSpeed = 1     },


              { type = "hawk",       subtype="obstacle", x = 280,    y = 50,    hSpeed = 8,    vSpeed = 1     },
              { type = "bell",       subtype="trinket",  x = 281,    y = 120,   hSpeed = 8,    vSpeed = 0     },

              { type = "hawk",       subtype="obstacle", x = 320,    y = 130,   hSpeed = 8,    vSpeed = 1     },
              { type = "hawk",       subtype="obstacle", x = 321,    y = 100,   hSpeed = 8,    vSpeed = 1     },
              { type = "balloon",    subtype="trinket",  x = 322,    y = 50,    hSpeed = 8,    vSpeed = 1     },

              { type = "mine",       subtype="obstacle", x = 350,    y = 160,   hSpeed = 8,    vSpeed = 1     },


              { type = "balloon",    subtype="trinket",  x = 380,    y = 120,   hSpeed = 8,    vSpeed = 1     },
              { type = "balloon",    subtype="trinket",  x = 390,    y = 100,   hSpeed = 8,    vSpeed = 1     },
              { type = "balloon",    subtype="trinket",  x = 400,    y = 80,    hSpeed = 8,    vSpeed = 1     },

              { type = "bell",       subtype="trinket",  x = 381,    y = 160,   hSpeed = 8,    vSpeed = 0     },
              { type = "bell",       subtype="trinket",  x = 391,    y = 160,   hSpeed = 8,    vSpeed = 0     },
              { type = "bell",       subtype="trinket",  x = 401,    y = 160,   hSpeed = 8,    vSpeed = 0     },


              { type = "hawk",       subtype="obstacle", x = 440,    y = 20,    hSpeed = 8,    vSpeed = 1     },
              { type = "hawk",       subtype="obstacle", x = 450,    y = 30,    hSpeed = 8,    vSpeed = 1     },

              { type = "hawk",       subtype="obstacle", x = 460,    y = 40,    hSpeed = 8,    vSpeed = 1     },
              { type = "hawk",       subtype="obstacle", x = 470,    y = 50,    hSpeed = 8,    vSpeed = 1     },

              { type = "hawk",       subtype="obstacle", x = 490,    y = 160,    hSpeed = 8,    vSpeed = 1     },
              { type = "bell",       subtype="trinket",  x = 495,    y = 50,     hSpeed = 8,    vSpeed = 0     },
              { type = "hawk",       subtype="obstacle", x = 500,    y = 150,    hSpeed = 8,    vSpeed = 1     },

              { type = "hawk",       subtype="obstacle", x = 550,    y = 160,    hSpeed = 8,    vSpeed = 1     },
              { type = "bell",       subtype="trinket",  x = 555,    y = 50,     hSpeed = 8,    vSpeed = 0     },
              { type = "hawk",       subtype="obstacle", x = 560,    y = 150,    hSpeed = 8,    vSpeed = 1     },

              { type = "hawk",       subtype="obstacle", x = 600,    y = 120,   hSpeed = 7,    vSpeed = 1     },
              { type = "bell",       subtype="trinket",  x = 625,    y = 150,   hSpeed = 7,    vSpeed = 0     },
              { type = "balloon",    subtype="trinket",  x = 626,    y = 120,   hSpeed = 7,    vSpeed = 1     },

              { type = "hawk",       subtype="obstacle", x = 650,    y = 60,   hSpeed = 7,    vSpeed = 1     },
              { type = "bell",       subtype="trinket",  x = 675,    y = 120,   hSpeed = 7,    vSpeed = 0     },
              { type = "hawk",       subtype="obstacle", x = 700,    y = 60,   hSpeed = 7,    vSpeed = 1     },


              { type = "ending",     subtype="trinket",  x = 720,    y = 0,     hSpeed = 5,   vSpeed = 1     },
            },
          },
        }
    return levels
  end,
} -- finkit_gameScreen

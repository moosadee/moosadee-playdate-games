import 'CoreLibs/graphics.lua'
import 'manager_gamedata.lua'
import 'manager_input.lua'
import 'utilities.lua'
import 'global.lua'
local gfx = playdate.graphics
local snd = playdate.sound

lasers_gameScreen = {
  ---------------------------------------------------------------------- INIT
  Init = function( self )
    self.nextState = ""   -- Set this when you're ready to go to another state

    self.gameData = nil

    self.images = {
      shadow  = gfx.image.new( "images/transparent.png" ),
      textbox = gfx.image.new( "images/textbox.png" ),
      tank = gfx.image.new( "packages/lasers/images/tank-static.png" ),
      tankPatrol = gfx.image.new( "packages/lasers/images/tank-patrol.png" ),
      tankBomber = gfx.image.new( "packages/lasers/images/tank-bomber.png" ),
      win = gfx.image.new( "packages/lasers/images/win.png" ),
    }

    self.audio = {
      laserShoot = snd.sampleplayer.new( "audio/shoot.wav" ),
      bulletShoot = snd.sampleplayer.new( "audio/punch.wav" ),
      explosion = snd.sampleplayer.new( "audio/sfx4.wav" ),
      success = snd.sampleplayer.new( "audio/sfx3.wav" ),
      failure = snd.sampleplayer.new( "audio/oof.wav" ),
      levelComplete = snd.sampleplayer.new( "audio/yay.wav" ),
    }

    self.tables = {
      alien = nil,
      alien2 = nil,
      explosion = nil,
      brickwall = nil,
      laser = nil,
      bullet = nil,
      stars = nil,
    }

    self.data = {
      level = 1,
      phase = "levelmark",
      instructions = { },
      instructionFont = "",
      levelEndCountdown = 0,
      tanksRemaining = 0,
      aliensRemaining = 0,
      startingAliens = 0,
    }

    self.player = {
      shootCooldown = 0,
      shootCooldownMax = 50
    }

    self.effects = {
    }

    self.deleteList = {}

    self.aliens = {}
    self.tanks = {}
    self.structures = {}
    self.bullets = {}

    self.buttonpressCooldown = 0
    self.counter = 0

    self.gameData = dataManager["data"][global.gameKey]
    --self.levelData = dataManager["data"][global.gameKey]["levels"][global.levelKey]
    self.allLevelData = self:SetupLevelData()
    self.levelData = self.allLevelData[global.levelKey]

    self.aliens = {}
    self.tanks = {}
    self.structures = {}

    self.tables.stars, result = TryLoadImageTable( "images/stars" )
    self.tables.alien, result = TryLoadImageTable( "packages/lasers/images/alien" )
    self.tables.alien2, result = TryLoadImageTable( "packages/lasers/images/alien2" )
    self.tables.explosion, result = TryLoadImageTable( "packages/lasers/images/explosion" )
    self.tables.laser, result = TryLoadImageTable( "packages/lasers/images/laser" )
    self.tables.bullet, result = TryLoadImageTable( "packages/lasers/images/bullet" )
    self.tables.brickwall, result = TryLoadImageTable( "packages/lasers/images/brickwall" )

    self.counter = 0
    self.data.level = global.levelKey
    self:SetupLevel()

    PlaySong( dataManager["data"][global.gameKey].song )
  end, -- Init = function( self )

  ---------------------------------------------------------------------- CLEANUP
  Cleanup = function( self )
    -- Free space here as needed, stop music
    self.tables = nil
    self.counter = nil
    self.data = nil
    self.images = nil
    self.audio = nil
    self.tables = nil
    self.gameData = nil
    self.levelData = nil
    self.nextState = nil
    self.player = nil
    self.effects = nil
    self.deleteList = nil
    self.aliens = nil
    self.tanks = nil
    self.structures = nil
    self.bullets = nil
    self.buttonpressCooldown = nil
    self.counter = nil
  end, -- Cleanup = function( self )

  ---------------------------------------------------------------------- OUT OF ALIENS
  OutOfAliens = function( self )
    self.data.phase = "gameover_lose"
    self.buttonpressCooldown = 100
    self.audio.failure:setVolume( dataManager.data.sound_volume / 100.0 )
    self.audio.failure:play()
  end,

  ---------------------------------------------------------------------- DEACTIVATE OBJECT
  DeactivateObject = function( self, key, object )
    object.active = false

    if ( object.type == "alien" ) then
      self.data.aliensRemaining -= 1

    elseif ( object.type == "tank" ) then
      self.data.tanksRemaining -= 1

    end

    if ( object.type == "alien" ) then
      table.remove( self.aliens, key )

    else
      table.insert( self.deleteList, { key = key, type = object.type } )
    end
  end,

  ---------------------------------------------------------------------- ADD EFFECT
  AddEffect = function( self, x, y, style )
    local newEffect = {}
    newEffect.style = style
    newEffect.x = x
    newEffect.y = y
    newEffect.active = true
    newEffect.type = "effect"
    newEffect.countdownMax = 10
    newEffect.countdown = newEffect.countdownMax

    if ( newEffect.style == "explosion" ) then
      newEffect.countdownMax = 10
      newEffect.countdown = newEffect.countdownMax
      newEffect.frame = 1
      self.audio.explosion:setVolume( dataManager.data.sound_volume / 100.0 )
      self.audio.explosion:play()
    end

    table.insert( self.effects, newEffect )
  end,

  ---------------------------------------------------------------------- ADD ALIEN
  AddAlien = function( self, x, y, subtype )
    if ( subtype == nil ) then
      subtype = "alien"
    end

    local newAlien = {}
    newAlien.x = x
    newAlien.y = y
    newAlien.frame = 1
    newAlien.frameMax = 2
    if ( subtype == "alien" ) then
      newAlien.width = 35
      newAlien.height = 35
    elseif ( subtype == "alien2" ) then
      newAlien.width = 45
      newAlien.height = 45
    end
    newAlien.animateSpeed = 0.1
    newAlien.active = true
    newAlien.type = "alien"
    newAlien.subtype = subtype
    table.insert( self.aliens, newAlien )
  end,

  ---------------------------------------------------------------------- ADD TANK
  AddTank = function( self, x, y, behavior, minX, maxX )
    local newTank = {}
    newTank.width = 35
    newTank.height = 35
    newTank.x = x - ( newTank.width / 2 )
    newTank.y = y
    newTank.hp = 1
    newTank.shootCooldown = 0
    newTank.behavior = behavior
    newTank.goal = 0
    newTank.minX = minX
    newTank.maxX = maxX
    newTank.active = true
    newTank.type = "tank"

    if ( newTank.behavior == "bomber" ) then
      newTank.shootCooldownMax = 5
      newTank.speed = 5
    else
      newTank.shootCooldownMax = 100
      newTank.speed = 1
    end

    table.insert( self.tanks, newTank )
  end,

  ---------------------------------------------------------------------- ADD STRUCTURE
  AddStructure = function( self, x, y )
    local newStructure = {}
    newStructure.width = 31
    newStructure.height = 31
    newStructure.x = x - ( newStructure.width / 2 )
    newStructure.y = y - ( newStructure.height / 2 )
    newStructure.hp = 3
    newStructure.active = true
    newStructure.type = "structure"
    table.insert( self.structures, newStructure )
  end,

  ---------------------------------------------------------------------- TANK SHOOT BULLET
  TankShootBullet = function( self, tankIndex )
    local tank = self.tanks[tankIndex]

    if ( tank == nil ) then return end

    if ( tank.shootCooldown > 0 ) then return end -- Can't shoot yet
    tank.shootCooldown = tank.shootCooldownMax

    local newBullet = {}
    newBullet.x = tank.x + (tank.width/2)
    newBullet.y = tank.y
    newBullet.speed = -2
    newBullet.width = 6
    newBullet.height = 10
    newBullet.active = true
    newBullet.type = "bullet"
    newBullet.subtype = "bullet"
    table.insert( self.bullets, newBullet )

    self["audio"]["bulletShoot"]:setVolume( dataManager.data.sound_volume / 100.0 )
    self["audio"]["bulletShoot"]:play() -- change
  end,

  ---------------------------------------------------------------------- ALIENS SHOOT LASER
  AliensShootLasers = function( self )
    if ( self.player.shootCooldown > 0 ) then return end -- Can't shoot yet
    self.player.shootCooldown = self.player.shootCooldownMax

    for key, alien in pairs( self.aliens ) do
      if ( alien.active ) then
        if ( alien.subtype == "alien" ) then
          local newBullet = {}
          newBullet.x = alien.x + (alien.width/2)
          newBullet.y = alien.y + alien.height
          newBullet.speed = 3
          newBullet.width = 5
          newBullet.height = 11
          newBullet.active = true
          newBullet.type = "bullet"
          newBullet.subtype = "laser"
          table.insert( self.bullets, newBullet )

        elseif ( alien.subtype == "alien2" ) then

          local x = alien.x + (alien.width/2) - 10

          for i = 0, 2 do
            local newBullet = {}
            newBullet.x = x
            newBullet.y = alien.y + alien.height
            newBullet.speed = 2
            newBullet.width = 5
            newBullet.height = 11
            newBullet.active = true
            newBullet.type = "bullet"
            if ( i == 0 ) then     newBullet.subtype = "laser-"
            elseif ( i == 1 ) then newBullet.subtype = "laser"
            elseif ( i == 2 ) then newBullet.subtype = "laser+"
            end
            table.insert( self.bullets, newBullet )

            x += 10
          end

        end
      end
    end

    self["audio"]["laserShoot"]:setVolume( dataManager.data.sound_volume / 100.0 )
    self["audio"]["laserShoot"]:play()
  end,

  ---------------------------------------------------------------------- SETUP LEVEL
  SetupLevel = function( self )
    self.data.phase = "levelmark"

    self.data.instructions = {}
    self.aliens = {}
    self.tanks = {}
    self.structures = {}
    self.bullets = {}
    self.deleteList = {}

    for key, item in pairs( self.levelData.placements ) do
      if ( item.type == "alien" ) then
        self:AddAlien( item.x, item.y, item.subtype )
      elseif ( item.type == "tank" ) then
        self:AddTank( item.x, item.y, item.subtype, item.minX, item.maxX )
      elseif ( item.type == "structure" ) then
        self:AddStructure( item.x, item.y )
      end
    end

    self.data.aliensRemaining = #self.aliens
    self.data.startingAliens = #self.aliens
    self.data.tanksRemaining = #self.tanks

    local levelInstructions = self.levelData.instructions

    for key, instruction in pairs( levelInstructions ) do
      table.insert( self.data.instructions, instruction )
    end

    --elseif ( self.data.level == 10 ) then
      --self.data.phase = "gameover_win"
      --self.buttonpressCooldown = 100
    --end
  end,

  ---------------------------------------------------------------------- BEGIN LEVEL
  BeginLevel = function( self )
    self.data.phase = "gameplay"
  end,

  ---------------------------------------------------------------------- DONE WITH LEVEL
  DoneWithLevel = function( self )
    self.data.phase = "levelend"

    self.stars = 0

    if ( self.data.aliensRemaining == self.data.startingAliens ) then
      self.stars = 3
    elseif ( self.data.aliensRemaining == 1 ) then
      self.stars = 1
    else
      self.stars = 2
    end

    -- 1 = locked, 2 = no stars, 3 = one star, 4 = two stars, 5 = three stars
    local score = self.stars + 2

    ---- Save score
    if ( dataManager["data"][global.gameKey]["levelStatuses"][global.levelKey].state < score ) then
      dataManager["data"][global.gameKey]["levelStatuses"][global.levelKey].state = score
    end

    -- Unlock next level
    if ( dataManager["data"][global.gameKey]["levelStatuses"][global.levelKey+1] ~= nil and dataManager["data"][global.gameKey]["levelStatuses"][global.levelKey+1].state == 1 ) then
      dataManager["data"][global.gameKey]["levelStatuses"][global.levelKey+1].state = 2
    end

    dataManager:SaveData()

    if ( self["audio"]["success"]:isPlaying() == false ) then
      self["audio"]["levelComplete"]:setVolume( dataManager.data.sound_volume / 100.0 )
      self["audio"]["levelComplete"]:play()
    end
  end,

  ---------------------------------------------------------------------- UPDATE
  Update = function( self )
    self:HandleInput()
        self.counter += 0.5

    if ( self.counter >= 3 ) then
      self.counter = 1
    end

    if ( self.data.phase == "levelmark" ) then
      self:Update_LevelMarker()

    elseif ( self.data.phase == "gameplay" ) then
      self:Update_Gameplay()

    end

    for key, effect in pairs ( self.effects ) do
      if ( effect.active ) then
        effect.countdown -= 1
        if ( effect.style == "explosion" and effect.countdown == effect.countdownMax / 2 ) then
          effect.frame = 2
        end
        if ( effect.countdown == 0 ) then
          self:DeactivateObject( key, effect )
        end
      end
    end
  end, -- Update = function( self )

  ---------------------------------------------------------------------- DRAW
  Draw = function( self )
    if ( self["data"]["phase"] == "levelmark" ) then
      self:Draw_LevelMarker()
    elseif ( self.data.phase == "gameplay" ) then
      self:Draw_Gameplay()
    elseif ( self.data.phase == "levelend" ) then
      self:Draw_LevelEnd()
    elseif ( self.data.phase == "gameover_lose" or self.data.phase == "gameover_win" ) then
      self:Draw_Gameover()
    end

    -- Draw effects
    for key, effect in pairs ( self.effects ) do
      if ( effect.active ) then
        self.tables.explosion:getImage( effect.frame ):draw( effect.x, effect.y )
      end
    end
  end, -- Draw = function( self )

  ---------------------------------------------------------------------- DRAW LEVEL END
  Draw_LevelEnd = function( self )
    if ( self.data.level == 9 ) then
      -- The last level
      self.images["win"]:draw( 0, 0 )
    else
      self.images["shadow"]:draw( 0, 0 )
      self.images["textbox"]:draw( 50, 30 )

      -- Display level summary
      gfx.setFont( fonts.pedallica )
      gfx.drawTextAligned( "SUCCESS!", 400/2, 50, kTextAlignment.center )
      gfx.drawTextAligned( "Press 'A' to continue!" , 400/2, 175, kTextAlignment.center )

      -- Draw stars
      local starWidth = 32
      self.tables.stars:getImage( 1 ):draw( 400/2 - (starWidth*2) - (starWidth/2), 100 )

      if ( self.stars >= 2 ) then
        self.tables.stars:getImage( 1 ):draw( 400/2 - (starWidth/2), 100 )
      else
        self.tables.stars:getImage( 2 ):draw( 400/2 - (starWidth/2), 100 )
      end

      if ( self.stars >= 3 ) then
        self.tables.stars:getImage( 1 ):draw( 400/2 + (starWidth*2) - (starWidth/2), 100 )
      else
        self.tables.stars:getImage( 2 ):draw( 400/2 + (starWidth*2) - (starWidth/2), 100 )
      end
    end
  end,
  ---------------------------------------------------------------------- LEVELMARKER, UPDATE
  Update_LevelMarker = function( self )
    if ( inputManager:IsKeyPressRelease( "a" ) ) then
      self:BeginLevel()
    end
  end,

  ---------------------------------------------------------------------- LEVELMARKER, DRAW
  Draw_LevelMarker = function( self )
    -- Fill background
    gfx.setColor( gfx.kColorWhite )
    gfx.fillRect( 0, 0, 400, 240 )

    self.images.shadow:draw( 0, 0 )
    self.images.textbox:draw( 50, 30 )

    gfx.setFont( fonts.rains )
    gfx.drawTextAligned( "LEVEL " .. tostring( self.data.level ), 400/2, 45, kTextAlignment.center )

    local y = 70
    local inc = 20

    local fontKey = self.levelData.instruction_font
    gfx.setFont( fonts[fontKey] )
    for key, inst in pairs( self.data.instructions ) do
      gfx.drawTextAligned( inst, 400/2, y, kTextAlignment.center )
      y += inc
    end
  end,

  ---------------------------------------------------------------------- GAMEOVER, DRAW
  Draw_Gameover = function( self )
      -- Fill background
    gfx.setColor( gfx.kColorWhite )
    gfx.fillRect( 0, 0, 400, 240 )

    if ( self.data.phase == "gameover_lose" ) then

      self.images.shadow:draw( 0, 0 )
      self.images.textbox:draw( 50, 30 )

      gfx.setFont( fonts.sasser )
      gfx.drawTextAligned( "The Misery Times", 400/2, 50, kTextAlignment.center )
      gfx.drawTextAligned( "ALIEN MENACE DEFEATED!", 400/2, 75, kTextAlignment.center )
      gfx.drawTextAligned( "U.S. RETURNS TO STATUS QUO.", 400/2, 100, kTextAlignment.center )
      gfx.drawTextAligned( "CORPORATIONS THRILLED", 400/2, 125, kTextAlignment.center )
      gfx.drawTextAligned( "FOR RECORD PROFITS!", 400/2, 145, kTextAlignment.center )
      gfx.drawTextAligned( "Game Over", 400/2, 175, kTextAlignment.center )

    else
      self.images.win:draw( 0, 0 )
    end
  end,

  ---------------------------------------------------------------------- GAMEPLAY, UPDATE
  Update_Gameplay = function( self )
    -- Cooldowns
    if ( self.player.shootCooldown > 0 ) then
      self.player.shootCooldown -= 1
    end

    -- Update tanks
    for key, tank in pairs( self.tanks ) do
      if ( tank.active ) then

        if ( tank.shootCooldown > 0 ) then
          tank.shootCooldown -= 1
        end

        local tankCenterX = tank.x + tank.width / 2

        if ( tank.behavior == "patrol" and ( tank.goal == 0 or tank.goal > #self.aliens ) ) then
          if ( #self.aliens > 0 ) then
            tank.goal = math.random( 1, #self.aliens )
          else
            self.goal = 0
          end
        end

        -- If a patrolling type, move towards an alien
        if ( tank.behavior == "patrol" and ( tank.goal ~= 0 or tank.goal > #self.aliens ) ) then
          local alienCenterX = self.aliens[tank.goal].x + self.aliens[tank.goal].width / 2

          if ( alienCenterX < tank.x and tank.x > tank.minX ) then
            tank.x -= tank.speed
          elseif ( alienCenterX > tank.x and tank.x < tank.maxX ) then
            tank.x += tank.speed
          end

        elseif ( tank.behavior == "bomber" ) then
          tank.x += tank.speed
          if ( tank.x > 400 ) then  tank.x = -35 end
        end

        for ak, alien in pairs( self.aliens ) do
          if ( alien.active ) then
            local alienCenterX = alien.x + alien.width / 2

            -- If aligned to an alien, then shoot at it!
            if ( tankCenterX >= alienCenterX - 5 and tankCenterX <= alienCenterX + 5 ) then
              self:TankShootBullet( key )
            end
          end
        end

      end -- if ( tank.active ) then
    end -- for key, tank in pairs( self.tanks ) do

    -- Update aliens
    for key, alien in pairs ( self.aliens ) do
      alien.frame += alien.animateSpeed
      if ( alien.frame >= alien.frameMax+1 ) then
        alien.frame = 1
      end
    end

    -- Update bullet(s)
    for key, bullet in pairs ( self.bullets ) do
      if ( bullet.active ) then
        bullet.y += bullet.speed

        if ( bullet.subtype == "laser-" ) then
          bullet.x -= 0.25

        elseif ( bullet.subtype == "laser+" ) then
          bullet.x += 0.25
        end
      end
    end

    -- Shoot laser!
    if ( inputManager:IsKeyPressed( "down" ) ) then
      self:AliensShootLasers()
    end

    -- Check for collisions
    for bk, bullet in pairs ( self.bullets ) do
      if ( bullet.active ) then
        local bulletCenter = { x = bullet.x + bullet.width / 2, y = bullet.y + bullet.height / 2 }

        if ( bullet.subtype == "laser" or bullet.subtype == "laser-" or bullet.subtype == "laser+" ) then
          -- Laser bullet vs. Structure
          for sk, structure in pairs ( self.structures ) do
            if ( structure.active ) then
              local structureCenter = { x = structure.x + structure.width / 2, y = structure.y + structure.height / 2 }
              local dist = GetDistance( bulletCenter, structureCenter )
              if ( dist < 15 ) then
                structure.hp -= 1
                self:AddEffect( structure.x, structure.y, "explosion" )
                self:DeactivateObject( bk, bullet )
              end

              if ( structure.hp == 0 ) then
                self:DeactivateObject( sk, structure )
              end
            end -- if ( structure.active ) then
          end

          -- Laser bullet vs. Tank
          for tk, tank in pairs ( self.tanks ) do
            if ( tank.active ) then
              local tankCenter = { x = tank.x + tank.width / 2, y = tank.y + tank.height / 2 }
              local dist = GetDistance( bulletCenter, tankCenter )
              if ( dist < 15 ) then
                self:AddEffect( tank.x, tank.y, "explosion" )
                self:DeactivateObject( bk, bullet )
                self:DeactivateObject( tk, tank )
              end
            end -- if ( tank.active ) then
          end -- for tk, tank in pairs ( self.tanks ) do


        elseif ( bullet.subtype == "bullet" ) then
          -- Bullet vs. Alien
          for ak, alien in pairs( self.aliens ) do
            local alienCenter = { x = alien.x + alien.width / 2, y = alien.y + alien.height / 2 }
            local dist = GetDistance( bulletCenter, alienCenter )
            if ( dist < 15 ) then
              self:AddEffect( alien.x, alien.y, "explosion" )
              self:DeactivateObject( bk, bullet )
              self:DeactivateObject( ak, alien )
            end
          end
        end -- if ( bullet.subtype == "laser" ) then
      end -- if ( bullet.active ) then
    end -- for bk, bullet in pairs ( self.bullets ) do

    -- Delete stuff
    -- Not going to delete bullets during the level
    -- because it seems to be causing slowdown,
    -- so they will be removed during the level end

    if ( self.data.aliensRemaining <= 0 ) then
      self:OutOfAliens()
    end
    if ( self.data.tanksRemaining <= 0 ) then
      self:DoneWithLevel()
    end
  end,

  ---------------------------------------------------------------------- GAMEPLAY, DRAW
  Draw_Gameplay = function( self )
    -- Fill background
    gfx.setColor( gfx.kColorBlack )
    gfx.fillRect( 0, 0, 400, 240 )

    -- Draw aliens
    for key, alien in pairs ( self.aliens ) do
      if ( alien.active ) then
        if ( alien.subtype == "alien" ) then
          self.tables.alien:getImage( math.floor( alien.frame ) ):draw( alien.x, alien.y )
        elseif ( alien.subtype == "alien2" ) then
          self.tables.alien2:getImage( math.floor( alien.frame ) ):draw( alien.x, alien.y )
        end
      end
    end

    -- Draw bunkers
    for key, structure in pairs ( self.structures ) do
      if ( structure.active ) then
        local frame = 4 - structure.hp
        self.tables.brickwall:getImage( frame ):draw( structure.x, structure.y )
      end
    end

    -- Draw tanks
    for key, tank in pairs( self.tanks ) do
      if ( tank.active ) then
        if ( tank.behavior == "static" ) then
          self.images.tank:draw( tank.x, tank.y )
        elseif ( tank.behavior == "patrol" ) then
          self.images.tankPatrol:draw( tank.x, tank.y )
        elseif ( tank.behavior == "bomber" ) then
          self.images.tankBomber:draw( tank.x, tank.y )
        end
      end
    end

    -- Draw bullets
    for key, bullet in pairs ( self.bullets ) do
      local frame = math.floor( self.counter )
      if ( bullet.active ) then
        if ( bullet.subtype == "laser" or bullet.subtype == "laser-" or bullet.subtype == "laser+" ) then
          self.tables.laser:getImage( frame ):draw( bullet.x, bullet.y )
        elseif ( bullet.subtype == "bullet" ) then
          self.tables.bullet:getImage( frame ):draw( bullet.x, bullet.y )
        end
      end
    end

    playdate.drawFPS( 380, 5 )
  end,

  ---------------------------------------------------------------------- HANDLE INPUT
  HandleInput = function( self )
    inputManager:Handle_Crank()

    if ( #self.aliens == 0 ) then return end -- no more aliens!

    local lastIndex = #self.aliens

    if ( self.data.phase == "levelmark" ) then
      if ( inputManager:IsKeyPressRelease( "a" ) ) then
        self:BeginLevel()
      end

    elseif ( self.data.phase == "levelend" ) then
      if ( inputManager:IsKeyPressRelease( "a" ) ) then
        if ( self.data.level == 9 ) then
          self.data.phase = "gameover_win"
        else
          self.nextState = "levelSelectScreen"
        end
      end

    elseif ( self.data.phase == "gameover_lose" or self.data.phase == "gameover_win" ) then
      if ( inputManager:IsKeyPressRelease ( "a" ) ) then
        self.nextState = "levelSelectScreen"
      end

    elseif ( self.data.phase == "gameplay" ) then

      if ( inputManager.crank.changeDegrees < 0 ) then
        -- MOVING LEFT
        self.aliens[1].x += inputManager.crank.changeDegrees
        if ( self.aliens[1].x < 0 ) then
          self.aliens[1].x = 0
        end

        for i=2, lastIndex do
          self.aliens[i].x = self.aliens[i-1].x + 50
        end

      elseif ( inputManager.crank.changeDegrees > 0 ) then
        -- MOVING RIGHT
        self.aliens[lastIndex].x += inputManager.crank.changeDegrees
        if ( self.aliens[lastIndex].x > 400-self.aliens[lastIndex].width ) then
          self.aliens[lastIndex].x = 400-self.aliens[lastIndex].width
        end

        for i=lastIndex-1, 1, -1 do
          self.aliens[i].x = self.aliens[i+1].x - 50
        end
      end

    end
  end,

  ---------------------------------------------------------------------- SETUP LEVEL DATA
  SetupLevelData = function( self )
    levels = {
          {
            instruction_font = "rains",
            instructions = {
              "WE HAVE RECEIVED A DISTRESS",
              "SIGNAL FROM PEOPLE ON A",
              "FAR OFF PLANET.",
              "WE ARE HERE TO RESPOND TO",
              "WHAT THREATENS THEM.",
              "CRANK = MOVE, DOWN = SHOOT LASER"
            },
            placements = {
              { type = "alien", subtype="alien",  x = 0,      y = 20 },
              { type = "alien", subtype="alien",  x = 45,     y = 20 },
              { type = "tank",  subtype="static", x = 400/2,  y = 200, minX = 400/2, maxX = 400/2 },
              { type = "structure", x = 100,  y = 150 },
              { type = "structure", x = 200,  y = 150 },
              { type = "structure", x = 300,  y = 150 },
            },
          },
          {
            instruction_font = "sasser",
            instructions = {
              "The Misery Times",
              "EXTRATERRESTRIALS ATTACK!",
              "KANSAS CITY IN CHAOS!",
              "FUNDING TO POLICE INCREASED!"
            },
            placements = {
              { type = "alien", subtype="alien",  x = 0,          y = 20 },
              { type = "alien", subtype="alien",  x = 45,         y = 20 },
              { type = "alien", subtype="alien",  x = 90,         y = 20 },
              { type = "tank",  subtype="patrol", x = 400*0.25,   y = 200, minX = 0, maxX = 133 },
              { type = "tank",  subtype="patrol", x = 400*0.50,   y = 200, minX = 133, maxX = 266 },
              { type = "tank",  subtype="patrol", x = 400*0.75,   y = 200, minX = 266, maxX = 400 },
              { type = "structure", x = 75,   y = 190 },
              { type = "structure", x = 100,  y = 175 },
              { type = "structure", x = 133,  y = 190 },
              { type = "structure", x = 200,  y = 175 },
              { type = "structure", x = 266,  y = 190 },
              { type = "structure", x = 300,  y = 175 },
              { type = "structure", x = 325,  y = 190 },
            },
          },
          {
            instruction_font = "sasser",
            instructions = {
              "ALIENS BREACH THE PERIMETER!",
              "MANY MISSOURIANS ABDUCTED!",
              "WORKER SHORTAGE AT",
              "ALL TIME HIGH!"
            },
            placements = {
              { type = "alien", subtype="alien",  x = 0,          y = 20 },
              { type = "alien", subtype="alien2", x = 45,         y = 30 },
              { type = "alien", subtype="alien",  x = 90,         y = 20 },
              { type = "tank",  subtype="static", x = 15,         y = 200, minX = 15,  maxX = 15 },
              { type = "tank",  subtype="patrol", x = 400*0.25,   y = 200, minX = 15,  maxX = 133 },
              { type = "tank",  subtype="patrol", x = 400*0.50,   y = 200, minX = 133, maxX = 266 },
              { type = "tank",  subtype="patrol", x = 400*0.75,   y = 200, minX = 266, maxX = 400-15 },
              { type = "tank",  subtype="static", x = 400-15,     y = 200, minX = 400, maxX = 400-15 },
              { type = "structure", x = 10,   y = 160 },
              { type = "structure", x = 85,   y = 160 },
              { type = "structure", x = 160,  y = 160 },
              { type = "structure", x = 235,  y = 160 },
              { type = "structure", x = 310,  y = 160 },
              { type = "structure", x = 385,  y = 160 },
            },
          },
          {
            instruction_font = "sasser",
            instructions = {
              "\"NOBODY WANTS TO",
              "WORK ANYMORE!",
              "ALL THESE LAZY",
              "MILLENNIALS ARE GOING",
              "WITH THE ALIENS!\"",
              "-- Local business owner"
            },
            placements = {
              { type = "alien", subtype="alien",  x = 0,          y = 20 },
              { type = "alien", subtype="alien2", x = 45,         y = 30 },
              { type = "alien", subtype="alien",  x = 90,         y = 20 },
              { type = "tank",  subtype="static", x = 400/2,      y = 200,  minX = 400/2,    maxX = 400/2 },
              { type = "tank",  subtype="patrol", x = 400*0.25,   y = 180,  minX = 0,        maxX = 400*0.25 },
              { type = "tank",  subtype="patrol", x = 400*0.75,   y = 180,  minX = 400*0.75, maxX = 400 },
              { type = "structure", x = 200+35*2,   y = 170 },
              { type = "structure", x = 200+35*3,   y = 170 },
              { type = "structure", x = 200+35*4,   y = 170 },
              { type = "structure", x = 200-35*2,   y = 170 },
              { type = "structure", x = 200-35*3,   y = 170 },
              { type = "structure", x = 200-35*4,   y = 170 },
            },
          },
          {
            instruction_font = "sasser",
            instructions = {
              "MILLENNIALS ARE KILLING",
              "THE \"NOT-BEING-ABDUCTED\"",
              "INDUSTRY!",
              "CHURCHES BLAME ALIENS FOR",
              "DEVIANT OUTERSPACE LIFESTYLE!"
            },
            placements = {
              { type = "alien", subtype="alien",  x = 0,          y = 20 },
              { type = "alien", subtype="alien2", x = 45,         y = 30 },
              { type = "alien", subtype="alien",  x = 90,         y = 20 },
              { type = "tank",  subtype="bomber", x = 400*0.50,   y = 140,  minX = 0,        maxX = 400 },
              { type = "tank",  subtype="patrol", x = 400/2,      y = 200,  minX = 0,        maxX = 400 },
              { type = "tank",  subtype="static", x = 135,        y = 150,  minX = 135,      maxX = 135 },
              { type = "tank",  subtype="static", x = 250,        y = 150,  minX = 250,      maxX = 250 },
              { type = "structure", x = 100,        y = 190 },
              { type = "structure", x = 131,        y = 190 },
              { type = "structure", x = 162,        y = 190 },
              { type = "structure", x = 193,        y = 190 },
              { type = "structure", x = 224,        y = 190 },
              { type = "structure", x = 255,        y = 190 },
              { type = "structure", x = 286,        y = 190 },
              { type = "structure", x = 100,        y = 190-35 },
              { type = "structure", x = 286,        y = 190-35 },
            },
          },
          {
            instruction_font = "sasser",
            instructions = {
              "THE U.S. GOVERNMENT",
              "HAS BANNED INTERACTION",
              "WITH SPACE ALIENS,",
              "CITING TANKING ECONOMY",
              "AS GEN-Z MOVEMENT WELCOMES",
              "\"COMMIE EXTRATERRESTRIALS\"."
            },
            placements = {
              { type = "alien", subtype="alien",  x = 0,          y = 20 },
              { type = "alien", subtype="alien2", x = 45,         y = 30 },
              { type = "alien", subtype="alien",  x = 90,         y = 20 },
              { type = "tank",  subtype="bomber", x = 400*0.25,   y = 180,  minX = 0,        maxX = 400 },
              { type = "tank",  subtype="bomber", x = 400*0.75,   y = 180,  minX = 0,        maxX = 400 },
              { type = "tank",  subtype="static", x = 50,         y = 140,  minX = 0,        maxX = 0 },
              { type = "tank",  subtype="static", x = 350,        y = 140,  minX = 0,        maxX = 0 },
              { type = "tank",  subtype="patrol", x = 0,          y = 160,  minX = 0,        maxX = 150 },
              { type = "tank",  subtype="patrol", x = 250,        y = 160,  minX = 250,      maxX = 400 },
              { type = "structure", x = 10-25,   y = 230 },
              { type = "structure", x = 10,      y = 160 },
              { type = "structure", x = 85-25,   y = 230 },
              { type = "structure", x = 85,      y = 160 },
              { type = "structure", x = 160-25,  y = 230 },
              { type = "structure", x = 160,     y = 160 },
              { type = "structure", x = 235-25,  y = 230 },
              { type = "structure", x = 235,     y = 160 },
              { type = "structure", x = 310-25,  y = 230 },
              { type = "structure", x = 310,     y = 160 },
              { type = "structure", x = 385-25,  y = 230 },
              { type = "structure", x = 385,     y = 160 },
            },
          },
          {
            instruction_font = "sasser",
            instructions = {
              "TEXAS SCHOOL DISTRICT",
              "VOTES TO BAN ANY TALK",
              "ABOUT EXTRATERRESTRIALS.",
              "\"IT WILL JUST GIVE",
              "OUR CHILDREN BAD IDEAS.\""
            },
            placements = {
              { type = "alien", subtype="alien2", x = 0,          y = 30 },
              { type = "alien", subtype="alien2", x = 45,         y = 30 },
              { type = "tank",  subtype="bomber", x = 400*0.25,   y = 150,  minX = 0,  maxX = 400 },
              { type = "tank",  subtype="static", x = 100,        y = 210,  minX = 0,  maxX = 0 },
              { type = "tank",  subtype="static", x = 200,        y = 210,  minX = 0,  maxX = 0 },
              { type = "tank",  subtype="static", x = 300,        y = 210,  minX = 0,  maxX = 0 },
              { type = "tank",  subtype="static", x = 400,        y = 210,  minX = 0,  maxX = 0 },
              { type = "structure", x = 000-15,   y = 170 },
              { type = "structure", x = 000+15,   y = 170 },
              { type = "structure", x = 100-15,   y = 170 },
              { type = "structure", x = 100+15,   y = 170 },
              { type = "structure", x = 200-15,   y = 170 },
              { type = "structure", x = 200+15,   y = 170 },
              { type = "structure", x = 300-15,   y = 170 },
              { type = "structure", x = 300+15,   y = 170 },
              { type = "structure", x = 400-15,   y = 170 },
              { type = "structure", x = 400+15,   y = 170 },
            },
          },
          {
            instruction_font = "sasser",
            instructions = {
              "FEW MILLENNIALS, GEN-Z",
              "REMAIN IN U.S.!",
              "GEN-X BEGIN TAKING CONTROL",
              "AMID MASS DEATHS OF",
              "BABY BOOMERS / OLD AGE!"
            },
            placements = {
              { type = "alien", subtype="alien",  x = 0,          y = 30 },
              { type = "alien", subtype="alien2", x = 30,         y = 30 },
              { type = "alien", subtype="alien2", x = 60,         y = 30 },
              { type = "alien", subtype="alien",  x = 90,         y = 30 },
              { type = "tank",  subtype="bomber", x = 400*0.50,   y = 175,  minX = 0,    maxX = 400 },
              { type = "tank",  subtype="patrol", x = 50,         y = 200,  minX = 0,    maxX = 100 },
              { type = "tank",  subtype="patrol", x = 400-50,     y = 200,  minX = 300,  maxX = 400 },
              { type = "tank",  subtype="patrol", x = 400/2,      y = 200,  minX = 100,  maxX = 300 },
              { type = "structure", x = 000-15,   y = 170 },
              { type = "structure", x = 000+15,   y = 170 },
              { type = "structure", x = 100-15,   y = 170 },
              { type = "structure", x = 100+15,   y = 170 },
              { type = "structure", x = 200-15,   y = 170 },
              { type = "structure", x = 200+15,   y = 170 },
              { type = "structure", x = 300-15,   y = 170 },
              { type = "structure", x = 300+15,   y = 170 },
              { type = "structure", x = 400-15,   y = 170 },
              { type = "structure", x = 400+15,   y = 170 },
            },
          },
          {
            instruction_font = "sasser",
            instructions = {
              "GEN-X PRESIDENT SAYS \"MEH.\"",
              "\"LANDBACK\" MOVEMENT",
              "SUCCEEDS ACROSS U.S.",
            },
            placements = {
              { type = "alien", subtype="alien2", x = 0,          y = 30 },
              { type = "alien", subtype="alien2", x = 30,         y = 30 },
              { type = "alien", subtype="alien2", x = 60,         y = 30 },
              { type = "tank",  subtype="static", x = 50,         y = 200,  minX = 0,    maxX = 0 },
              { type = "tank",  subtype="static", x = 350,        y = 200,  minX = 0,    maxX = 0 },
              { type = "tank",  subtype="patrol", x = 100,        y = 200,  minX = 100,  maxX = 200 },
              { type = "tank",  subtype="patrol", x = 200,        y = 140,  minX = 100,  maxX = 300 },
              { type = "tank",  subtype="patrol", x = 300,        y = 200,  minX = 200,  maxX = 300 },
              { type = "tank",  subtype="bomber", x = 100,        y = 145,  minX = 0,  maxX = 0 },
              { type = "tank",  subtype="bomber", x = 300,        y = 145,  minX = 0,  maxX = 0 },
              { type = "structure", x = 05,   y = 200 },
              { type = "structure", x = 50,   y = 200 },
              { type = "structure", x = 95,   y = 200 },
              { type = "structure", x = 140,  y = 200 },
              { type = "structure", x = 185,  y = 200 },
              { type = "structure", x = 230,  y = 200 },
              { type = "structure", x = 275,  y = 200 },
              { type = "structure", x = 320,  y = 200 },
              { type = "structure", x = 365,  y = 200 },
            },
          },
        }
    return levels
  end,
}

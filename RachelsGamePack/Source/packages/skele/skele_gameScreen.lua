import 'CoreLibs/graphics.lua'
import 'CoreLibs/sprites.lua'
import 'manager_gamedata.lua'
import 'manager_input.lua'
import 'global.lua'
import 'utilities.lua'
local gfx = playdate.graphics
local snd = playdate.sound

-- TODO: Game over state
-- attack animation
-- Change music on battle screen?
-- Level end state

skele_gameScreen = {
  ---------------------------------------------------------------------- INIT
  Init = function( self )
    self.nextState = ""   -- Set this when you're ready to go to another state

    self.gameData = dataManager["data"][global.gameKey]

    self.phase = "play"
    self.deliverFromList = {}

    self.images = {
      shadow  = gfx.image.new( "images/transparent.png" ),
      textbox = gfx.image.new( "images/textbox.png" ),
      win = gfx.image.new( "packages/skele/images/win.png" )
    }

    self.tables = {
      fist = nil
    }

    self.audio = {
      enemyAttack = snd.sampleplayer.new( "audio/sfx1.wav" ),
      playerHurt = snd.sampleplayer.new( "audio/oof.wav" ),
      enemyHurt = snd.sampleplayer.new( "audio/sfx4.wav" )
    }

    self.player = {
      score = 0,
      locationX = 0,
      locationY = 0,
      questActive = false,
      health = 100,
      healthMax = 100,
      punchCooldown = 0,
      punchCooldownMax = 20,
      punchDamage = 5,
      isDefending = false,
      hurtTimerMax = 40,
      hurtTimer = 0,
    }

    local crankRatio = inputManager.crank.angleDegrees / 360

    self.view = {
      angle = inputManager.crank.angleDegrees,
      xOffset =  -(240*4) * crankRatio,
      turnSpeed = 1,
      viewWidth = 240*4,
      currentDirection = "north",
      moveTimer = 0,
      moveTimerMax = 10
    }

    self.gameFinishTimer = 0

    self.businessSayings = {
      entrepreneur = {
        "It's Humpday!",
        "Any weekend plans?",
        "Teamwork makes the dream work!",
        "There's no \"I\" in team!",
        "We're like a family here!",
        "We only hire rockstars!"
      },
      hr = {
        "Why's it so cold in the office?",
        "Let me tell you about my dog!",
        "Is it Friday yet??",
        "Office potluck on Thursday!",
        "How about this weather?",
      },
      marketing = {
        "Workin hard or hardly workin?",
        "Mondays, am I right?",
        "Happy hour Friday this week?",
        "That's what I call synergizing!",
        "How about them sportsteam?",
        "Work hard, play hard!"
      },
      developer = {
        "I hate it here.",
        "Tacos for lunch?",
        "When do I get paid?",
        "My code's compiling."
      },
      ghost = {
        "Can we schedule a call?",
        "We need to follow up.",
        "Let's get lunch together.",
        "Will you help me debug?"
      }
    }

    self.characters = {}
    self.enemies = {}
    self.questDoneTimeout = 0
    self.questDoneTimeoutMax = 100

    self.effects = {
    }

    self.onetwo = 1
    self.ghostMove = 100

    self.images["mapArrow"] = gfx.image.new( "packages/skele/images/map-arrow.png" )
    self.images["fade"] = gfx.image.new( 400, 240, gfx.kColorBlack )
    self.images["questBegin"] = gfx.image.new( "packages/skele/images/map-quest.png" )
    self.images["questEnd"] = gfx.image.new( "packages/skele/images/map-destination.png" )
    self.images["mapCharacter"] = gfx.image.new( "packages/skele/images/map-character.png" )
    self.images["mapGhost"] = gfx.image.new( "packages/skele/images/map-ghost.png" )
    self.images["clipboard"] = gfx.image.new( "packages/skele/images/clipboard.png" )
    self.images["clipboard-defend"] = gfx.image.new( "packages/skele/images/clipboard-defend.png" )
    self.images["speechBalloon"] = gfx.image.new( "images/speech_balloon.png" )
    self.images["responseBalloon" ] = gfx.image.new( "packages/skele/images/response-balloon.png" )
    self.images["actionBalloon"] = gfx.image.new( "packages/skele/images/action-balloon.png" )
    self.images["actionBalloonBig"] = gfx.image.new( "packages/skele/images/action-balloon-big.png" )
    self.images["mail"] = gfx.image.new( "packages/skele/images/mail.png" )
    self.images["pencil-top"] = gfx.image.new( "packages/skele/images/pencil-top.png" )
    self.images["pencil-mid"] = gfx.image.new( "packages/skele/images/pencil-mid.png" )
    self.images["pencil-bottom"] = gfx.image.new( "packages/skele/images/pencil-bottom.png" )


    self.tables["fist"], result = TryLoadImageTable( "packages/skele/images/punch" )
    self.tables["corridor"], result = TryLoadImageTable( "packages/skele/images/corridor" )
    self.tables["characters"], result = TryLoadImageTable( "packages/skele/images/characters" )
    self.tables["enemies"], result = TryLoadImageTable( "packages/skele/images/enemy" )
    self.tables["buttonPrompts"], result = TryLoadImageTable( "images/buttonprompts" )

    self.roomCountWidth = 10
    self.roomCountHeight = 10
    self.mapArray = {}
    self:GenerateMaze()

    -- Copy coworkers to the "deliver from list" to keep track of who has had their mail delivered
    self.deliverFromList = nil
    self.deliverFromList = {}
    for key, chara in pairs( self.characters ) do
      self.deliverFromList[ key ] = chara.name
    end

    PlaySong( self.gameData.song )
  end, -- Init = function( self )

  ---------------------------------------------------------------------- CLEAN UP
  Cleanup = function( self )
    -- Free space here as needed, stop music
    self.nextState = nil
    self.phase = nil
    self.images = nil
    self.tables = nil
    self.timer = nil
    self.player = nil
    self.effects = nil
    self.gameData = nil
    self.timer = nil
    self.mapArray = nil
  end, -- Cleanup = function( self )

  ---------------------------------------------------------------------- UPDATE TIMERS
  UpdateTimers = function( self )

    -- Punch timer
    if ( self.player.punchCooldown > 0 ) then
      self.player.punchCooldown -= 1
    end

    -- Make sure player can't leave win screen too quickly
    if ( self.gameFinishTimer > 0 ) then
      self.gameFinishTimer -= 1
    end

    -- This timer refreshes the coworker positions after a few moments
    if ( self.questDoneTimeout > 0 ) then
      self.questDoneTimeout = self.questDoneTimeout - 1
      if ( self.questDoneTimeout == 0 ) then
        self:GenerateQuest()
      end
    end

    -- This is for the button prompt animation
    self.onetwo = self.onetwo + 0.1
    if ( self.onetwo >= 3 ) then
      self.onetwo = 1
    end

    -- This is the hurt cooldown timer
    if ( self.player.hurtTimer > 0 ) then
      self.player.hurtTimer -= 1
    end

    -- This is the enemy behavior timer
    for key, enemy in pairs( self.enemies ) do
      enemy.timer += 1
      if ( enemy.timer < enemy.timerMax / 2 ) then
        enemy.state = "idle"
      else
        enemy.state = "attacking"
      end

      if ( enemy.timer >= enemy.timerMax ) then
        enemy.timer = 0
      end

      if ( enemy.hurtTimer > 0 ) then
        enemy.hurtTimer -= 1
      end
    end

    if ( self.view["moveTimer"] > 0 ) then
      self.view["moveTimer"] -= 1

      if ( self.view["moveTimer"] == self.view["moveTimerMax"]/2 ) then
        self.player.locationX = self.player.moveToX
        self.player.locationY = self.player.moveToY
      end
    end

    if ( self.player.enemyEncounter == false ) then
      self.ghostMove -= 1
      if ( self.ghostMove <= 0 ) then
        self:SetupEnemies()
        self.ghostMove = 100
      end
    end
  end,

  ---------------------------------------------------------------------- UPDATE
  Update = function( self )
    self:HandleInput()

    playdate.timer.updateTimers()
    gfx.sprite.update() -- update all sprite objects

    self:UpdateTimers()

    if ( self["phase"] == "play" ) then

      self.player.enemyEncounter = false
      local removeKeys = {}
      for key, enemy in pairs( self.enemies ) do

        if ( enemy.x == self.player.locationX and enemy.y == self.player.locationY ) then
          self.player.enemyEncounter = true

          if ( enemy.timer == enemy.timerMax/2 ) then
            enemy.attackText = self.businessSayings["ghost"][ math.random( 1, #self.businessSayings.ghost ) ]

            -- Is the player defending?
            if ( self.player.isDefending == false ) then
              self:GetHurt( enemy.damage )
              --else
              --enemy.health -= 50
            end
          end

          if ( enemy.timer < enemy.timerMax / 2 and self.player.isAttacking ) then
            self:HurtEnemy( key, 50 )
          end

          if ( enemy.health <= 0 ) then
            table.insert( removeKeys, key )
          end

        end
      end

      for key, enemyKey in pairs( removeKeys ) do
        self.enemies[ enemyKey ] = nil
      end

      if ( self["player"]["health"] <= 0 and self["phase"] == "play" ) then
        self["phase"] = "gameover"
        self.gameFinishTimer = 100
        self:GameOver()
      end

    elseif ( self["phase"] == "gameover" or self["phase"] == "win" ) then
      if ( inputManager:IsKeyPressRelease( "a" ) and self.gameFinishTimer <= 0 ) then
        self.nextState = "titleScreen"
      end

    elseif ( self["phase"] == "win" ) then
    end

  end, -- Update = function( self )

  ---------------------------------------------------------------------- CREATE MAZE
  GenerateMaze = function( self )
    self.roomCountWidth = 10
    self.roomCountHeight = 10
    local pointCount = math.random( 5, 10 )

    -- Create 2D "array" to represent rooms
    -- Use 0 to represent blank space
    self.mapArray = nil
    self.mapArray = {}
    for x = 0, self.roomCountWidth do
      self.mapArray[x] = {}
      for y = 0, self.roomCountHeight do
        self.mapArray[x][y] = { tile = 0, obj = nil, seen = false, type = math.random( 1, 4 ) }
      end
    end

    -- Create "rooms" (points on a map)
    self.nodes = {}
    self.validRooms = {}
    for i = 0, pointCount do
      local randX = math.random( 1, self.roomCountWidth-1 )
      local randY = math.random( 1, self.roomCountHeight-1 )
      table.insert( self.nodes, { x = randX, y = randY } )
      table.insert( self.validRooms, { x = randX, y = randY } )
    end

    -- Make sure every room is connected
    local endX = 0
    local endY = 0
    for index1 = 1, #self.nodes-1 do
      local index2 = index1+1

      local startX = self.nodes[index1]["x"]
      local startY = self.nodes[index1]["y"]
      endX = self.nodes[index2]["x"]
      endY = self.nodes[index2]["y"]

      -- Mark these as rooms
      self.mapArray[ startX ][ startY ]["tile"] = 1
      self.mapArray[ endX ][ endY ]["tile"] = 1
      -- Set the type of room it is

      -- Traverse between
      while ( startX ~= endX ) do
        if ( endX > startX ) then startX += 1
        else startX -= 1
        end
        self.mapArray[ startX ][ startY ]["tile"] = 1
        table.insert( self.validRooms, { x = startX, y = startY } )
      end

      while ( startY ~= endY ) do
        if ( endY > startY ) then startY += 1
        else startY -= 1
        end
        self.mapArray[ startX ][ startY ]["tile"] = 1
        table.insert( self.validRooms, { x = startX, y = startY } )
      end
    end

    local startX = self.nodes[1]["x"]
    local startY = self.nodes[1]["y"]

    self.mapArray[startX][startY]["obj"] = "start"
    self.mapArray[endX][endY]["obj"] = "end"

    self.player["locationX"] = startX
    self.player["locationY"] = startY

    self:GenerateQuest()
  end,

  ---------------------------------------------------------------------- FINISH "QUEST"
  FinishQuest = function( self )
    self.player.questActive = false
    self.questDoneTimeout = self.questDoneTimeoutMax

    self.deliverFromList[ self.gameData[ "beginQuestCharacter" ] ] = nil
    -- Reset all characters
    for key, char in pairs( self.characters ) do
      char.beginQuest = false
      char.endQuest = false
      self.gameData[ "beginQuestCharacter" ] = nil
      self.gameData[ "endQuestCharacter" ] = nil
    end

    if ( #self.deliverFromList == 0 ) then
      self["phase"] = "win"
      self.gameFinishTimer = 100
    end
  end,

  ---------------------------------------------------------------------- GENERATE "QUEST"
  GenerateQuest = function( self )
    self:SetupCoworkers()
    self:SetupEnemies()

    local charA = math.random( 1, #self.characters )
    local charB = math.random( 1, #self.characters )
    -- Don't allow delivering mail to self:
    while ( charB == charA ) do
      charB = math.random( 1, #self.characters )
    end

    self.gameData[ "beginQuestCharacter" ] = charA
    self.gameData[ "endQuestCharacter" ] = charB

    self.characters[ charA ].beginQuest = true
    self.characters[ charB ].endQuest = true  end,

  ---------------------------------------------------------------------- SETUP ENEMIES
  SetupEnemies = function( self )
    self.enemies = nil
    self.enemies = {}

    local alreadyTaken = {}
    local nodesCopy = self.validRooms

    for i=1, 4 do
      local randomIndex = math.random( 1, #nodesCopy )
      while ( alreadyTaken[ randomIndex] == true ) do
        randomIndex = math.random( 1, #nodesCopy )
      end

      local text = self.businessSayings["ghost"][ math.random( 1, #self.businessSayings["ghost"] ) ]

      table.insert( self.enemies, { type = 1, x = nodesCopy[randomIndex]["x"], y = nodesCopy[randomIndex]["y"], timer = 0, timerMax = 160, damage = 10, health = 100, healthMax = 100, hurtTimer = 0, hurtTimerMax = 40, state = "idle", attackText = text } )

      alreadyTaken[ randomIndex ] = true
    end

  end,

  ---------------------------------------------------------------------- SETUP COWORKERS
  SetupCoworkers = function( self )
    -- Create characters on the map
    local alreadyTaken = {}
    self.characters = nil
    self.characters = {}

    local nodesCopy = self.validRooms

    local randomSayingE = self.businessSayings["entrepreneur"][ math.random( 1, #self.businessSayings["entrepreneur"] ) ]
    local randomSayingH = self.businessSayings["hr"][ math.random( 1, #self.businessSayings["hr"] ) ]
    local randomSayingM = self.businessSayings["marketing"][ math.random( 1, #self.businessSayings["marketing"] ) ]
    local randomSayingD = self.businessSayings["developer"][ math.random( 1, #self.businessSayings["developer"] ) ]

    -- CHARACTER 1
    local randomIndex = math.random( 1, #nodesCopy )
    table.insert( self.characters, { name = "The Overlord", type = 1, x = nodesCopy[randomIndex]["x"], y = nodesCopy[randomIndex]["y"], edge = math.random( 2, 3 ), saying = randomSayingE } )
    alreadyTaken[ randomIndex ] = true

    -- CHARACTER 2
    randomIndex = math.random( 1, #nodesCopy )
    while ( alreadyTaken[ randomIndex] == true ) do
      randomIndex = math.random( 1, #nodesCopy )
    end
    table.insert( self.characters, { name = "HR Gorgon", type = 2, x = nodesCopy[randomIndex]["x"], y = nodesCopy[randomIndex]["y"], edge = math.random( 2, 3 ), saying = randomSayingH } )
    alreadyTaken[ randomIndex ] = true

    -- CHARACTER 3
    randomIndex = math.random( 1, #nodesCopy )
    while ( alreadyTaken[ randomIndex] == true ) do
      randomIndex = math.random( 1, #nodesCopy )
    end
    table.insert( self.characters, { name = "Marketing Vamp", type = 3, x = nodesCopy[randomIndex]["x"], y = nodesCopy[randomIndex]["y"], edge = math.random( 2, 3 ), saying = randomSayingM } )
    alreadyTaken[ randomIndex ] = true

    -- CHARACTER 4
    randomIndex = math.random( 1, #nodesCopy )
    while ( alreadyTaken[ randomIndex] == true ) do
      randomIndex = math.random( 1, #nodesCopy )
    end
    table.insert( self.characters, { name = "Developer Imp", type = 4, x = nodesCopy[randomIndex]["x"], y = nodesCopy[randomIndex]["y"], edge = math.random( 2, 3 ), saying = randomSayingD } )
    alreadyTaken[ randomIndex ] = true

  end,

  ---------------------------------------------------------------------- GET HURT
  GetHurt = function( self, damage )
    if ( self.player.hurtTimer <= 0 ) then
      self["player"]["health"] -= damage
      self["audio"]["playerHurt"]:setVolume( dataManager.data.sound_volume / 100.0 )
      self["audio"]["playerHurt"]:play()
      self.player.hurtTimer = self.player.hurtTimerMax
    end
  end,

  ---------------------------------------------------------------------- HURT ENEMY
  HurtEnemy = function( self, enemyKey, damage )
    if ( self.enemies[ enemyKey ].hurtTimer <= 0 ) then
      self["audio"]["enemyHurt"]:setVolume( dataManager.data.sound_volume / 100.0 )
      self["audio"]["enemyHurt"]:play()
      self.enemies[ enemyKey ].health -= damage
      self.enemies[ enemyKey ].hurtTimer = self.enemies[ enemyKey ].hurtTimerMax
    end
  end,

  ---------------------------------------------------------------------- GAME OVER
  GameOver = function( self )

    -- Check for high score
    local highscore = dataManager:GetHighScore( global.gameKey )
    if ( highscore ~= nil and type( highscore ) ~= "table" ) then
      if ( self.player.score <= highscore ) then
        dataManager:SetHighScore( global.gameKey, self.player.score )
      end
    else
      dataManager:SetHighScore( global.gameKey, self.player.score )
    end

  end,

  ---------------------------------------------------------------------- DRAW
  Draw = function( self )
    if ( self["phase"] == "gameover" ) then
      self:Draw_Gameover()
    elseif ( self["phase"] == "win" ) then
      self:Draw_WinScreen()
    else
      self:Draw_Play()

      if ( self.player.isDefending ) then
        self:Draw_Defend()
      else
        self:Draw_Hud()
      end

      if ( self.player.enemyEncounter == true and self.player.isDefending == false ) then
        self.images["actionBalloonBig" ]:draw( 240/2-115/2, 180 )
        self.tables["buttonPrompts"]:getImage( math.floor( self.onetwo ), 1 ):draw( 240/2 - 50, 190-5 )
        gfx.drawTextAligned( "TO DEFEND!", 240/2 + 10, 195-2, kTextAlignment.center )
        self.tables["buttonPrompts"]:getImage( math.floor( self.onetwo ), 2 ):draw( 240/2 - 50, 190+18 )
        gfx.drawTextAligned( "TO ATTACK!", 240/2 + 10, 195+20, kTextAlignment.center )
      end
    end
  end, -- Draw = function( self )

  ---------------------------------------------------------------------- DRAW DEFEND
  Draw_Defend = function( self )
    self.images["clipboard-defend"]:draw( 0, 240-160 )
  end,

  ---------------------------------------------------------------------- DRAW HUD
  Draw_Hud = function( self )
    -- Background
    self.images["clipboard"]:draw( 240, 0 )

    -- Draw mail
    if ( self.player.questActive ) then
      self.images["mail"]:draw( 253, 154 )
      gfx.setFont( fonts.pedallica )
      gfx.drawTextAligned( "To:", 260, 180, kTextAlignment.left )
      gfx.drawTextAligned( self.characters[ self.gameData[ "endQuestCharacter" ] ].name, 320, 200, kTextAlignment.center )
    end

    -- Draw health bar (pencil)
    local notch = 1
    local totalNotches = self.player.health / 10
    gfx.setPattern( {0xaa, 0x55, 0xaa, 0x55, 0xaa, 0x55, 0xaa, 0x55})
    gfx.fillRect( 250, 48, 7, 69 )
    gfx.setColor( gfx.kColorBlack )
    self.images["pencil-top"]:draw( 250, 48 )
    for notch = 1, totalNotches do
      self.images["pencil-mid"]:draw( 250, 48 + 19 + ((notch-1)*5) )
    end

    self.images["pencil-bottom"]:draw( 250, 48 + 19 + ((totalNotches-1)*5) )

    gfx.setFont( fonts.nontendo )
    gfx.drawTextAligned( tostring(self.player.health), 254, 35, kTextAlignment.center )

    local w = 10
    local minimapOffsetX = 265
    local minimapOffsetY = 25

    gfx.setColor( gfx.kColorBlack )
    gfx.drawRect( minimapOffsetX-1, minimapOffsetY-1, w * (self.roomCountWidth + 1 ) + 2, w * (self.roomCountHeight+1) + 2 )

    -- MINIMAP
    for x = 0, self.roomCountWidth do
      for y = 0, self.roomCountHeight do

        gfx.setColor( gfx.kColorBlack )
        if ( self.mapArray[x][y]["tile"] == 1 ) then
          gfx.drawRect( x*w + minimapOffsetX, y*w + minimapOffsetY, w, w )

        else
          -- Non-rooms, fill in
          gfx.setPattern( {0xaa, 0x55, 0xaa, 0x55, 0xaa, 0x55, 0xaa, 0x55})
          gfx.fillRect( x*w + minimapOffsetX, y*w + minimapOffsetY, w, w )
          gfx.setColor( gfx.kColorBlack )
        end

        -- Draw player arrow on minimap
        if ( self.player.locationX == x and self.player.locationY == y ) then
          self.images["mapArrow"]:drawRotated( x*w + minimapOffsetX + 5, y*w + 5 + minimapOffsetY, self["view"]["angle"] )
        end

        for key, enemy in pairs( self.enemies ) do
          if ( enemy.x == x and enemy.y == y and ( self.player.locationX ~= x or self.player.locationY ~= y ) ) then
            self.images["mapGhost"]:draw( x*w + minimapOffsetX, y*w + minimapOffsetY )
          end
        end

        for key, chara in pairs( self.characters ) do
          -- Don't draw the NPC icon if the player is currently in this location
          if ( chara.x == x and chara.y == y and ( self.player.locationX ~= x or self.player.locationY ~= y ) ) then

            -- Draw begin quest marker
            if ( self.player.questActive == false and chara.beginQuest == true ) then
              self.images["questBegin"]:draw( x*w + minimapOffsetX, y*w + minimapOffsetY )

            -- Draw end quest marker
            elseif ( self.player.questActive == true and chara.endQuest == true ) then
              self.images["questEnd"]:draw( x*w + minimapOffsetX, y*w + minimapOffsetY )

            -- Draw character marker
            else
              self.images["mapCharacter"]:draw( x*w + minimapOffsetX, y*w + minimapOffsetY )
            end
          end
        end

      end
    end
  end,

  ---------------------------------------------------------------------- DRAW GAMEOVER
  Draw_Gameover = function( self )
    self.images["shadow"]:draw( 0, 0 )
    self.images["textbox"]:draw( 50, 30 )

    gfx.setFont( fonts.roobert )
    gfx.drawTextAligned( "GAME OVER", 400/2, 50, kTextAlignment.center )

    gfx.setFont( fonts.pedallica )
    gfx.drawTextAligned( "You end up having to take a mental", 400/2, 85, kTextAlignment.center )
    gfx.drawTextAligned( "health break, so you have to stay", 400/2, 105, kTextAlignment.center )
    gfx.drawTextAligned( "late at work to catch up. :'(", 400/2, 125, kTextAlignment.center )
  end,

  ---------------------------------------------------------------------- DRAW WIN
  Draw_WinScreen = function( self )
    self.images.win:draw( 0, 0 )
  end,

  ---------------------------------------------------------------------- DRAW PLAY
  Draw_Play = function( self )
    local offset = self["view"]["xOffset"]
    local bgWidth = self["backgroundWidth"]
    -- Damage shake
    offset += (self.player.hurtTimer % 3)

    -- FIRST PERSON VIEW
    -- Current room: at self.player.locationX, self.player.locationY
    -- Draw rooms in order: NORTH/EAST/SOUTH/WEST
    local roomWidth = 240
    local curX = self.player.locationX
    local curY = self.player.locationY

    local step = 0

    --DebugTable( "mapArray", "0", self.mapArray )
    --DebugLog( "mapArray", self.mapArray[curX][curY-1]["type"] )
    local northmapref = self.mapArray[curX][curY-1]["tile"]

    local tileX_N = self.mapArray[curX][curY-1]["tile"]
    local tileX_E = self.mapArray[curX+1][curY]["tile"]
    local tileX_S = self.mapArray[curX][curY+1]["tile"]
    local tileX_W = self.mapArray[curX-1][curY]["tile"]

    local roomType = self.mapArray[curX][curY]["type"]

    -- Blur is really slow :(
    if ( self.view["moveTimer"] > self.view["moveTimerMax"]/2 ) then
      -- Transition frames
      -- playdate.graphics.image:draw(x, y, [flip, [sourceRect]])
      self.tables["corridor"]:getImage( tileX_N+3, roomType ):draw( roomWidth*0 + offset, 0 ) -- North room
      self.tables["corridor"]:getImage( tileX_E+3, roomType ):draw( roomWidth*1 + offset, 0 ) -- East room
      self.tables["corridor"]:getImage( tileX_S+3, roomType ):draw( roomWidth*2 + offset, 0 ) -- South room
      self.tables["corridor"]:getImage( tileX_W+3, roomType ):draw( roomWidth*3 + offset, 0 ) -- West room

    else
      -- Plain frames
      self.tables["corridor"]:getImage( tileX_N+1, roomType ):draw( roomWidth*0 + offset, 0 ) -- North room
      self.tables["corridor"]:getImage( tileX_E+1, roomType ):draw( roomWidth*1 + offset, 0 ) -- East room
      self.tables["corridor"]:getImage( tileX_S+1, roomType ):draw( roomWidth*2 + offset, 0 ) -- South room
      self.tables["corridor"]:getImage( tileX_W+1, roomType ):draw( roomWidth*3 + offset, 0 ) -- West room

      if ( self["view"]["angle"] >= 210 ) then -- Draw north room again
        self.tables["corridor"]:getImage( tileX_N+1, roomType ):draw( roomWidth*4 + offset, 0 ) -- North room
        self.tables["corridor"]:getImage( tileX_E+1, roomType ):draw( roomWidth*5 + offset, 0 ) -- East room
      end
    end

    -- DRAW ENEMIES
    for key, enemy in pairs( self.enemies ) do
      if ( enemy.x == curX and enemy.y == curY ) then

        -- Ghost floating image
        local floating = enemy.timer
        if ( enemy.timer >= enemy.timerMax/2 ) then
          floating = enemy.timerMax - floating
        end

        self.images["speechBalloon"]:draw( 0, 0 )
        -- Health bar
        gfx.setColor( gfx.kColorBlack )
        gfx.fillRect( 60, 60, 10, enemy.healthMax )
        gfx.setColor( gfx.kColorWhite )
        gfx.fillRect( 60+1, 60+1, 10-2, enemy.health-2 )


        gfx.setFont( fonts.roobert10 )

        -- Timer until attack
        if ( enemy.timer < enemy.timerMax / 2 ) then
          gfx.drawTextAligned( "Hey, I have a request...", 117, 10, kTextAlignment.center )
        else
          gfx.drawTextAligned( enemy.attackText, 117, 10, kTextAlignment.center )
        end

        -- Always draw in front of player, no matter the angle
        if ( enemy.hurtTimer > 0 ) then
          self.tables["enemies"]:getImage( 1, enemy.type ):drawFaded( 240/2-133/2, 25 + floating/4, 0.5, gfx.image.kDitherTypeBayer2x2 )
        else
          self.tables["enemies"]:getImage( 1, enemy.type ):draw( 240/2-133/2, 25 + floating/4 )
        end

        if ( enemy.timer >= enemy.timerMax / 2 and self.player.isDefending ) then
          self.images["actionBalloon" ]:draw( 240/2-115/2, 25 )
          if ( self.player.hurtTimer > 0 ) then
            gfx.drawTextAligned( "Too late...", 240/2, 40, kTextAlignment.center )
          else
            gfx.drawTextAligned( "NO!!!", 240/2, 40, kTextAlignment.center )
          end
        end
      end
    end

    -- DRAW CHARACTERS
    for key, chara in pairs( self.characters ) do
      if ( chara.x == curX and chara.y == curY ) then
        local xImage = 1
        local text = ""
        local prompt = false
        local promptText = ""

        local adjustedX = roomWidth * chara.edge + self.view["xOffset"]

        -- Begin quest
        if ( self.player.questActive == false and chara.beginQuest == true ) then
          xImage = 1
          text = "Could you deliver this?" --.. self.characters[ charB ]["name"] .. " for me?"
          promptText = "Sure."
          prompt = true

          -- End quest
        elseif ( self.player.questActive == true and chara.endQuest == true ) then
          xImage = 2
          text = "You have something for me?"
          promptText = "Here."
          prompt = true

          -- Plain ole character
        else
          xImage = 2
          text = chara.saying
        end

        self.tables["characters"]:getImage( xImage, chara.type ):draw( adjustedX, 25 )

        -- Draw word balloon
        self.images["speechBalloon"]:draw( adjustedX, 0 )
        gfx.setFont( fonts.roobert10 )
        gfx.drawTextAligned( text, adjustedX + 117, 10, kTextAlignment.center )

        if ( prompt == true ) then
          self.images["responseBalloon" ]:draw( adjustedX + 10, 25 )
          self.tables["buttonPrompts"]:getImage( math.floor( self.onetwo ), 2 ):draw( adjustedX + 15, 20 )
          gfx.drawTextAligned( promptText, adjustedX + 60 , 29, kTextAlignment.center )
        end
      end
    end

    local fade = 1
    local dither = gfx.image.kDitherTypeBayer2x2
    local maxHalf = self.view["moveTimerMax"]/2
    local timer = 10-self.view["moveTimer"]
    if ( self.view["moveTimer"] > 0 ) then

      if ( timer < maxHalf ) then
        fade = timer / maxHalf
      else
        fade = (self.view["moveTimer"]) / maxHalf
      end

      self.images["fade"]:drawFaded( 0, 0, fade, dither )
    end

    if ( self.player.punchCooldown > 0 and math.floor( self.player.punchCooldown / 4 ) > 0 ) then
      local frame = math.floor( self.player.punchCooldown / 4 )
      -- 122x122, 240x240
      self.tables["fist"]:getImage( frame, 1 ):draw( 240/2 - 122/2, 240/2 - 122/2  )
    end

  end,

  ---------------------------------------------------------------------- HANDLE INPUT
  HandleInput = function( self )
    inputManager:Handle_Crank()

    if ( self["phase"] == "gameover" ) then return end

    local crankRatio = inputManager.crank.angleDegrees / 360

    self["view"]["angle"] = inputManager.crank.angleDegrees
    self["view"]["xOffset"] = -self["view"]["viewWidth"] * crankRatio

    if      ( inputManager.crank.angleDegrees >= 360-45 or inputManager.crank.angleDegrees < 45 ) then
      self.view["currentDirection"] = "north"
    elseif  ( inputManager.crank.angleDegrees < 180-45 ) then
      self.view["currentDirection"] = "east"
    elseif  ( inputManager.crank.angleDegrees < 270-45 ) then
      self.view["currentDirection"] = "south"
    else
      self.view["currentDirection"] = "west"
    end

    if ( self.view["moveTimer"] == 0 ) then
      local wantToMove = false
      local moveX = self.player.locationX
      local moveY = self.player.locationY

      local beginX = -100
      local beginY = -100
      if ( self.gameData[ "beginQuestCharacter" ] ~= nil ) then
        beginX = self.characters[ self.gameData[ "beginQuestCharacter" ] ].x
        beginY = self.characters[ self.gameData[ "beginQuestCharacter" ] ].y
      end

      local endX = -100
      local endY = -100
      if ( self.gameData[ "endQuestCharacter" ] ~= nil ) then
        endX   = self.characters[ self.gameData[ "endQuestCharacter" ] ].x
        endY   = self.characters[ self.gameData[ "endQuestCharacter" ] ].y
      end

      if ( inputManager:IsKeyPressed( "a" ) ) then
        if ( self.player.questActive == false ) then
          -- Quest has not yet been accepted
          if ( self.player.locationX == beginX and self.player.locationY == beginY ) then
            self.player.questActive = true
          end
        else
          -- Quest is active
          if ( self.player.locationX == endX and self.player.locationY == endY ) then
            self:FinishQuest()
          end
        end
      end


      local nearbyEnemy = 0
      for key, enemy in pairs( self.enemies ) do
        if ( enemy.x == self.player.locationX and enemy.y == self.player.locationY ) then
          nearbyEnemy = key
        end
      end

      if ( self.player.enemyEncounter ) then
        if ( inputManager:IsKeyPressed( "a" ) and self.player.punchCooldown <= 0 ) then --and self.enemies[nearbyEnemy].timer < self.enemies[nearbyEnemy].timerMax / 2 ) then
          self.player.isAttacking = true
          self.player.punchCooldown = self.player.punchCooldownMax
        else
          self.player.isAttacking = false
        end

        if ( inputManager:IsKeyPressed( "b" ) ) then
          --self.player.health -= 1
          self.player.isDefending = true
        else
          self.player.isDefending = false
        end
      end


      if ( inputManager:IsKeyPressed( "up" ) ) then
        wantToMove = true
        -- Move forward if possible

        -- Figure out direction we want to go
        if      ( self.view["currentDirection"] == "north" ) then    moveY -= 1
        elseif  ( self.view["currentDirection"] == "south" ) then    moveY += 1
        elseif  ( self.view["currentDirection"] == "west" )  then    moveX -= 1
        elseif  ( self.view["currentDirection"] == "east" )  then    moveX += 1
        end

      end

      -- Is this a valid room?
      if ( wantToMove and self.mapArray[ moveX ][ moveY ]["tile"] == 1 and self.player.enemyEncounter == false) then
        -- Valid location to move to
        self.player.moveToX = moveX
        self.player.moveToY = moveY
        self.view["moveTimer"] = self.view["moveTimerMax"]
      end
    end
  end,
}

import 'CoreLibs/graphics.lua'
import 'manager_gamedata.lua'
import 'manager_input.lua'
import 'utilities.lua'
import 'global.lua'
local gfx = playdate.graphics
local snd = playdate.sound

bass_gameScreen = {

  ---------------------------------------------------------------------- INIT
  Init = function( self )
    self.id = "bass_gameScreen"
    self.nextState = ""   -- Set this when you're ready to go to another state
    
    self.images = {
      ball = gfx.image.new( "packages/bass/images/ball.png" ),
      bass1 = gfx.image.new( "packages/bass/images/bass-horizontal.png" ),
      bass2 = gfx.image.new( "packages/bass/images/bass-vertical.png" ),
      waves = gfx.image.new( "packages/bass/images/waves.png" ),
      textbox = gfx.image.new( "images/textbox.png" ),
      win     = gfx.image.new( "packages/bass/images/win.png" ),
      shadow  = gfx.image.new( "images/transparent.png" ),
    }
    
    self.tables = {
      penguin = nil,
      stars = nil,
    }
    
    self.audio = {
      fail = snd.sampleplayer.new( "audio/sfx4.wav" ),
      success = snd.sampleplayer.new( "audio/sfx5.wav" ),
      levelComplete = snd.sampleplayer.new( "audio/yay.wav" ),
      throw = snd.sampleplayer.new( "audio/shoot.wav" ),
    }

    self.data = {
      level = 1,
      phase = "levelmark",
      instructions = { },
      aimAngle = 90,
      aimRadius = 20,
      originX = 34,
      originY = 68,
      gravity = -2,
      pengFrame = 1
    }
    
    self.ball = {
      startX = 19,
      startY = 56,
      x = 19,
      y = 56,
      width = 23,
      height = 24,
      traveling = false,
      travelTimer = 0,
      angle = 0,
      initialVelocity = -7
    }

    self.level = {
      throws = 0,
      totalBasses = 0,
      initialBasses = 0
    }

    self.basses = {}

    self.tracker = {}
    
    self.allLevelData = self:SetupLevelData()
    self.levelData = self.allLevelData[global.levelKey]

    self.tables.penguin, result = TryLoadImageTable( "packages/bass/images/penguin" )
    self.tables.stars, result = TryLoadImageTable( "images/stars" )

    self["data"]["level"] = global.levelKey
    self:SetupLevel()
    
    PlaySong( dataManager["data"][global.gameKey].song )
  end, -- Init = function( self )

  ---------------------------------------------------------------------- CLEANUP
  Cleanup = function( self )
    -- Free space here as needed, stop music
    self.images = nil
    self.tables = nil
    self.data = nil
    self.ball = nil
    self.level = nil
    self.basses = nil
    self.tracker = nil
    self.levelData = nil
    self.audio = nil
  end, -- Cleanup = function( self )

  ---------------------------------------------------------------------- SETUP LEVEL
  SetupLevel = function( self )
    local data = self["data"]
    data["phase"] = "levelmark"
    data["instructions"] = {}
    data["level"] = global.levelKey
    self["level"]["throws"] = 0
    
    --local levelInstructions = dataManager["data"][global.gameKey]["levels"][global.levelKey]["instructions"]
    local levelInstructions = self.levelData["instructions"]
    
    for key, instruction in pairs( levelInstructions ) do
      table.insert( data["instructions"], instruction )
    end
  end,

  ---------------------------------------------------------------------- START LEVEL
  StartLevel = function( self )
    self["data"]["phase"] = "play"
    
    self["basses"] = {}
    
    for key, item in pairs( self.levelData.placements ) do
      self:CreateBass( item.x, item.y, item.orientation, item.type )
    end
    
    self["level"]["throws"] = 0
    self["level"]["totalBasses"] = #self["basses"]
    self["level"]["initialBasses"] = #self["basses"]
  end,

  ---------------------------------------------------------------------- CREATE BASS
  CreateBass = function( self, x, y, orientation, behavior )
    local newBass = {}
    newBass["x"] = x
    newBass["y"] = y
    newBass["initialX"] = x
    newBass["initialY"] = y
    newBass["animationCounter"] = 0
    newBass["animationCounterMax"] = 50
    newBass["type"] = orientation
    newBass["angle"] = 0

    if ( newBass["type"] == 1 ) then
      newBass["mouthCenterOffsetX"] = 3
      newBass["mouthCenterOffsetY"] = 31

    elseif ( newBass["type"] == 2 ) then
      newBass["mouthCenterOffsetX"] = 30
      newBass["mouthCenterOffsetY"] = 3

    end

    newBass["behavior"] = behavior
    
    if ( newBass["behavior"] == "path4" ) then
      newBass["animationCounterMax"] = 200
    end
    
    newBass["state"] = "idle"
    table.insert( self["basses"], newBass )
  end,

  ---------------------------------------------------------------------- UPDATE
  Update = function( self )    
    self:HandleInput()

    if ( self["data"]["phase"] == "play" ) then

      self:UpdateBall()
      self:UpdateBasses()

      local allBassesOffScreen = true
      for key, bass in pairs( self["basses"] ) do
        if ( bass["state"] == "hit" ) then
          bass["y"] -= 5
        end

        if ( bass["y"] >= -200 ) then
          allBassesOffScreen = false
        end
      end

      if ( self:BassBallCollision() ) then
        self["level"]["totalBasses"] -= 1
        self:ResetBall()
        self["audio"]["success"]:setVolume( dataManager.data.sound_volume / 100.0 )
        self["audio"]["success"]:play()
      end

      if ( allBassesOffScreen ) then
        self:DoneWithLevel()
      end
    end

  end, -- Update = function( self )

  ---------------------------------------------------------------------- UPDATE BASSES
  UpdateBasses = function( self )
    for key, bass in pairs( self["basses"] ) do

      if ( bass["behavior"] == "updown" ) then
        if ( bass["animationCounter"] < bass["animationCounterMax"] / 2 ) then
          bass["y"] += 2
        else
          bass["y"] -= 2
        end

      elseif ( bass["behavior"] == "leftright" ) then
        if ( bass["animationCounter"] < bass["animationCounterMax"] / 2 ) then
          bass["x"] += 1
        else
          bass["x"] -= 1
        end

      elseif ( bass["behavior"] == "dipdown" ) then
        if ( bass["animationCounter"] < bass["animationCounterMax"] / 2 ) then
          bass["y"] += 8
        else
          bass["y"] -= 8
        end

      elseif ( bass["behavior"] == "path1" ) then
        if ( bass["animationCounter"] < bass["animationCounterMax"] * 0.25 ) then
          bass["y"] -= 7

        elseif ( bass["animationCounter"] < bass["animationCounterMax"] * 0.50 ) then
          bass["x"] += 7

        elseif ( bass["animationCounter"] < bass["animationCounterMax"] * 0.75 ) then
          bass["y"] += 7

        elseif ( bass["animationCounter"] < bass["animationCounterMax"] * 1.00 ) then
          bass["x"] -= 7
        end

      elseif ( bass["behavior"] == "path2" ) then
        if ( bass["animationCounter"] < bass["animationCounterMax"] * 0.50 ) then
          bass["y"] += 5
        elseif ( bass["animationCounter"] < bass["animationCounterMax"] * 1.00 ) then
          bass["y"] -= 5
        end
        
        bass["x"] += 1
        
        if ( bass["x"] > 400 ) then
          bass["x"] -= 500
        end

      elseif ( bass["behavior"] == "path3" ) then        
        bass["x"] += math.random( -5, 5 ) * 2
        
        if ( bass["x"] + 62 > 400 ) then
          bass["x"] = -62
        elseif ( bass["x"] < -62 ) then
          bass["x"] = 400
        end

      elseif ( bass["behavior"] == "path4" ) then        
      
        if ( bass["animationCounter"] < bass["animationCounterMax"] * 0.25 ) then
          bass["y"] += 5
        elseif ( bass["animationCounter"] < bass["animationCounterMax"] * 0.50 ) then
          bass["y"] -= 5
        else
          -- no move
        end        
      
        if ( bass["y"] > 300 ) then
          bass["x"] = math.random( 100, 350 )
        end
        
      end

      bass["animationCounter"] += 1
      if ( bass["animationCounter"] >= bass["animationCounterMax"] ) then
        bass["animationCounter"] = 0
      end
    end
  end,

  ---------------------------------------------------------------------- BASS BALL COLLISION
  BassBallCollision = function( self )
    local ball = self["ball"]
    ballCenter = {
      x = ball["x"] + ball["width"] / 2,
      y = ball["y"] + ball["height"] / 2
    }

    for key, bass in pairs( self["basses"] ) do
      if ( bass["state"] ~= "hit" ) then

        mouthCenter = {}
        mouthCenter["x"] = bass["x"] + bass["mouthCenterOffsetX"]
        mouthCenter["y"] = bass["y"] + bass["mouthCenterOffsetY"]

        local distance = GetDistance( ballCenter, mouthCenter )

        if ( distance <= 30 ) then
          bass["state"] = "hit"
          bass["behavior"] = "static"
          return true
        end

      end
    end

    -- No collisions detected
    return false
  end,

  ---------------------------------------------------------------------- BEGIN THROW
  BeginThrow = function( self )
    local crankAngle = math.rad( inputManager.crank.angleDegrees - 90 )
    -- Can't throw again if already throwing!
    if ( self["ball"]["traveling"] == true ) then return end
    local ball = self["ball"]
    ball["travelTimer"] = 0
    ball["traveling"] = true
    ball["angle"] = crankAngle
    self["level"]["throws"] += 1
    self["data"]["pengFrame"] = 2
    self["audio"]["throw"]:setVolume( dataManager.data.sound_volume / 100.0 )
    self["audio"]["throw"]:play()
  end,

  ---------------------------------------------------------------------- UPDATE BALL
  UpdateBall = function( self )    
    if ( self["ball"]["traveling"] == false ) then return end
    local ball = self["ball"]
    ball["travelTimer"] = ball["travelTimer"] + 0.1

    local oldX = ball["x"]
    local oldY = ball["y"]

    local cosine = math.cos( ball["angle"] )
    local sine = math.sin( ball["angle"] )

    local newX = oldX - ( cosine * ball["initialVelocity"] )
    local newY = oldY - ( sine * ball["initialVelocity"] )

    local gravity = self["data"]["gravity"] * ball["travelTimer"]

    ball["x"] = newX
    ball["y"] = newY - gravity

    table.insert( self["tracker"], { x = newX+11, y = newY+12 } )

    -- Off screen
    if ( newY > 220 ) then
      self:ResetBall()
      self["audio"]["fail"]:setVolume( dataManager.data.sound_volume / 100.0 )
      self["audio"]["fail"]:play()
    end
  end,

  ---------------------------------------------------------------------- RESET BALL
  ResetBall = function( self )
    local ball = self["ball"]
    self["tracker"] = {}
    ball["x"] = ball["startX"]
    ball["y"] = ball["startY"]
    ball["angle"] = 0
    ball["traveling"] = false
    self["data"]["pengFrame"] = 1
  end,

  ---------------------------------------------------------------------- DONE WITH LEVEL
  DoneWithLevel = function( self )
    self.data.phase = "levelend"
    
    self.stars = 0
    
    if ( self.level.throws <= self.level.initialBasses ) then
      self.stars = 3
    elseif ( self.level.throws <= self.level.initialBasses * 2 ) then
      self.stars = 2
    else
      self.stars = 1
    end
    
    -- 1 = locked, 2 = no stars, 3 = one star, 4 = two stars, 5 = three stars
    local score = self.stars + 2
    
    ---- Save score
    if ( dataManager["data"][global.gameKey]["levelStatuses"][global.levelKey].state < score ) then
      dataManager["data"][global.gameKey]["levelStatuses"][global.levelKey].state = score
    end
    
    -- Unlock next level
    if ( dataManager["data"][global.gameKey]["levelStatuses"][global.levelKey+1] ~= nil and dataManager["data"][global.gameKey]["levelStatuses"][global.levelKey+1].state == 1 ) then
      dataManager["data"][global.gameKey]["levelStatuses"][global.levelKey+1].state = 2
    end
    
    dataManager:SaveData()
    
    if ( self["audio"]["success"]:isPlaying() == false ) then
      self["audio"]["levelComplete"]:setVolume( dataManager.data.sound_volume / 100.0 )
      self["audio"]["levelComplete"]:play()
    end
  end,

  ---------------------------------------------------------------------- DRAW
  Draw = function( self )    
    gfx.setColor( gfx.kColorBlack )
    gfx.fillRect( 0, 0, 400, 240 )

    local data = self["data"]

    for key, bass in pairs( self["basses"] ) do
      self.images["bass" .. tostring( bass["type"] ) ]:draw( bass["x"], bass["y"] )
    end

    self.images["waves"]:draw( 0, 132 )
    self.tables["penguin"]:getImage( data["pengFrame"] ):draw( 2, 74 )
    self.images["ball"]:draw( self["ball"]["x"], self["ball"]["y"] )
      
    if ( self.data.phase == "levelmark" ) then
      self:Draw_LevelMark()
      
    elseif ( self.data.phase == "levelend" ) then
      self:Draw_LevelEnd()

    elseif ( self.data.phase == "play" ) then
      self:Draw_Game()
    end

  end, -- Draw = function( self )
  
  ---------------------------------------------------------------------- DRAW WIN
  Draw_Win = function( self )
    
  end,
  
  ---------------------------------------------------------------------- DRAW LEVEL MARK
  Draw_LevelMark = function( self )    
    self.images["shadow"]:draw( 0, 0 )
    self.images["textbox"]:draw( 50, 30 )
    gfx.setFont( fonts.roobert )
    gfx.drawTextAligned( "LEVEL " .. tostring( self["data"]["level"] ), 400/2, 50, kTextAlignment.center )

    -- Display info box
    gfx.setFont( fonts.pedallica )

    local y = 90
    local inc = 20

    for key, inst in pairs( self["data"]["instructions"] ) do
      gfx.drawTextAligned( inst, 400/2, y, kTextAlignment.center )
      y += inc
    end

    if ( self.data["level"] < 7 ) then
      gfx.drawTextAligned( "Press 'A' to begin!" , 400/2, 175, kTextAlignment.center )
    else
      gfx.drawTextAligned( "DON'T press 'A'." , 400/2, 175, kTextAlignment.center )
    end
  end,
  
  ---------------------------------------------------------------------- DRAW LEVEL END
  Draw_LevelEnd = function( self )
    if ( self.data.level == 9 ) then
      -- The last level
      self.images["win"]:draw( 0, 0 )
    else
      self.images["shadow"]:draw( 0, 0 )
      self.images["textbox"]:draw( 50, 30 )
      
      -- Display level summary
      gfx.setFont( fonts.pedallica )
      gfx.drawTextAligned( "SUCCESS!", 400/2, 50, kTextAlignment.center )
      gfx.drawTextAligned( "Press 'A' to continue!" , 400/2, 175, kTextAlignment.center )
      
      -- Draw stars
      local starWidth = 32
      self.tables.stars:getImage( 1 ):draw( 400/2 - (starWidth*2) - (starWidth/2), 100 )
      
      if ( self.stars >= 2 ) then
        self.tables.stars:getImage( 1 ):draw( 400/2 - (starWidth/2), 100 )
      else
        self.tables.stars:getImage( 2 ):draw( 400/2 - (starWidth/2), 100 )
      end
      
      if ( self.stars >= 3 ) then
        self.tables.stars:getImage( 1 ):draw( 400/2 + (starWidth*2) - (starWidth/2), 100 )
      else
        self.tables.stars:getImage( 2 ):draw( 400/2 + (starWidth*2) - (starWidth/2), 100 )
      end
    end
  end,
  
  ---------------------------------------------------------------------- DRAW GAME
  Draw_Game = function( self )    
    local crankAngle = math.rad( inputManager.crank.angleDegrees - 90 )
    local cosine = math.cos( crankAngle ) --self["data"]["crankAngleRadians"] )
    local sine = math.sin( crankAngle ) --self["data"]["crankAngleRadians"] )
    local radius = self["data"]["aimRadius"]
    local x1 = self["data"]["originX"]
    local y1 = self["data"]["originY"]
    x2 = x1 + (cosine * radius)
    y2 = y1 + (sine * radius)

    -- Draw aim line
    gfx.setColor( gfx.kColorXOR )
    gfx.drawLine( x1, y1, x2, y2 )

    -- Draw throw tracking pixels
    for key, track in pairs( self["tracker"] ) do
      gfx.setColor( gfx.kColorXOR )
      gfx.drawPixel( track["x"], track["y"] )
    end
    
    -- HUD
    gfx.setColor( gfx.kColorWhite )
    gfx.fillRect( 0, 220, 400, 20 )

    gfx.drawText( "Throws: " .. tostring( self["level"]["throws"] ), 5, 222 )
    local hitBasses = self["level"]["initialBasses"] - self["level"]["totalBasses"]
    gfx.drawText( "Hits: " .. tostring( hitBasses ) .. "/" .. tostring( self["level"]["initialBasses"] ), 100, 222 )
    local ratio = hitBasses / self["level"]["throws"] * 100
    if ( ratio > 100 ) then ratio = 100 end
    ratio = math.floor( ratio )
    gfx.drawText( "Accuracy: " .. ratio .. "%", 200, 222 )
  end,
  
  ---------------------------------------------------------------------- HANDLE INPUT
  HandleInput = function( self )
    inputManager:Handle_Crank()
    
    if ( self.data.phase == "levelmark" ) then
      if ( inputManager:IsKeyPressRelease( "a" ) ) then
        self:StartLevel()
      end
      
    elseif ( self.data.phase == "levelend" ) then
      if ( inputManager:IsKeyPressRelease( "a" ) ) then
        self.nextState = "levelSelectScreen"
      end

    elseif ( self.data.phase == "play" ) then
      if ( inputManager:IsKeyPressed( "up" ) ) then
        self:BeginThrow()
      end
      
    end
  end,
  
  ---------------------------------------------------------------------- SETUP LEVEL DATA
  SetupLevelData = function( self )
    levels = {
          {
            instructions = {
              "Use the crank to aim",
              "press UP to shoot!",
              "Aim for the bass' mouth!"
            },
            placements = {
              { type = "static", x = 304, y = 134, orientation = 2 }
            },
          },
          {
            instructions = {
              "Hit all the bass to complete the level!",
            },
            placements = {
              { type = "static", x = 304, y = 134, orientation = 2 },
              { type = "static", x = 204, y = 134, orientation = 2 },
              { type = "static", x = 104, y = 134, orientation = 2 },
            },
          },
          {
            instructions = {
              "Not all bass are so calm...",
            },
            placements = {
              { type = "leftright", x = 304, y = 134, orientation = 2 },
              { type = "updown", x = 204, y = 134, orientation = 2 },
              { type = "leftright", x = 104, y = 134, orientation = 2 },
            },
          },
          {
            instructions = {
              "Got to get 'em while",
              "they're above the water!"
            },
            placements = {
              { type = "dipdown", x = 150, y = 134, orientation = 2 },
              { type = "dipdown", x = 350, y = 134, orientation = 2 },
            },
          },
          {
            instructions = {
              "Think you're a pro?",
              "Well how 'bout self..."
            },
            placements = {
              { type = "path1", x = 200, y = 180, orientation = 2 },
            },
          },
          {
            instructions = {
              "I mean that was an",
              "\"easy\" hard level.",
              "Now try self..."
            },
            placements = {
              { type = "path2", x = 000, y = 130, orientation = 2 },
              { type = "path2", x = 100, y = 150, orientation = 2 },
              { type = "path2", x = 200, y = 130, orientation = 2 },
              { type = "path2", x = 300, y = 150, orientation = 2 },
              { type = "path2", x = 400, y = 130, orientation = 2 },
            },
          },
          {
            instructions = {
              "I'm going to run out of",
              "fish at this rate. :|",
              "Maybe you should just stop?"
            },
            placements = {
              { type = "path3", x = 100, y = 130, orientation = 2 },
              { type = "path3", x = 200, y = 130, orientation = 2 },
              { type = "path3", x = 300, y = 130, orientation = 2 },
            },
          },
          {
            instructions = {
              "I CAN DO THIS ALL DAY!!",
              "I won't tire of this!!"
            },
            placements = {
              { type = "updown",    x = 150, y = 150, orientation = 2 },
              { type = "leftright", x = 200, y = 130, orientation = 2 },
              { type = "dipdown",   x = 250, y = 150, orientation = 2 },
              { type = "path1",     x = 300, y = 180, orientation = 2 },
              { type = "path2",     x = 350, y = 130, orientation = 2 },
            },
          },
          {
            instructions = {
              "OK seriously I'm not",
              "having fun anymore and",
              "I'm running out of fish money."
            },
            placements = {
              { type = "path4", x = 300, y = 130, orientation = 2 },
              { type = "path4", x = 200, y = 130, orientation = 2 },
            },
          },
        }
      return levels
  end,
}

import 'CoreLibs/graphics.lua'
import 'manager_gamedata.lua'
import 'manager_input.lua'
local gfx = playdate.graphics
local snd = playdate.sound

crank_gameScreen = {
  ---------------------------------------------------------------------- INIT
  Init = function( self )
    self.nextState = ""   -- Set this when you're ready to go to another state

    self.allLevelData = self:SetupLevelData()
    self.level = self.allLevelData[global.levelKey]

    self.images = {
      heart = gfx.image.new( "images/heart.png" ),
      key = gfx.image.new( "packages/crank/images/key.png" ),
      coffee = gfx.image.new( "packages/crank/images/coffeecup.png" ),
      textbox = gfx.image.new( "images/textbox.png" ),
      shadow  = gfx.image.new( "images/transparent.png" ),
      win  = gfx.image.new( "packages/crank/images/win.png" ),
      background = gfx.image.new( self.level.background ),
    }

    self.tables = {
      stars = nil,
    }

    self.audio = {
      collect = snd.sampleplayer.new( "audio/sfx2.wav" ),
      levelComplete = snd.sampleplayer.new( "audio/yay.wav" ),
      placeCoffee = snd.sampleplayer.new( "audio/sfx6.wav" ),
      grabCoffee = snd.sampleplayer.new( "audio/sfx1.wav" ),
      invalid = snd.sampleplayer.new( "audio/sfx4.wav" ),
      reset = snd.sampleplayer.new( "audio/sfx5.wav" ),
    }

    self.game = {
      animationCounter = 1,
      animationCounterMax = 2,
      frame = 1,
      player = {
        x = 0, y = 0, cups = 4,
        width = 20, height = 20,
        speed = 2,
        direction = 1, -- down = 1, up = 2, left = 3, right = 4
        walking = false,
        walkingTo = { x = 0, y = 0 },
        dropCooldown = 0,
        turnCooldown = 0,
        turnCooldownMax = 5,
        waitForRelease = false,
        coins = 0
      },
      enemies = {},
      key = {},
      door = { frame = 1 -- locked
      },
      coffee = {},
      coins = {},
      state = "level_intro",
      deleteList = { coins = {} },
      totalCoins = 0,
      stars = 0,
      crankCounter = 0,
      crankCounterMax = 90,
      resetLevelCounter = 0,
      resetLevelCounterMax = 100
    }


    self.game.instructions = {}

    for key, instruction in pairs( self.level.instructions ) do
      table.insert( self.game.instructions, instruction )
    end

    table.insert( self.game.instructions, "" )
    table.insert( self.game.instructions, "Press A to start" )

    self.tables.stars, result = TryLoadImageTable( "images/stars" )

    self.tables.tileset, result = TryLoadImageTable( "packages/crank/images/tileset" )
    self.tables.employeeA, result = TryLoadImageTable( "packages/crank/images/employeeA" )
    self.tables.employeeB, result = TryLoadImageTable( "packages/crank/images/employeeB" )
    self.tables.employeeC, result = TryLoadImageTable( "packages/crank/images/employeeC" )
    self.tables.employeeD, result = TryLoadImageTable( "packages/crank/images/employeeD" )
    self.tables.employeeE, result = TryLoadImageTable( "packages/crank/images/employeeE" )
    self.tables.player, result = TryLoadImageTable( "packages/crank/images/player" )
    if ( global.levelKey == 9 ) then
      self.tables.door, result = TryLoadImageTable( "packages/crank/images/dooralt" )
    else
      self.tables.door, result = TryLoadImageTable( "packages/crank/images/door" )
    end
    self.tables.coin, result = TryLoadImageTable( "packages/crank/images/coin" )
    self.tables.loadCursor, result = TryLoadImageTable( "images/loadcursor" )

    self:Init_Level()

    PlaySong( dataManager["data"][global.gameKey].song )
  end, -- Init = function( self )

  ---------------------------------------------------------------------- CLEANUP
  Cleanup = function( self )
    -- Free space here as needed, stop music
    self.images = nil
    self.tables = nil
    self.game = nil
    self.audio = nil
    self.gameData = nil
  end, -- Cleanup = function( self )

  ---------------------------------------------------------------------- INIT LEVEL
  Init_Level = function( self )
    self.game.player.x = self.level.start.x * 20
    self.game.player.y = self.level.start.y * 20

    -- Place key
    self.game.key.x = self.level.key.x * 20
    self.game.key.y = self.level.key.y * 20
    self.game.key.collected = false

    -- Place door
    self.game.door.x = self.level.door.x * 20
    self.game.door.y = self.level.door.y * 20
    self.game.door.frame = 1 -- not opened

    -- Place enemies
    for key, enemy in pairs( self.level.enemies ) do
      local newEnemy = {
          x = enemy.x * 20,
          y = enemy.y * 20,
          width = 20,
          height = 20,
          type = enemy.type,
          direction = enemy.direction,
          goal = nil,
          walking = false,
          walkingTo = { x = 0, y = 0 },
          speed = 1,
          turnCountdown = 50,
          turnCountdownMax = 50,
        }

        if ( newEnemy.type == "employeeC" ) then
          newEnemy.walkCooldown = 0
          newEnemy.walkCooldownMax = enemy.walkCooldownMax
        elseif ( newEnemy.type == "employeeD" ) then
          newEnemy.walkCooldown = 0
          newEnemy.walkCooldownMax = enemy.walkCooldownMax
        end

      table.insert( self.game.enemies, newEnemy )
    end

    -- Place coins
    for key, coin in pairs( self.level.coins ) do
      local newCoin = {
        x = coin.x * 20,
        y = coin.y * 20
      }
      table.insert( self.game.coins, newCoin )
      self.game.totalCoins += 1
    end
  end, -- Init_Level = function( self )

  ---------------------------------------------------------------------- UPDATE
  Update = function( self )
    inputManager:Handle_Crank()
    self:HandleInput()

    self.game.animationCounter += 0.1
    if ( self.game.animationCounter > self.game.animationCounterMax+1 ) then
      self.game.animationCounter = 1
    end
    self.game.frame = math.floor( self.game.animationCounter )

    if ( self.game.state == "play" ) then
      self:Update_Player()
      self:Update_Enemies()
      self:Update_Objects()

    elseif ( self.game.state == "resetting" ) then
      self:Update_Resetting()

    elseif ( self.game.state == "level_end" ) then
      if ( inputManager:IsKeyPressRelease( "a" ) ) then
        if ( global.levelKey == 9 ) then
          self.game.state = "game_end"
        else
          self.nextState = "levelSelectScreen"
        end
      end

    elseif ( self.game.state == "game_end" ) then
      if ( inputManager:IsKeyPressRelease( "a" ) ) then
        self.nextState = "levelSelectScreen"
      end

    end
  end, -- Update = function( self )

  ---------------------------------------------------------------------- UPDATE RESETTING
  Update_Resetting = function( self )
    self.game.resetLevelCounter += 1
    if ( self.game.resetLevelCounter >= self.game.resetLevelCounterMax ) then
      self:Init()
    end
  end,

  ---------------------------------------------------------------------- UPDATE OBJECTS
  Update_Objects = function( self )
    for k, key in pairs( self.game.deleteList.coins ) do
      self.game.coins[key] = nil
    end
  end,

  ---------------------------------------------------------------------- LOOKING AT
  DoYouSeeCoffee = function( self, sight )
      -- UGH I NEVER REMEMBER LUA FOR LOOPS >_>     for var=exp1,exp2,exp3 do
      -- Technically this isn't O(n^3) because either the x or y loop only goes once :P
      -- but it's still O(n^2) so I might need to figure out something better.
      for sightY = sight.minY, sight.maxY, 20 do
        for sightX = sight.minX, sight.maxX, 20 do
          for key, coffee in pairs( self.game.coffee ) do
            if ( coffee.x == sightX and coffee.y == sightY ) then
              -- Guy can see this coffee
              return true
            end
          end -- for key, coffee in pairs( self.game.coffee ) do
        end -- for sightX = minX, maxX, 20 do
      end -- for sightY = minY, maxY, 20 do
      return false
  end,

  ---------------------------------------------------------------------- UPDATE ENEMIES
  Update_Enemies = function( self )
    for key, enemy in pairs( self.game.enemies ) do

      -- Try to grab coffee
      self:Update_TryGrabCoffee( enemy )

      -- Are they touching a coin?
      self:Update_TryGrabCoin( enemy )


      if ( enemy.type == "employeeA" ) then
        -- COFFEE MOTIVATED EMPLOYEE
        -- Enemy try to find goal
        self:Update_Enemy_FindGoal( enemy )

        -- Walk toward goal
        self:Update_MoveTowardGoal( enemy, enemy.walkingTo )

        if ( enemy.walking == false )  then-- Turns around when idle

          -- Turn around every-so-often
          if ( enemy.turnCountdown <= 0 ) then
            -- Rotate
            -- Blah I should have put the sprites in a different order.
            if ( enemy.direction == 1 ) then enemy.direction = 3
            elseif ( enemy.direction == 2 ) then enemy.direction = 4
            elseif ( enemy.direction == 3 ) then enemy.direction = 2
            elseif ( enemy.direction == 4 ) then enemy.direction = 1
            end
            enemy.turnCountdown = enemy.turnCountdownMax
          else
            enemy.turnCountdown -= 1
          end
        end -- if ( enemy.walking == false )  then

      elseif ( enemy.type == "employeeC" ) then
        -- Patrol back and forth

        if ( enemy.walking ) then
          -- Walk toward goal
          self:Update_MoveTowardGoal( enemy, enemy.walkingTo )
        elseif ( enemy.walkCooldown > 0 ) then
          enemy.walkCooldown -= 1
        else
          -- Try to move forward. If not possible, then flip around.
          local moveX = enemy.x
          local moveY = enemy.y

          if     ( enemy.direction == 1 ) then      -- down
            moveY += 20
          elseif ( enemy.direction == 2 ) then      -- up
            moveY -= 20
          elseif ( enemy.direction == 3 ) then      -- left
            moveX -= 20
          elseif ( enemy.direction == 4 ) then  -- right
            moveX += 20
          end

          local rect = { x = moveX, y = moveY, width = 20, height = 20 }

          if ( self:CanWalkThere( rect ) ) then
            -- OK, set that as where we're walking to.
            enemy.walking = true
            enemy.walkingTo.x = moveX
            enemy.walkingTo.y = moveY
            enemy.walkCooldown = enemy.walkCooldownMax
          else
            -- Can't walk that way; turn around.
            if      ( enemy.direction == 1 ) then enemy.direction = 2
            elseif  ( enemy.direction == 2 ) then enemy.direction = 1
            elseif  ( enemy.direction == 3 ) then enemy.direction = 4
            elseif  ( enemy.direction == 4 ) then enemy.direction = 3
            end
          end -- if ( self:CanWalkThere( x, y ) ) then
        end -- if enemy.walking

      elseif ( enemy.type == "employeeD" ) then

        if ( enemy.walking ) then
          -- Walk toward goal
          self:Update_MoveTowardGoal( enemy, enemy.walkingTo )
        elseif ( enemy.walkCooldown > 0 ) then
          enemy.walkCooldown -= 1
        else
          -- Try to move forward. If not possible, then turn
          local moveX = enemy.x
          local moveY = enemy.y

          if     ( enemy.direction == 1 ) then      -- down
            moveY += 20
          elseif ( enemy.direction == 2 ) then      -- up
            moveY -= 20
          elseif ( enemy.direction == 3 ) then      -- left
            moveX -= 20
          elseif ( enemy.direction == 4 ) then  -- right
            moveX += 20
          end

          local rect = { x = moveX, y = moveY, width = 20, height = 20 }

          if ( self:CanWalkThere( rect ) ) then
            -- OK, set that as where we're walking to.
            enemy.walking = true
            enemy.walkingTo.x = moveX
            enemy.walkingTo.y = moveY
            enemy.walkCooldown = enemy.walkCooldownMax
          else
            -- Can't walk that way; turn around.
            if      ( enemy.direction == 1 ) then enemy.direction = 3 -- Down to left
            elseif  ( enemy.direction == 2 ) then enemy.direction = 4 -- Up to right
            elseif  ( enemy.direction == 3 ) then enemy.direction = 2 -- Left to up
            elseif  ( enemy.direction == 4 ) then enemy.direction = 1 -- Right to down
            end
          end -- if ( self:CanWalkThere( x, y ) ) then
        end -- if enemy.walking

      end -- if ( enemy.type == "employeeA" ) then
    end -- for key, enemy in pairs( self.game.enemies ) do
  end,

  ---------------------------------------------------------------------- UPDATE PLAYER
  Update_Player = function( self )
    if ( self.game.player.dropCooldown > 0 ) then
      self.game.player.dropCooldown -= 1
    end
    if ( self.game.player.turnCooldown > 0 ) then
      self.game.player.turnCooldown -= 1
    end

    -- Walk toward goal
    self:Update_MoveTowardGoal( self.game.player, self.game.player.walkingTo )

    -- Is the player touching the key?
    if ( self.game.player.x == self.game.key.x and self.game.player.y == self.game.key.y ) then
      self:KeyCollect()

    -- Has the player collected the key AND now touching the door?
    elseif ( self.game.key.collected and self.game.player.x == self.game.door.x and self.game.player.y == self.game.door.y ) then
      self:ExitLevel()
    end

    -- Are they touching a coin?
    if ( self:Update_TryGrabCoin( self.game.player ) ) then
      self.game.player.coins += 1
    end
  end, -- Update_Player = function( self )

  ---------------------------------------------------------------------- UPDATE MOVE TOWARD GOAL
  Update_MoveTowardGoal = function( self, object, goal )
    if ( object.walking == true ) then
      if ( goal.x < object.x ) then
        object.x -= object.speed

      elseif ( goal.x > object.x ) then
        object.x += object.speed

      elseif ( goal.y < object.y ) then
        object.y -= object.speed

      elseif ( goal.y > object.y ) then
        object.y += object.speed

      else
        -- Reached destination
        object.walking = false
      end
    end
  end,

  ---------------------------------------------------------------------- UPDATE ENEMY TRY TO FIND GOAL
  Update_Enemy_FindGoal = function( self, enemy )
    -- Do you see any coffee?
    -- Get view range
    local sight = {
      minX = enemy.x,
      maxX = enemy.x,
      minY = enemy.y,
      maxY = enemy.y
    }

    if ( enemy.direction == 1 ) then -- Down
      sight.maxY = 12*20
    elseif ( enemy.direction == 2 ) then -- Up
      sight.minY = 0
    elseif ( enemy.direction == 3 ) then -- Left
      sight.minX = 0
    elseif ( enemy.direction == 4 ) then -- Right
      sight.maxX = 20*20
    end

    local seesCoffeeInThisDirection = self:DoYouSeeCoffee( sight )

    -- See a coffee; try to move in that direction (one tile at a time).
    if ( seesCoffeeInThisDirection ) then

      local moveX = enemy.x
      local moveY = enemy.y

      if ( enemy.direction == 2 ) then
        moveY -= 20

      elseif ( enemy.direction == 1 ) then
        moveY += 20

      elseif ( enemy.direction == 3 ) then
        moveX -= 20

      elseif ( enemy.direction == 4 ) then
        moveX += 20

      end

      -- Stay on grid
      moveX = math.floor( moveX / 20 ) * 20
      moveY = math.floor( moveY / 20 ) * 20
      local rect = { x = moveX, y = moveY, width = 20, height = 20 }

      -- See if that is a valid direction
      if ( self:CanWalkThere( rect ) ) then
        -- OK, set that as where we're walking to.
        enemy.walking = true
        enemy.walkingTo.x = moveX
        enemy.walkingTo.y = moveY
      end -- if ( self:CanWalkThere( x, y ) ) then
    end
  end,

  ---------------------------------------------------------------------- UPDATE TRY GRAB COFFEE
  Update_TryGrabCoffee = function( self, object )
    local faceX = object.x
    local faceY = object.y

    if ( object.direction == 1 ) then -- Down
      faceY += 20
    elseif ( object.direction == 2 ) then -- Up
      faceY -= 20
    elseif ( object.direction == 3 ) then -- Left
      faceX -= 20
    elseif ( object.direction == 4 ) then -- Right
      faceX += 20
    end

    -- Is there a coffee in front of object?
    local deleteKey = nil

    for key, coffee in pairs( self.game.coffee ) do
      if ( coffee.x == faceX and coffee.y == faceY ) then
        deleteKey = key
        break
      end
    end

    if ( deleteKey ~= nil ) then
      self.game.coffee[deleteKey] = nil

      -- Play level complete sound
      if ( self.audio.grabCoffee:isPlaying() == false ) then
        self.audio.grabCoffee:setVolume( dataManager.data.sound_volume / 100.0 )
        self.audio.grabCoffee:play()
      end

      return true
    end

    return false
  end,

  ---------------------------------------------------------------------- UPDATE TRY GRAB COIN
  Update_TryGrabCoin = function( self, object )
    for key, coin in pairs( self.game.coins ) do
      if ( object.x == coin.x and object.y == coin.y ) then
        self:CoinCollect( key )
        return true
      end
    end
    return false
  end,

  ---------------------------------------------------------------------- KEY COLLECT
  KeyCollect = function( self )

    self.game.key.x = -100
    self.game.key.collected = true
    self.game.door.frame = 2 -- open the door

    -- Play happy sound
    if ( self.audio.collect:isPlaying() == false ) then
      self.audio.collect:setVolume( dataManager.data.sound_volume / 100.0 )
      self.audio.collect:play()
    end
  end,

  ---------------------------------------------------------------------- COIN COLLECT
  CoinCollect = function( self, key )
    self.game.coins[ key ].x = -100
    table.insert( self.game.deleteList.coins, key )

    -- Play happy sound
    if ( self.audio.collect:isPlaying() == false ) then
      self.audio.collect:setVolume( dataManager.data.sound_volume / 100.0 )
      self.audio.collect:play()
    end
  end,

  ---------------------------------------------------------------------- EXIT LEVEL
  ExitLevel = function( self )
    self.game.state = "level_end"

    self.game.stars = 0

    if      ( self.game.player.coins == self.game.totalCoins ) then
      self.game.stars = 3
    elseif  ( self.game.player.coins > 0 ) then
      self.game.stars = 2
    else
      self.game.stars = 1
    end

    local score = self.game.stars + 2

   ---- Save score
    if ( dataManager["data"][global.gameKey]["levelStatuses"][global.levelKey].state < score ) then
      dataManager["data"][global.gameKey]["levelStatuses"][global.levelKey].state = score
    end

    -- Unlock next level
    if ( dataManager["data"][global.gameKey]["levelStatuses"][global.levelKey+1] ~= nil and dataManager["data"][global.gameKey]["levelStatuses"][global.levelKey+1].state == 1 ) then
      dataManager["data"][global.gameKey]["levelStatuses"][global.levelKey+1].state = 2
    end

    dataManager:SaveData()

    -- Play level complete sound
    if ( self.audio.levelComplete:isPlaying() == false ) then
      self.audio.levelComplete:setVolume( dataManager.data.sound_volume / 100.0 )
      self.audio.levelComplete:play()
    end
  end,

  ---------------------------------------------------------------------- PLAYER DEAD
  PlayerDead = function( self )
  end,

  ---------------------------------------------------------------------- DRAW
  Draw = function( self )
    if ( self.game.state == "play" ) then
      self:Draw_Map()
      self:Draw_Objects()
      self:Draw_Player()
      self:Draw_Hud()

    elseif ( self.game.state == "level_end" ) then
      self:Draw_LevelEnd()

    elseif ( self.game.state == "resetting" ) then
      self:Draw_Resetting()

    elseif ( self.game.state == "level_intro" ) then
      self:Draw_LevelIntro()

    elseif ( self.game.state == "game_end" ) then
      self:Draw_GameEnd()

    end

    if ( self.game.crankCounter > 0 ) then
      self:Draw_CrankToReset()
    end
  end, -- Draw = function( self )

  ---------------------------------------------------------------------- DRAW HUD
  Draw_Hud = function( self )
    -- HUD
    gfx.setFont( fonts.rains )
    gfx.setColor( gfx.kColorWhite )
    gfx.fillRect( 0, 0, 400, 10 )
    gfx.setColor( gfx.kColorXOR )
    gfx.drawText( "Score " .. tostring( self.game.player.coins ) .. "/" .. tostring( self.game.totalCoins ), 1, 1 )
    playdate.drawFPS( 380, 1 )
  end,

  ---------------------------------------------------------------------- DRAW GAME END
  Draw_GameEnd = function( self )
    self.images.win:draw( 0, 0 )
  end,

  ---------------------------------------------------------------------- DRAW LEVEL INTRO
  Draw_LevelIntro = function( self )
    self:Draw_Map()

    self.images.textbox:draw( 50, 30 )
    gfx.setFont( fonts.pedallica )
    gfx.drawTextAligned( "LEVEL " .. tostring( global.levelKey ), 400/2, 50, kTextAlignment.center )

    local y = 90
    local inc = 20

    for key, inst in pairs( self.game.instructions ) do
      gfx.drawTextAligned( inst, 400/2, y, kTextAlignment.center )
      y += inc
    end
  end,

  ---------------------------------------------------------------------- RESETTING
  Draw_Resetting = function( self )
    local wh = self.game.resetLevelCounter * 4

    gfx.setColor( gfx.kColorWhite )
    gfx.drawRect( 400/2 - wh/2, 240/2 - wh/2, wh, wh )
  end,

  ---------------------------------------------------------------------- DRAW CRANK TO RESET
  Draw_CrankToReset = function( self )
    local rectWidth = 400 / 2
    local rectHeight = 240 / 4
    local centerX = 400 / 2 - rectWidth / 2
    local centerY = 240 / 2 - rectHeight / 2
    gfx.setColor( gfx.kColorWhite )
    gfx.fillRect( centerX, centerY, rectWidth, rectHeight )
    gfx.setColor( gfx.kColorBlack )
    gfx.drawRect( centerX, centerY, rectWidth, rectHeight )
    gfx.setFont( fonts.pedallica )
    gfx.drawTextAligned( "Crank to reset level", 400/2, centerY+2, kTextAlignment.center )

    local frame = math.floor( self.game.crankCounter / 10 ) + 1 -- Ugh Lua beginning at 1
    self.tables.loadCursor:getImage( frame ):draw( 400/2 - 30/2, centerY+20 )
  end,

  ---------------------------------------------------------------------- DRAW LEVEL END
  Draw_LevelEnd = function( self )
    self.images.shadow:draw( 0, 0 )
    self.images.textbox:draw( 50, 30 )

    -- Display level summary
    gfx.setFont( fonts.pedallica )
    gfx.drawTextAligned( "SUCCESS!", 400/2, 50, kTextAlignment.center )
    gfx.drawTextAligned( "Press 'A' to continue!" , 400/2, 175, kTextAlignment.center )

    -- Draw stars
    local starWidth = 32
    self.tables.stars:getImage( 1 ):draw( 400/2 - (starWidth*2) - (starWidth/2), 100 )

    if ( self.game.stars >= 2 ) then
      self.tables.stars:getImage( 1 ):draw( 400/2 - (starWidth/2), 100 )
    else
      self.tables.stars:getImage( 2 ):draw( 400/2 - (starWidth/2), 100 )
    end

    if ( self.game.stars >= 3 ) then
      self.tables.stars:getImage( 1 ):draw( 400/2 + (starWidth*2) - (starWidth/2), 100 )
    else
      self.tables.stars:getImage( 2 ):draw( 400/2 + (starWidth*2) - (starWidth/2), 100 )
    end
  end,

  ---------------------------------------------------------------------- DRAW MAP
  Draw_Map = function( self )
    self.images.background:draw( 0, 0 )
    --x = 0
    --y = 0
    --wh = 20
    --for key, tile in pairs( self.level.map ) do
      --self.tables.tileset:getImage( tile ):draw( x, y )

      ----gfx.setColor( gfx.kColorXOR ) -- Debug
      ----gfx.drawText( tostring( tile ), x, y )

      --x += wh
      --if ( x >= 400 ) then
        --y += wh
        --x = 0
      --end
    --end
  end, -- Draw_Map = function( self )

  ---------------------------------------------------------------------- DRAW OBJECTS
  Draw_Objects = function( self )
    for key, coin in pairs( self.game.coins ) do
      self.tables.coin:getImage( self.game.frame ):draw( coin.x, coin.y )
    end

    -- Draw characters
    for key, enemy in pairs( self.game.enemies ) do
      local index = self:GetSpriteIndex( enemy.direction, self.game.frame )
      self.tables[enemy.type]:getImage( index ):draw( enemy.x, enemy.y )
    end

    for key, cup in pairs( self.game.coffee ) do
      self.images.coffee:draw( cup.x, cup.y )
    end

    self.tables.door:getImage( self.game.door.frame ):draw( self.game.door.x, self.game.door.y )
    self.images.key:draw( self.game.key.x, self.game.key.y )
  end, -- Draw_Objects = function( self )

  ---------------------------------------------------------------------- DRAW PLAYER
  Draw_Player = function( self )
    if ( self.game.player.cups > 0 ) then self.images.heart:draw( self.game.player.x+00, self.game.player.y-4 ) end
    if ( self.game.player.cups > 1 ) then self.images.heart:draw( self.game.player.x+05, self.game.player.y-5 ) end
    if ( self.game.player.cups > 2 ) then self.images.heart:draw( self.game.player.x+10, self.game.player.y-5 ) end
    if ( self.game.player.cups > 3 ) then self.images.heart:draw( self.game.player.x+15, self.game.player.y-4 ) end

    local index = self:GetSpriteIndex( self.game.player.direction, self.game.frame )
    self.tables.player:getImage( index ):draw( self.game.player.x, self.game.player.y )

    -- Draw square where player wants to go
    --gfx.drawRect( self.game.player.walkingTo.x, self.game.player.walkingTo.y, 20, 20 ) -- Debug
  end, -- Draw_Player = function( self )

  ---------------------------------------------------------------------- GET SPRITE INDEX
  GetSpriteIndex = function( self, dir, frame )
    return 2 * dir + frame - 2
  end,

  ---------------------------------------------------------------------- HANDLE INPUT
  HandleInput = function( self )
    if ( self.game.state == "play" ) then
      self:HandleInput_Play()

    elseif ( self.game.state == "level_intro" and inputManager:IsKeyPressed( "a" ) ) then
      self.game.state = "play"

    end
  end, -- HandleInput = function( self )

  ---------------------------------------------------------------------- HANDLE INPUT
  HandleInput_Play = function( self )
    local moveX = self.game.player.x
    local moveY = self.game.player.y
    local faceX = self.game.player.x
    local faceY = self.game.player.y
    local wantToWalk = false

    if ( self.game.player.walking == false ) then

      -- WANT TO DROP COFFEE?
      if ( self.game.player.dropCooldown <= 0 and inputManager:IsKeyPressed( "b" ) ) then
        -- Figure out what is ahead of player
      if ( self.game.player.direction == 1 ) then -- Down
        faceY += 20
      elseif ( self.game.player.direction == 2 ) then -- Up
        faceY -= 20
      elseif ( self.game.player.direction == 3 ) then -- Left
        faceX -= 20
      elseif ( self.game.player.direction == 4 ) then -- Right
        faceX += 20
      end

        local rect = { x = faceX, y = faceY, width = 20, height = 20 }
        -- Check to see if that area is empty
        if ( self:CanWalkThere( rect ) ) then -- Can walk forward
          self:PlaceCoffee( faceX, faceY )
        end
      end

      -- WANT TO TAKE COFFEE?
      if ( inputManager:IsKeyPressed( "a" ) ) then
        if ( self:Update_TryGrabCoffee( self.game.player ) ) then
          self.game.player.cups += 1
        end
      end

      -- WANT TO MOVE?

      -- Update where the player *wants* to go

      -- If the player isn't already facing the direction they want to go,
      -- then rotate them in that direction first.
      -- Otherwise, have them begin moving that way.

      -- Not facing the direction... turn to face
      if ( inputManager:IsKeyPressed( "up" )    and self.game.player.direction ~= 2 ) then
        self.game.player.direction = 2
        self.game.player.turnCooldown = self.game.player.turnCooldownMax

      elseif ( inputManager:IsKeyPressed( "down" )  and self.game.player.direction ~= 1 ) then
        self.game.player.direction = 1
        self.game.player.turnCooldown = self.game.player.turnCooldownMax

      elseif ( inputManager:IsKeyPressed( "left" )  and self.game.player.direction ~= 3 ) then
        self.game.player.direction = 3
        self.game.player.turnCooldown = self.game.player.turnCooldownMax

      elseif ( inputManager:IsKeyPressed( "right" ) and self.game.player.direction ~= 4 ) then
        self.game.player.direction = 4
        self.game.player.turnCooldown = self.game.player.turnCooldownMax

      -- Already facing the direction... go ahead and move
      elseif ( inputManager:IsKeyPressed( "up" )    and self.game.player.direction == 2 and self.game.player.turnCooldown <= 0 ) then
        moveY -= 20
        wantToWalk = true

      elseif ( inputManager:IsKeyPressed( "down" )  and self.game.player.direction == 1 and self.game.player.turnCooldown <= 0 ) then
        moveY += 20
        wantToWalk = true

      elseif ( inputManager:IsKeyPressed( "left" )  and self.game.player.direction == 3 and self.game.player.turnCooldown <= 0 ) then
        moveX -= 20
        wantToWalk = true

      elseif ( inputManager:IsKeyPressed( "right" ) and self.game.player.direction == 4 and self.game.player.turnCooldown <= 0 ) then
        moveX += 20
        wantToWalk = true

      end


      -- Stay on grid
      moveX = math.floor( moveX / 20 ) * 20
      moveY = math.floor( moveY / 20 ) * 20
      local rect = { x = moveX, y = moveY, width = 20, height = 20 }

      -- See if that is a valid direction
      if ( wantToWalk and self:CanWalkThere( rect ) ) then
        -- OK, set that as where we're walking to.
        self.game.player.walking = true
        self.game.player.walkingTo.x = moveX
        self.game.player.walkingTo.y = moveY
      end -- if ( wantToWalk and self:CanWalkThere( x, y ) ) then
    end -- if ( self.game.player.walking == false ) then


    -- Are we crankin'?
    inputManager:Handle_Crank()
    if ( inputManager.crank.angleDegrees > 0 and inputManager.crank.changeDegrees > 0 ) then
      self.game.crankCounter += 1
    else
      self.game.crankCounter = 0
    end

    if ( self.game.crankCounter >= self.game.crankCounterMax-1 ) then

      if ( self.audio.reset:isPlaying() == false ) then
        self.audio.reset:setVolume( dataManager.data.sound_volume / 100.0 )
        self.audio.reset:play()
      end

      -- Reset level
      self.game.state = "resetting"
      self.game.crankCounter = 0
      self.game.resetLevelCounter = 0
    end
  end, -- HandleInput_Play = function( self )

  ---------------------------------------------------------------------- PLACE COFFEE
  PlaceCoffee = function( self, x, y )
    if ( self.game.player.cups == 0 ) then
      -- Play error sound
      if ( self.audio.invalid:isPlaying() == false ) then
        self.audio.invalid:setVolume( dataManager.data.sound_volume / 100.0 )
        self.audio.invalid:play()
      end
    else
      -- Drop a coffee ahead of the player
      local newCoffee = {}
      newCoffee.x = x
      newCoffee.y = y
      table.insert( self.game.coffee, newCoffee )
      self.game.player.dropCooldown = 20

      -- Play coffee placement sound
      if ( self.audio.placeCoffee:isPlaying() == false ) then
        self.audio.placeCoffee:setVolume( dataManager.data.sound_volume / 100.0 )
        self.audio.placeCoffee:play()
      end

      self.game.player.cups -= 1
    end

  end,


  ---------------------------------------------------------------------- CAN WALK THERE
  CanWalkThere = function( self, object )
    local canWalk = true

    -- Check map
    local tileX = object.x / 20
    local tileY = object.y / 20
    local tileIndex = ( ( tileY * 20 ) + ( tileX % 20 ) ) + 1 -- Don't forget Lua indexing
    local tileType = self.level.map[tileIndex]

    if ( tileType ~= 1 ) then
      canWalk = false
    end


    -- Check for coffee (can't walk on that)
    for key, coffee in pairs( self.game.coffee ) do
      if ( object.x == coffee.x and object.y == coffee.y ) then
        canWalk = false
      end
    end

    -- Check for enemies (can't walk on them)
    for key, enemy in pairs( self.game.enemies ) do
      --if ( x == enemy.x and y == enemy.y ) then
      if ( GetBoundingBoxCollisionFull( object, enemy ) ) then
        canWalk = false
      end
    end

    -- If this is an enemy, they can't walk on the player
    --if ( x == self.game.player.x and y == self.game.player.y ) then
    if ( GetBoundingBoxCollisionFull( object, self.game.player ) ) then
      canWalk = false
    end

    -- Only 1 is the floor tile.
    return canWalk
  end,

  ---------------------------------------------------------------------- SETUP LEVEL DATA
  SetupLevelData = function( self )
    levels = {
    {
            instructions = {
              "I need to get out of here!",
              "Maybe I can distract my",
              "coworkers with coffee and",
              "get outta here!"
            },

            -- Level data
            background = "packages/crank/images/Level1.png",
            map = {
              22,12,12,12,12,12,23,12,12,12,12,12,12,23,12,12,12,12,12,24,
              31,5,38,38,38,38,31,38,38,38,38,38,38,41,38,38,38,38,38,31,
              31,15,25,25,25,35,31,4,1,1,1,1,4,38,1,1,1,1,1,31,
              31,1,1,1,1,1,31,1,1,1,1,1,1,1,1,1,1,1,1,31,
              31,1,1,1,1,4,31,1,1,1,1,1,1,21,2,1,2,1,2,31,
              32,13,1,1,1,11,34,4,1,1,1,1,4,31,3,1,3,1,3,31,
              31,38,1,1,1,38,32,12,12,12,12,12,12,43,12,12,13,1,11,34,
              31,1,1,1,1,1,41,38,38,38,38,38,38,38,38,38,38,1,38,31,
              31,45,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,31,
              31,2,1,1,1,2,21,2,1,2,1,2,1,2,1,2,1,1,1,31,
              31,3,1,1,1,3,31,3,1,3,1,3,1,3,1,3,1,1,1,31,
              42,12,12,12,12,12,43,12,12,12,12,12,12,12,12,12,12,12,12,44
            },
            door = { x = 9, y = 4 },
            start = { x = 3, y = 3 },
            key = { x = 18, y = 10 },
            enemies = {
              { x = 6, y = 8,  type = "employeeA", direction = 1 },
              { x = 17, y = 7,  type = "employeeA", direction = 2 },
              { x = 13, y = 3, type = "employeeA", direction = 3 },
            },
            coins = {
              { x = 3, y = 10 },
              { x = 10, y = 10 },
              { x = 15, y = 5 },
              { x = 1, y = 7 },
            }
          },
          {
            instructions = {
              "They probably put us so",
              "many floors up so we don't",
              "leave work so easily. Meh."
            },
            -- Level data
            background = "packages/crank/images/Level2.png",
            map = {
              22,12,12,12,12,23,12,12,12,12,12,12,12,12,12,12,12,12,12,24,
              31,38,38,38,38,31,38,38,38,38,38,38,38,38,38,38,38,38,38,31,
              31,2,1,1,2,31,2,1,2,1,2,1,1,1,1,1,1,1,1,31,
              31,3,1,1,3,31,3,1,3,1,3,1,22,13,1,11,24,1,1,31,
              32,13,1,11,12,34,1,1,1,1,1,1,31,38,1,38,31,1,1,31,
              31,38,1,38,38,32,12,12,12,12,12,12,34,29,1,29,31,1,1,31,
              31,2,1,1,2,31,38,38,38,38,38,38,42,12,12,12,43,13,1,31,
              31,3,1,1,3,41,1,1,1,1,1,29,5,38,38,38,38,38,1,31,
              31,1,1,1,1,38,1,1,1,1,1,1,15,25,25,35,25,25,1,31,
              31,45,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,31,
              31,1,1,1,1,21,1,1,1,1,1,1,1,1,1,1,1,1,1,31,
              42,12,12,12,12,43,12,12,12,12,12,12,12,12,12,12,12,12,12,44
            },
            door = { x = 8, y = 7 },
            start = { x = 1, y = 8 },
            key = { x = 7, y = 2 },
            enemies = {
              { x = 2, y = 5,  type = "employeeA", direction = 1 },
              { x = 5, y = 9,  type = "employeeA", direction = 2 },
              { x = 18, y = 6, type = "employeeA", direction = 3 },
            },
            coins = {
              { x = 14, y = 5 },
              { x = 9, y = 2 },
              { x = 2, y = 2 },
              { x = 9, y = 7 },
            }
          },
          {
            instructions = {
              "Man, programmers suck!",
              "I can't be out of here",
              "soon enough!!"
            },
            -- Level data
            background = "packages/crank/images/Level3.png",
            map = {
              22,12,12,12,12,12,23,12,12,12,12,12,12,23,12,12,12,12,12,24,
              31,38,38,38,38,38,31,38,38,38,38,38,5,31,38,38,38,38,38,31,
              31,2,1,2,1,2,31,25,35,25,25,25,15,31,1,1,1,1,1,31,
              31,3,1,3,1,3,31,1,45,1,1,1,1,31,1,1,1,1,1,31,
              31,1,1,1,1,22,44,1,1,1,1,1,1,31,1,1,1,1,1,31,
              31,1,1,1,1,31,38,1,1,1,1,1,1,31,4,1,1,1,4,31,
              31,4,1,1,4,31,1,1,1,1,1,1,1,32,12,13,1,11,12,34,
              32,12,13,1,11,43,12,12,13,1,11,12,12,44,38,38,1,38,38,31,
              31,38,38,1,38,38,38,38,38,1,38,38,38,38,1,1,1,1,1,31,
              31,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,31,
              31,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,31,
              42,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,44
            },
            door = { x = 16, y = 3 },
            start = { x = 10, y = 4 },
            key = { x = 14, y = 8 },
            enemies = {
              { x = 16, y = 7,  type = "employeeA", direction = 1 },
              { x = 9, y = 8,  type = "employeeA", direction = 2 },
              { x = 3, y = 8, type = "employeeA", direction = 3 },
            },
            coins = {
              { x = 6, y = 6 },
              { x = 2, y = 2 },
              { x = 9, y = 10 },
              { x = 3, y = 9 },
            }
          }, -- Level end
          {
            instructions = {
              "There's nothing to do on",
              "break in this sort of place...",
              "So people just wander around",
              "and make terrible, terrible small talk."
            },
            -- Level data
            background = "packages/crank/images/Level4.png",
            map = {
22,12,12,12,12,12,23,12,12,23,12,12,12,23,12,12,12,12,12,24,
31,38,38,38,38,38,31,38,38,31,5,38,38,31,38,38,38,38,38,31,
31,1,1,1,1,1,31,4,1,31,15,25,35,31,4,1,1,1,4,31,
31,1,1,1,22,12,44,1,1,31,1,1,1,41,1,1,1,1,1,31,
31,1,1,1,31,38,38,1,1,31,1,1,1,38,1,1,1,1,1,31,
31,1,1,1,31,1,1,1,1,31,1,1,1,1,1,1,1,1,1,31,
31,1,1,1,31,1,1,1,1,31,1,1,1,1,1,1,1,1,1,31,
31,1,1,1,31,1,1,1,1,41,1,1,1,22,12,12,13,1,11,34,
31,1,1,1,41,1,1,1,1,38,1,1,1,31,38,38,38,1,38,31,
31,1,1,1,38,1,1,1,1,1,1,1,1,31,18,9,1,1,1,31,
31,1,1,1,1,1,1,1,1,21,1,1,1,31,28,1,1,1,1,31,
42,12,12,12,12,12,12,12,12,43,12,12,12,43,12,12,12,12,12,44
            },
            door = { x = 16, y = 2 },
            start = { x = 6, y = 6 },
            key = { x = 18, y = 9 },
            enemies = {
              --{ x = 10, y = 5,  type = "employeeA", direction = 3 },
              { x = 1, y = 7,  type = "employeeC", direction = 1, walkCooldownMax = 60 },
              { x = 2, y = 7,  type = "employeeC", direction = 1, walkCooldownMax = 60 },
              { x = 3, y = 7,  type = "employeeC", direction = 1, walkCooldownMax = 60 },
              { x = 7, y = 5,  type = "employeeC", direction = 3, walkCooldownMax = 10 },
              { x = 12, y = 3,  type = "employeeD", direction = 1, walkCooldownMax = 25 },
              { x = 11, y = 4,  type = "employeeB", direction = 1 },
              { x = 17, y =8,  type = "employeeA", direction = 1 },
            },
            coins = {
              { x = 1, y = 3 },
              { x = 5, y = 2 },
              { x = 8, y = 2 },
              { x = 11, y = 6 },
            }
          }, -- Level end
          {
            instructions = {
              "They scheduled a meeting",
              "during lunch hour and they",
              "aren't providing lunch!",
              "WTF??"
            },
            -- Level data
            background = "packages/crank/images/Level5.png",
            map = {
              22,12,12,12,12,12,12,23,12,12,12,12,12,23,12,12,12,12,12,24,
              31,38,38,36,37,38,38,31,38,38,38,38,38,31,38,38,38,38,38,31,
              31,1,1,1,1,1,1,31,29,1,1,29,29,31,1,1,1,1,1,31,
              31,1,10,6,8,9,1,31,29,1,1,1,46,31,1,1,21,1,1,31,
              31,1,10,16,18,9,1,31,29,29,1,1,46,31,1,1,31,1,1,31,
              31,1,10,26,28,9,1,32,12,13,1,11,12,43,12,12,34,1,1,31,
              31,1,1,1,1,1,1,31,38,38,1,38,38,38,38,38,31,1,1,31,
              32,12,13,1,1,11,12,44,4,1,1,4,1,4,1,4,31,1,1,31,
              31,38,38,1,1,38,38,38,1,1,1,1,1,1,1,1,41,1,1,31,
              31,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,38,1,1,31,
              31,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,31,
              42,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,44
            },
            door = { x = 14, y = 2 },
            start = { x = 10, y = 4 },
            key = { x = 6, y = 2 },
            enemies = {
              { x = 10, y = 6,  type = "employeeA", direction = 1 },
              { x = 2, y = 6,  type = "employeeA", direction = 2 },
              { x = 1, y = 6,  type = "employeeA", direction = 3 },
              { x = 3, y = 2, type = "employeeB", direction = 1 },
            },
            coins = {
              { x = 14, y = 4 },
              { x = 9, y = 2 },
              { x = 14, y = 7 },
              { x = 1, y = 3 },
            }
          }, -- Level end
          {
            name = "Level 6",
            instructions = {
              "Hmm, the marketers seem",
              "to be less distractable",
              "by coffee than the devs are..."
            },
            icon = 6,
            gotoLevel = 6,
            state = 1, -- 1 = locked, 2 = no stars, 3 = one star, 4 = two stars, 5 = three stars

            -- Level data
            background = "packages/crank/images/Level6.png",
            map = {
              22,12,12,12,12,12,23,12,12,12,12,23,12,12,23,12,23,12,12,24,
              31,38,38,38,38,38,31,38,38,38,38,31,12,12,31,38,31,38,38,31,
              31,1,1,1,1,1,31,1,1,1,1,42,12,12,44,1,31,1,1,31,
              31,1,1,1,1,1,31,1,1,1,1,38,36,37,38,1,31,1,1,31,
              31,1,22,13,1,11,33,13,1,21,1,1,1,1,1,1,31,1,1,31,
              31,1,31,38,1,38,31,38,1,31,1,30,6,8,40,1,31,1,29,31,
              31,1,31,29,1,29,31,1,1,31,1,30,16,18,40,1,31,1,1,31,
              31,1,31,29,29,29,31,1,1,31,1,30,16,18,40,1,31,1,29,31,
              31,1,42,13,29,11,44,1,1,31,1,1,16,18,40,1,41,1,1,31,
              31,1,38,38,1,38,38,1,1,31,1,1,26,28,1,1,38,1,1,31,
              31,1,1,1,1,1,1,1,1,31,1,1,1,1,1,1,1,1,1,31,
              42,12,12,12,12,12,12,12,12,43,12,12,12,12,12,12,12,12,12,44
            },
            door = { x = 17, y = 2 },
            start = { x = 5, y = 2 },
            key = { x = 15, y = 2 },
            enemies = {
              { x = 8, y = 6,  type = "employeeA", direction = 3 },
              { x = 4, y = 10,  type = "employeeA", direction = 2 },
              { x = 16, y = 10,  type = "employeeA", direction = 2 },
              { x = 12, y = 4,  type = "employeeB", direction = 1 },
              { x = 18, y = 6,  type = "employeeC", direction = 3, walkCooldownMax = 40 },
            },
            coins = {
              { x = 4, y = 6 },
              { x = 13, y = 4 },
              { x = 11, y = 9 },
              { x = 1, y = 3 },
            }
          }, -- Level end
          {
            instructions = {
              "I miss software BEFORE",
              "it became a \"service\".",
              "Now, everything is",
              "a subscription model."
            },
            -- Level data
            background = "packages/crank/images/Level7.png",
            map = {
22,12,12,12,12,23,12,12,12,12,12,12,12,12,12,23,12,12,12,24,
31,38,38,38,38,31,38,38,38,38,38,38,38,38,38,31,38,5,38,31,
31,1,1,29,29,31,2,1,1,1,1,1,1,1,1,31,25,15,25,31,
31,1,1,1,29,31,1,1,1,1,21,1,6,8,1,31,1,1,1,31,
32,13,1,11,12,43,24,1,1,4,31,7,17,18,1,31,1,1,45,31,
31,38,1,36,37,38,32,12,12,12,34,27,27,28,1,31,1,1,1,31,
31,1,1,1,1,1,31,38,38,38,31,1,1,1,1,31,1,1,1,31,
31,1,6,7,8,1,31,1,1,1,42,13,1,11,12,43,13,1,1,31,
31,1,16,17,18,1,41,4,1,4,38,38,1,38,38,38,38,1,1,31,
31,1,26,27,28,1,38,1,1,1,1,1,1,1,1,1,1,1,1,31,
31,1,1,1,1,1,1,1,1,1,21,1,1,1,1,1,1,1,1,31,
42,12,12,12,12,12,12,12,12,12,43,12,12,12,12,12,12,12,12,44
            },
            door = { x = 9, y = 7 },
            start = { x = 1, y = 2 },
            key = { x = 6, y = 3 },
            enemies = {
              { x = 4, y = 6,  type = "employeeD", direction = 4, walkCooldownMax = 2 },
              { x = 8, y = 2,  type = "employeeB", direction = 1 },
              { x = 8, y = 3,  type = "employeeC", direction = 4, walkCooldownMax = 30 },
              { x = 11, y = 2,  type = "employeeA", direction = 4 },
              { x = 10, y = 9,  type = "employeeA", direction = 4 },
              { x = 12, y = 8,  type = "employeeA", direction = 4 },
              { x = 17, y = 8,  type = "employeeA", direction = 4 },
              { x = 18, y = 8,  type = "employeeA", direction = 3 },
            },
            coins = {
              { x = 1, y = 10 },
              { x = 9, y = 2 },
              { x = 14, y = 6 },
              { x = 18, y = 3 },
            }
          }, -- Level end
          {
            instructions = {
              "Just a little further!",
              "I need to escape before",
              "they decide to make us",
              "do \"MANDITORY FUN\"",
              "team building exercises."
            },
            -- Level data
            background = "packages/crank/images/Level8.png",
            map = {
22,12,12,12,12,12,23,12,12,12,12,12,12,23,12,12,12,12,12,24,
31,38,38,38,38,5,31,38,38,49,50,38,38,31,38,38,38,38,38,31,
31,1,4,35,25,15,41,1,1,1,1,1,1,41,1,29,29,1,1,31,
31,1,1,1,1,1,38,1,6,7,48,8,1,38,1,1,29,1,1,31,
31,1,1,1,1,1,1,1,16,17,17,18,1,1,1,1,1,1,1,31,
31,1,1,1,1,1,21,1,26,27,27,28,1,21,1,1,1,1,1,31,
31,1,30,6,47,1,31,1,1,1,1,1,1,31,1,1,1,6,47,31,
31,1,1,16,39,40,42,23,12,12,12,12,12,44,1,1,30,16,39,31,
31,1,1,26,28,46,38,41,38,38,38,38,38,38,1,1,1,26,28,31,
31,1,1,1,45,1,4,38,1,1,1,1,1,1,1,1,1,1,46,31,
31,1,1,1,1,1,1,1,1,21,21,1,1,1,1,1,1,1,1,31,
42,12,12,12,12,12,12,12,12,43,43,12,12,12,12,12,12,12,12,44
            },
            door = { x = 1, y = 10 },
            start = { x = 2, y = 3 },
            key = { x = 17, y = 2 },
            enemies = {
              { x = 8, y = 6,   type = "employeeD", direction = 3, walkCooldownMax = 5 },
              { x = 10, y = 6,  type = "employeeD", direction = 3, walkCooldownMax = 5 },
              { x = 12, y = 6,  type = "employeeD", direction = 3, walkCooldownMax = 5 },
              { x = 12, y = 2,  type = "employeeD", direction = 1, walkCooldownMax = 5 },
              { x = 10, y = 2,  type = "employeeD", direction = 4, walkCooldownMax = 5 },
              { x = 8, y = 2,  type = "employeeD", direction = 4, walkCooldownMax = 5 },
              { x = 11, y = 9,  type = "employeeA", direction = 4 },
            },
            coins = {
              { x = 14, y = 2 },
              { x = 10, y = 9 },
              { x = 2, y = 7 },
              { x = 1, y = 2 },
            }
          }, -- Level end
          {
            instructions = {
              "Finally on the ground level!",
              "I am ready to GET OUT!!"
            },
            -- Level data
            background = "packages/crank/images/Level9.png",
            map = {
22,12,12,12,12,23,12,12,12,12,23,12,12,12,12,12,12,12,12,24,
31,38,38,38,38,31,38,38,38,38,31,38,38,38,38,38,38,38,38,31,
31,38,38,38,5,31,38,38,38,38,31,38,38,38,38,38,1,38,38,31,
31,25,35,25,15,31,1,1,19,1,31,1,1,1,1,1,1,1,4,31,
31,45,1,1,1,31,1,6,7,8,31,1,1,47,1,1,1,1,1,31,
31,4,1,1,1,31,1,26,27,28,31,6,48,18,1,1,1,1,1,31,
32,12,13,1,11,34,1,1,1,1,31,26,27,28,1,1,22,12,12,31,
31,38,38,1,38,31,4,1,1,1,31,1,1,1,1,1,31,38,38,31,
31,29,29,1,29,42,12,13,1,11,44,1,1,11,12,12,44,29,29,31,
31,1,1,1,1,38,38,38,1,38,38,1,1,38,38,38,38,1,29,31,
31,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,29,31,
42,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,44
            },
            door = { x = 16, y = 2 },
            start = { x = 17, y = 9 },
            key = { x = 11, y = 4 },
            enemies = {
              { x = 14, y = 4,  type = "employeeB", direction = 4 },
              { x = 8, y = 3,  type = "employeeB", direction = 1 },
              { x = 12, y = 4,  type = "employeeE", direction = 1 },
              { x = 3, y = 5,  type = "employeeA", direction = 1 },
              { x = 8, y = 10,  type = "employeeC", direction = 3, walkCooldownMax = 1 },
              { x = 8, y = 6,  type = "employeeD", direction = 4, walkCooldownMax = 30 },
            },
            coins = {
              { x = 2, y = 5 },
              --{ x = 9, y = 7 },
              { x = 15, y = 4 },
              { x = 6, y = 3 },
            }
          }, -- Level end
    }
    return levels
  end,
}

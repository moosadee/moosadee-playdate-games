import 'utilities.lua'
import 'screen_mainmenu.lua'
import 'screen_options.lua'
import 'screen_credits.lua'
import 'screen_levelselect.lua'
import 'screen_help.lua'
import 'screen_title.lua'

import 'packages/bass/bass_gameScreen.lua'
import 'packages/crank/crank_gameScreen.lua'
import 'packages/highfive/highfive_gameScreen.lua'
import 'packages/lasers/lasers_gameScreen.lua'
import 'packages/motor/motor_gameScreen.lua'
import 'packages/pup/pup_gameScreen.lua'
import 'packages/skele/skele_gameScreen.lua'
import 'packages/tpcat/tpcat_gameScreen.lua'
import 'packages/finkit/finkit_gameScreen.lua'

currentState = nil

function ChangeState( nextState )
  if ( currentState ~= nil ) then
    currentState:Cleanup()
    currentState = nil
  end

  -- Hopefully this is better for memory
  if     ( nextState == "mainMenuScreen" )       then     currentState = mainMenuScreen
  elseif ( nextState == "optionsScreen" )        then     currentState = optionsScreen
  elseif ( nextState == "creditsScreen" )        then     currentState = creditsScreen
  elseif ( nextState == "levelSelectScreen" )    then     currentState = levelSelectScreen
  elseif ( nextState == "helpScreen" )           then     currentState = helpScreen
  elseif ( nextState == "titleScreen" )          then     currentState = titleScreen
  elseif ( nextState == "tpcat_gameScreen" )     then     currentState = tpcat_gameScreen
  elseif ( nextState == "bass_gameScreen" )      then     currentState = bass_gameScreen
  elseif ( nextState == "crank_gameScreen" )     then     currentState = crank_gameScreen
  elseif ( nextState == "highfive_gameScreen" )  then     currentState = highfive_gameScreen
  elseif ( nextState == "lasers_gameScreen" )    then     currentState = lasers_gameScreen
  elseif ( nextState == "motor_gameScreen" )     then     currentState = motor_gameScreen
  elseif ( nextState == "pup_gameScreen" )       then     currentState = pup_gameScreen
  elseif ( nextState == "skele_gameScreen" )     then     currentState = skele_gameScreen
  elseif ( nextState == "finkit_gameScreen" )    then     currentState = finkit_gameScreen
  end

  --if ( DoesTableContainKey( stateMap, nextState ) ) then
    --currentState = stateMap[nextState]
  --end

  InitState()
end

function InitState()
  if ( currentState ~= nil ) then
    currentState:Init()
  end
end

function UpdateState()
  if ( currentState ~= nil ) then
    currentState:Update()

    if ( currentState.nextState ~= "" ) then
      ChangeState( currentState.nextState )
    end
  end
end

function DrawState()
  if ( currentState ~= nil ) then
    currentState:Draw()
  end
end

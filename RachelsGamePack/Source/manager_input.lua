import 'CoreLibs/graphics.lua'
local gfx = playdate.graphics

-- Button states:
-- 0: not pressed,  was not pressed previously
-- 1: pressed
-- 2: not pressed,  was pressed previously

--  0 -> 1 -> 2 -> 0

BTN_UP = 0
BTN_DOWN = 1
BTN_UP_WASPRESSED = 2

inputManager = {

  keypresses = {
    up    = { pressed = BTN_UP, },
    down  = { pressed = BTN_UP, },
    left  = { pressed = BTN_UP, },
    right = { pressed = BTN_UP, },
    a     = { pressed = BTN_UP, },
    b     = { pressed = BTN_UP, }
  },

  crank = {
    angleDegrees = 0,
    angleRadians = 0,
    changeDegrees = 0,
    changeRadians = 0
  },

  Update = function( self )

    for keyKey, keyTable in pairs( self.keypresses ) do
      if ( keyTable.pressed == BTN_UP_WASPRESSED ) then
        keyTable.pressed = BTN_UP
      end
    end
  end,

  AreAllDirectionsUp = function( self )
    return ( self.keypresses.up.pressed == BTN_UP and
             self.keypresses.down.pressed == BTN_UP and
             self.keypresses.left.pressed == BTN_UP and
             self.keypresses.right.pressed == BTN_UP )
  end,

  -- This is meant for menus
  IsKeyPressRelease = function( self, keyName )
    return ( self.keypresses[keyName].pressed == BTN_UP_WASPRESSED )
  end,

  -- This is for gameplay
  IsKeyPressed = function( self, keyName )
    return ( self.keypresses[keyName].pressed == BTN_DOWN )
  end,


  Handle_Crank = function( self )
    self.crank.angleDegrees  = playdate.getCrankPosition()
    self.crank.angleRadians  = math.rad( self.crank.angleDegrees )
    self.crank.changeDegrees = playdate.getCrankChange()
    self.crank.changeRadians = math.rad( self.crank.changeDegrees )
  end,
  
  Handle_upButtonDown     = function( self )
    inputManager.keypresses.up.pressed = BTN_DOWN
  end,

  Handle_upButtonUp       = function( self )
    inputManager.keypresses.up.pressed = BTN_UP_WASPRESSED
  end,

  Handle_downButtonDown   = function( self )
    inputManager.keypresses.down.pressed = BTN_DOWN
  end,

  Handle_downButtonUp     = function( self )
    inputManager.keypresses.down.pressed = BTN_UP_WASPRESSED
  end,

  Handle_leftButtonDown   = function( self )
    inputManager.keypresses.left.pressed = BTN_DOWN
  end,

  Handle_leftButtonUp     = function( self )
    inputManager.keypresses.left.pressed = BTN_UP_WASPRESSED
  end,

  Handle_rightButtonDown  = function( self )
    inputManager.keypresses.right.pressed = BTN_DOWN
  end,

  Handle_rightButtonUp    = function( self )
    inputManager.keypresses.right.pressed = BTN_UP_WASPRESSED
  end,

  Handle_BButtonDown      = function( self )
    inputManager.keypresses.b.pressed = BTN_DOWN
  end,

  Handle_BButtonUp        = function( self )
    inputManager.keypresses.b.pressed = BTN_UP_WASPRESSED
  end,

  Handle_AButtonDown      = function( self )
    inputManager.keypresses.a.pressed = BTN_DOWN
  end,

  Handle_AButtonUp        = function( self )
    inputManager.keypresses.a.pressed = BTN_UP_WASPRESSED
  end,
}
